# teyat
Django project for management of theater (schools centers, campaigns, functions, theater companies, reserves, earnings, statistics)

Proyecto en Django para el manejo de teatros (centros de enseñanza, campañas, funciones, compañías de teatro, reservas, recaudaciones, estadísticas, etc.)

Login screen:

![Login](img01.png)

Dashboard screen:

![Dashboard](img02.png)

Reserves screen:

![Dashboard](img03.png)
