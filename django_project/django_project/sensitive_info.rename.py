# -*- coding: utf-8 -*-


# SECURITY WARNING: keep the secret key used in production secret!
SETTINGS_SECRET_KEY = 'SECRETKEY1234567890'
SETTINGS_EMAIL_HOST_USER = 'my@email.es'
SETTINGS_EMAIL_HOST_PASSWORD = 'mailpassword'
SETTINGS_DATABASE_PASSWORD = 'dbpassword'
