# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

"""
Django settings for django_project project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

LOCALE_PATHS=  ( os.path.join(BASE_DIR, 'locale'), )

ADMINS = (
    ('Teyat Admin', 'admin@teyat.es'),
)

MANAGERS = ADMINS

ALLOWED_HOSTS =  ['teyat.es', 'www.teyat.es', '104.236.63.165', '104.236.63.165:9000', '45.55.226.211', '45.55.226.211:9000', u'0.0.0.0:9000', '0.0.0.0:9000', '127.0.0.1:9000', u'127.0.0.1:9000',] 

# SECURITY WARNING: keep the secret key used in production secret!
from django_project.sensitive_info import SETTINGS_SECRET_KEY
SECRET_KEY = SETTINGS_SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
if '/prod/' in BASE_DIR:
    DEBUG = False
    TEMPLATE_DEBUG = False
else:
    DEBUG = True
    TEMPLATE_DEBUG = True


# Application definition

INSTALLED_APPS = (
    
    'import_export',
    'modeltranslation',
    'rules.apps.AutodiscoverRulesConfig',
    'admin_stats',
    'teyat',
    'suit',
    'django.contrib.admin.apps.SimpleAdminConfig',
    #'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.humanize',
    'bootstrap3',
    'debug_toolbar',
    'easy_pdf',
    'django_markdown',
    #'easy_select2',
    'django_select2',
    'meta',
)

#################
SITE_ID=1

#################
## Django-meta configuration
#################
META_SITE_PROTOCOL = 'http'
META_USE_SITES = True
META_USE_OG_PROPERTIES = True
META_USE_TWITTER_PROPERTIES = True
META_USE_GOOGLEPLUS_PROPERTIES = False
META_SITE_NAME='teyat.es'
META_SITE_TYPE='website'

from django_project.sensitive_info import SETTINGS_EMAIL_HOST_USER,SETTINGS_EMAIL_HOST_PASSWORD
EMAIL_USE_TLS = True
EMAIL_HOST = 'localhost'
EMAIL_PORT = 587
EMAIL_HOST_USER = SETTINGS_EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = SETTINGS_EMAIL_HOST_PASSWORD
 
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER



MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'teyat.middleware.FilterPersistMiddleware',
    
)

ROOT_URLCONF = 'django_project.urls'

WSGI_APPLICATION = 'django_project.wsgi.application'

# Markdown
MARKDOWN_EDITOR_SKIN = 'simple'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
from django_project.sensitive_info import SETTINGS_DATABASE_PASSWORD

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'django',
        'USER': 'django',
        'PASSWORD': SETTINGS_DATABASE_PASSWORD,
        'HOST': 'localhost',
        'PORT': '',
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-ES'

TIME_ZONE = 'Europe/Madrid'

USE_I18N = True

USE_L10N = True

USE_TZ = True



# We decided to set a dir outside dev and prod developments to avoid
# overwriting
MEDIA_ROOT = '/home/django/media/'
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = BASE_DIR + '/static/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_URL = '/static/'

TEMPLATE_DIRS = (
    BASE_DIR + '/templates/',
    BASE_DIR + '/teyat/templates/',
)

'''We need to add the authorization backend or Rules
that is able to provide object-level permissions by looking into 
the permissions-specific rule set'''
AUTHENTICATION_BACKENDS = (
    'rules.permissions.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)


'''Required for Ratings app to work:'''
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
   'django.contrib.auth.context_processors.auth',
   'django.core.context_processors.request',
   'django.core.context_processors.i18n',
)


# list of languages to translate:
gettext = lambda s: s
LANGUAGES= (
    ('ca', _(u'Catalán')),
    ('es', _(u'Castellano')),
)

MODELTRANSLATION_LANGUAGES = ('ca', 'es')

MODELTRANSLATION_DEFAULT_LANGUAGE = 'es'

MODELTRANSLATION_PREPOPULATE_LANGUAGE = 'es'


BOOTSTRAP3 = {
 'required_css_class': 'required',
}



SUIT_CONFIG = {
    # header
     'ADMIN_NAME': _(u'teyat'),
     'HEADER_DATE_FORMAT': 'l, j F Y',
     'HEADER_TIME_FORMAT': 'H:i',

    # menu
     #'SEARCH_URL': '/teyat-admin/teyat/obraencampanya/',
     'SEARCH_URL': '',
     'MENU_OPEN_FIRST_CHILD': True,
     #'MENU_EXCLUDE': ('auth', 'sites',),
     'MENU': (
         
        
        {'label': _(u'Configura tu teatro'), 'icon':'icon-film', 'models': ('teyat.teatro',)},
        '-',
        {'label': _(u'Compañías y obras'), 'icon':' icon-star', 'models': ('teyat.companyia', 'teyat.obra')},
        '-',
        {'label': _(u'Campañas y funciones'), 'icon':'icon-bullhorn', 'models': ('teyat.campanya', 'teyat.funcion',)},
        '-',
        {'label': _(u'Aforos'), 'icon':'icon-user', 'models': ('teyat.aforo_funcion','teyat.aforo_obraencampanya','teyat.aforo_campanya',)},
        {'label': _(u'Recaudaciones'), 'icon':'icon-fire', 'models': ('teyat.recaudacion_funcion', 'teyat.recaudacion_obraencampanya','teyat.recaudacion_campanya')},

        '-',
        {'label': _(u'Reservas'), 'icon':'icon-calendar', 'models': ('teyat.reserva',)},
        {'label': _(u'Correos'), 'icon':'icon-envelope', 'models': ('teyat.correoconfirmacionreserva', 'teyat.correoreserva', 'teyat.logcorreosestadoreserva', 'teyat.logcorreosrecordatorios', )},
        '-',
        {'label': _(u'Centros'), 'icon':'icon-book', 'models': ('teyat.centro', 'teyat.persona_contacto')},
        '-',#'-',         
        {'label': _(u'Poblaciones'), 'icon':' icon-map-marker', 'models': ('teyat.poblacion',)},
        #'-',
        #{'label': _(u'Idiomas'), 'icon': 'icon-italic', 'models': ('teyat.idioma',)},
        ), 

    
    # misc
     'LIST_PER_PAGE': 20
}


# Django Suit configuration example
SUIT_CONFIG_RAVAL = {
    # header
     'ADMIN_NAME': _(u'teyat'),
     'HEADER_DATE_FORMAT': 'l, j F Y',
     'HEADER_TIME_FORMAT': 'H:i',

    # menu
     'SEARCH_URL': '',
     'MENU_OPEN_FIRST_CHILD': True,
     #'MENU_EXCLUDE': ('auth', 'sites',),
     'MENU': (
         #{'app': 'wildcaribe', 'label': 'Wild Caribe', 'icon': 'icon-lock'},
         # Reorder app models
        
        {'label': _(u'Compañías y obras'), 'icon':' icon-star', 'models': ('teyat.companyia', 'teyat.obra')},
        '-',
        {'label': _(u'Campañas, obras en campaña y funciones'), 'icon':'icon-bullhorn', 'models': ('teyat.campanya', 'teyat.obraencampanya', 'teyat.funcion',)},
        '-',
        {'label': _(u'Reservas'), 'icon':'icon-calendar', 'models': ('teyat.reserva',)},
        {'label': _(u'Correos'), 'icon':'icon-envelope', 'models': ('teyat.correoconfirmacionreserva', 'teyat.correoreserva', 'teyat.adjunto', 'teyat.logcorreosrecordatorios')},
        '-',
        {'label': _(u'Centros'), 'icon':'icon-book', 'models': ('teyat.centro', 'teyat.persona_contacto')},
        '-','-',         
        {'label': _(u'Poblaciones y comarcas'), 'icon':' icon-map-marker', 'models': ('teyat.poblacion', 'teyat.comarca')},
        '-',
        {'label': _(u'Idiomas'), 'icon': 'icon-italic', 'models': ('teyat.idioma',)},
        ), 

    
    # misc
     'LIST_PER_PAGE': 20
}


# Django Suit configuration example
SUIT_CONFIG_STAFF = {
    # header
     'ADMIN_NAME': _(u'teyat STAFF'),
     'HEADER_DATE_FORMAT': 'l, j F Y',
     'HEADER_TIME_FORMAT': 'H:i',

    # menu
     'SEARCH_URL': '',
     'MENU_OPEN_FIRST_CHILD': True,
     #'MENU_EXCLUDE': ('auth', 'sites',),
     'MENU': (
         #{'app': 'wildcaribe', 'label': 'Wild Caribe', 'icon': 'icon-lock'},
         # Reorder app models
        
        {'label': _(u'Usuarios'), 'icon':' icon-user', 'models': ('teyat.teatro_contacto','auth.user',)},
        '-',
        {'label': _(u'Teatros'), 'icon':' icon-star', 'models': ('teyat.teatro','auth.group',)},
        '-',
        {'label': _(u'Acciones de usuarios'), 'icon':' icon-tasks', 'models': ('admin.logentry',)},
        
        ), 

    
    # misc
     'LIST_PER_PAGE': 20
}



CACHES = {

    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    },
    
    'select2': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'select2_cache_table_dev',
    }
}

if DEBUG:
    CACHES = {

        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        },
        
        'select2': {
            'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
            'LOCATION': 'select2_cache_table_dev',
        }
    }
    
# set the cache backend to select2
SELECT2_CACHE_BACKEND= 'select2'
