# -*- coding: utf-8 -*-
from __future__ import division
from django.db import models
from django.utils import timezone
from django.utils.html import mark_safe
import datetime
from time import strftime
from django.contrib.sessions.models import Session
from django.core.validators import RegexValidator
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.template.defaultfilters import date as _date
from django.contrib.auth.models import User
import os
from django.utils import timezone
from teyat.suit_multi import suit_multi_admin
from django_project.settings import BASE_DIR

if 'prod' in BASE_DIR:
    base_url = 'http://teyat.es/'
else:
    base_url = 'http://teyat.es:9999/'
    
################################
##      TEATRO                ##
################################ 
AFORO_TEATRE = 210




class Teatro(models.Model):

    VALENCIANO = 'ca'
    CASTELLANO = 'es'
    BILINGUE = 'bi'
    
    PREFERENCIA_IDIOMA = (
        (VALENCIANO, _(u'Catalán')),
        (CASTELLANO, _(u'Español')),
        (BILINGUE, _(u'Ambos (Bilingüe)')),
    )
    
    nombre = models.CharField(_(u'Nombre'), max_length=250) 
    logo_bn  = models.ImageField(upload_to='images/teatro', verbose_name=_(u'Logo para la esquina superior'), help_text=_(u'Dimensiones mínimas recomendadas: 200 x 100px. El logo de tu teatro que aparecerá en la esquina superior izquierda de esta página.'), default='images/teatro/default_logo_teatro_bn.png')
    logo_nb  = models.ImageField(upload_to='images/teatro', verbose_name=_(u'Logo para correos e informes'), help_text=_(u'Dimensiones mínimas recomendadas: 200 x 100px. El logo de tu teatro que aparecerá en los correos que mandes a través de teyat y en los informes sobre aforos y recaudaciones.'), default='images/teatro/default_logo_teatro_nb.png')
    direccion = models.CharField(_(u'Dirección'), max_length=500, null=True, blank=True)
    ciudad = models.ForeignKey('Poblacion', verbose_name=_(u'Población'), null=True, blank=True)
    email = models.EmailField(_(u'Email del teatro'))
    emailcampanya = models.EmailField(_(u'Email para campañas'),help_text=_(u'La dirección de correo desde la que el teatro realiza sus campañas o reservas'))
    website = models.CharField(_(u'Sitio web'), max_length=100, null=True, blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(u"El número de teléfono tiene que tener el formato: '+1234567890' y una longitud máxima de 15 caracteres.")) 
    telefono = models.CharField(validators=[phone_regex], verbose_name=_(u'Teléfono principal'), max_length=16, blank=True, null=True)
    aforo = models.PositiveSmallIntegerField(_(u'Aforo total'), validators = [MinValueValidator(0),], default=AFORO_TEATRE, help_text=_(u"Número total de localidades de tu teatro."))
    idioma_mails = models.CharField(_(u'Idioma Mails'), max_length=2, choices=PREFERENCIA_IDIOMA, default=BILINGUE, help_text=_(u"El idioma o idiomas en que quieres enviar correos a tus clientes."))
    users = models.ManyToManyField(User, verbose_name=_(u'Usuarios'))
    contacto = models.ForeignKey('Teatro_contacto', verbose_name=_(u'Contacto'), null=True, blank=True)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)

    class Meta:
        verbose_name=_(u'Tu teatro')
        verbose_name_plural = _(u'Tu teatro') 
        ordering=['nombre']
        
    def usuarios(self):
        return ";\n".join([u.username for u in self.users.all()])
    usuarios.short_description= _(u'Usuarios')
    
    def __unicode__(self):
        return self.nombre



#############################
## COMARCA Y POBLACION     ##
#############################


class Comarca(models.Model):
    nombre = models.CharField(_(u'Nombre'), max_length=250)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    class Meta:
        unique_together = (('nombre', 'owner'),)
        verbose_name=_(u'Comarca')
        verbose_name_plural = _(u'Comarcas') 
        ordering=['nombre']
        
    def __unicode__(self):
        return self.nombre
        
class Poblacion(models.Model):
    nombre = models.CharField(_(u'Nombre'), max_length=500)
    codigo_postal = models.PositiveIntegerField(_(u'Código postal'), validators = [MinValueValidator(1),], null=True, blank=True)
    comarca = models.ForeignKey('Comarca', verbose_name=_(u'Comarca'), null=True, blank=True)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    class Meta:
        unique_together = (('nombre', 'codigo_postal','owner',),)
        verbose_name=_(u'Población')
        verbose_name_plural = _(u'Poblaciones') 
        ordering=['nombre', 'codigo_postal']
    
    def nombre_bonito(self):
        if self.comarca:
            if self.codigo_postal:
                return self.nombre + ' - ' + str(self.codigo_postal) + ' (' + self.comarca.nombre + ')'
            else:
                return self.nombre + ' (' + self.comarca.nombre + ')'
        else:
            if self.codigo_postal:
                return self.nombre + ' - ' + str(self.codigo_postal) 
            else:
                return self.nombre
    def __unicode__(self):
        return self.nombre_bonito()
        
        
#####################################
##  CENTRO Y PERSONAS DE CONTACTO  ##
#####################################

class Centro(models.Model):

    PUBLICO = 'pu'
    PRIVADO = 'pr'
    CONCERTADO = 'co'
    
    TITULARIDAD_TIPO = (
        (PUBLICO, _(u'Público')),
        (CONCERTADO, _(u'Concertado')),
        (PRIVADO, _(u'Privado')),
    )
    
    INFANTIL = 'i'
    PRIMARIA = 'p'
    SECUNDARIA = 's'
    INF_PRIM = 'ie'
    INF_PRIM_SEC = 'ies'
    PRIM_SEC = 'ps'
    ESCOLETA = 'esc'
    ESPECIAL = 'esp'
    
    GRADO_TIPO = (
        (INFANTIL, _(u'Educación infantil')),
        (INF_PRIM, _(u'Educación infantil y primaria')),
        (INF_PRIM_SEC, _(u'Educación infantil, primaria y secundaria')),
        (PRIMARIA, _(u'Educación primaria')),
        (PRIM_SEC, _(u'Educación primaria y secundaria')),
        (SECUNDARIA, _(u'Educación secundaria')),
        (ESCOLETA, _(u'Escoleta')),
        (ESPECIAL, _(u'Otros')),
    )
    
    nombre = models.CharField(_(u'Nombre'), max_length=1000)
    titularidad = models.CharField(_(u'Financiación'), max_length=2, choices=TITULARIDAD_TIPO, default=PUBLICO, blank=True, null=True)
    grado = models.CharField(_(u'Grado educación'), max_length=3, choices=GRADO_TIPO, default=INF_PRIM, blank=True, null=True)
    direccion = models.CharField(_(u'Dirección'), max_length=500, null=True, blank=True)
    poblacion = models.ForeignKey('Poblacion', verbose_name=_(u'Población'), null=True, blank=True)
    email = models.EmailField(_(u'Email del centro'))
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(u"El número de teléfono tiene que tener el formato: '+1234567890' y una longitud máxima de 15 caracteres.")) 
    telefono = models.CharField(validators=[phone_regex], verbose_name=_(u'Teléfono principal'), max_length=16, blank=True, null=True)
    extension = models.CharField(verbose_name=_(u'Extensión'), max_length=8, blank=True, null=True)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    ref = models.CharField(_(u'ref'), max_length=5, help_text=_(u'El código de referencia utilizado en las antiguas Excels para vincular reservas con centros.'),null=True, blank=True)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    def comarca(self):
        if self.poblacion:
            if self.poblacion.comarca:
                return self.poblacion.comarca.nombre
            else:
                return ''
        else:
           return ''
    comarca.short_description= _(u'Comarca')
    
    def nombre_y_poblacion(self):
        if self.poblacion:
            if self.poblacion.comarca:
                return  self.nombre + ' - ' + '(' + self.poblacion.nombre + ', ' + self.comarca() + ')'
            else:
                return self.nombre + ' - ' + '(' + self.poblacion.nombre + ')' 
        else:
            return self.nombre
    
    def direccion_postal(self):
        if self.poblacion:
            return self.nombre + '\n' + self.direccion + '\n' + self.poblacion.nombre_bonito()
        else:
            return '' 
    direccion_postal.short_description= _(u'Dirección Postal')
    
    def personas_contacto(self):
        personas = Persona_contacto.objects.filter(centro=self)
        result=''
        for p in personas:
            result+= p.nombre 
            if p.email:
                result += ' ' + p.email 
            if p.telefono:
                result += ' - telefono: ' + p.telefono 
            if p.cargo:
                result += ' - cargo: ' + p.cargo 
            result+=';\n'
        return result
    personas_contacto.short_description= _(u'Personas de contacto')
    
    def num_reservas(self):
        return Reserva.objects.filter(centro=self).count()
    num_reservas.allow_tags = True
    num_reservas.short_description = _(u'Num. reservas')
    
    def num_reservas_por_campanya(self):
        campanyas = Campanya.objects.filter(owner=self.owner)
        result=''
        for c in campanyas:
            reservas = Reserva.objects.filter(funcion__obra_campanya__campanya=c, centro=self).count()
            result+= c.nombre + ': ' + str(reservas) + '; '
        return result
    num_reservas_por_campanya.allow_tags = True
    num_reservas_por_campanya.short_description = _(u'Num. reservas')
    
    def num_reservas_por_campanya_html(self):
        campanyas = Campanya.objects.filter(owner=self.owner)
        result='<ul>'
        for c in campanyas:
            reservas = Reserva.objects.filter(funcion__obra_campanya__campanya=c, centro=self).count()
            result+= '<li><strong>' + c.nombre + '</strong>: ' + str(reservas) + '</li>'
        result +='</ul>'
        return result
    num_reservas_por_campanya_html.allow_tags = True
    num_reservas_por_campanya_html.short_description = _(u'Num. reservas')
    
    def nombre_reservas_por_campanya_html(self):
        campanyas = Campanya.objects.filter(owner=self.owner)
        result=''
        for c in campanyas:
            reservas = Reserva.objects.filter(funcion__obra_campanya__campanya=c, centro=self)
            
            if len(reservas)>0:
                result+= '<strong>' + c.nombre + '</strong>: ' + str(len(reservas)) + '<br/>'
                result += '<ul>'
                for r in reservas:
                    result += '<li><a href="' + base_url + 'admin/teyat/reserva/' + str(r.id) + '/" target="_blank">' + r.print_obra_hora() + ' - ' + r.print_asistencia() + ' (' + r.get_estado_display() + ')'  + '</a></li>'
                result += '</ul>'
            #else:
            #    result += _(u'Ninguna')+ ';'
            #result+='<br/>'
        return result
    nombre_reservas_por_campanya_html.allow_tags = True
    nombre_reservas_por_campanya_html.short_description = _(u'Reservas')
    
    def nombre_reservas_por_campanya(self):
        campanyas = Campanya.objects.filter(owner=self.owner)
        result=''
        for c in campanyas:
            reservas = Reserva.objects.filter(funcion__obra_campanya__campanya=c, centro=self)
            if len(reservas)>0:
                result+= c.nombre + ': '
                for r in reservas:
                    result += unicode(r.print_obra_hora()) + ' - ' + unicode(r.print_asistencia()) + ' (' + unicode(r.get_estado_display()) + ')' + '; '
            #else:
            #    result += _(u'Ninguna')+ '; '
        return result
    nombre_reservas_por_campanya.allow_tags = True
    nombre_reservas_por_campanya.short_description = _(u'Reservas')
    
    
    class Meta:
        #unique_together = (('nombre', 'poblacion'),)
        verbose_name=_(u'Centro')
        verbose_name_plural = _(u'Centros') 
        ordering=['nombre']
        
    def __unicode__(self):
        return self.nombre_y_poblacion()

class Persona_contacto(models.Model):

    nombre = models.CharField(_(u'Nombre'), max_length=400)
    email = models.EmailField(_(u'Email'), blank=True, null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(u"El número de teléfono tiene que tener el formato: '+1234567890' y una longitud máxima de 15 caracteres.")) 
    telefono = models.CharField(validators=[phone_regex], verbose_name=_(u'Teléfono principal'), max_length=16, blank=True, null=True)
    cargo = models.CharField(_(u'Cargo'), max_length=400, null=True, blank=True)
    centro = models.ForeignKey('Centro', verbose_name=_(u'Centro'))
    observaciones = models.CharField(_(u'Observaciones'), max_length=1000, help_text=_(u'Horarios de atención, disponibilidad, números de teléfono alternativos, etc.'), null=True, blank=True)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    class Meta:
        verbose_name=_(u'Persona de contacto')
        verbose_name_plural = _(u'Personas de contacto') 
        ordering=['centro__nombre', 'nombre']
    
    def detalle(self):
        if self.centro:
            
            if self.cargo:
                return self.centro.nombre_y_poblacion() + ': ' + self.nombre + ' - ' + self.cargo
            else:
                return self.centro.nombre_y_poblacion() + ': ' + self.nombre
        else:
            return unicode(_(u'Centro Desconocido')) + ': ' + self.nombre
    def __unicode__(self):
        return self.detalle()

        
           
#############################
###  COMPAÑIA Y OBRAS     ###
#############################

class Idioma(models.Model):
    nombre = models.CharField(_(u'Nombre'), max_length=250)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    class Meta:
        verbose_name=_(u'Idioma')
        verbose_name_plural = _(u'Idiomas') 
        ordering=['nombre']
        
    def __unicode__(self):
        return self.nombre

class Companyia(models.Model):
    nombre = models.CharField(_(u'Nombre'), max_length=250)
    direccion = models.CharField(_(u'Dirección'), max_length=500, null=True, blank=True)
    poblacion = models.ForeignKey('Poblacion', verbose_name=_(u'Población'), null=True, blank=True)
    email = models.EmailField(_(u'Email'), null=True, blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(u"El número de teléfono tiene que tener el formato: '+1234567890' y una longitud máxima de 15 caracteres.")) 
    telefono = models.CharField(validators=[phone_regex], verbose_name=_(u'Teléfono principal'), max_length=16, blank=True, null=True)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    def obras(self):
        return ";\n".join([o.nombre for o in Obra.objects.filter(companyia=self.id)])
    obras.short_description= _(u'Obras que representa')
        
        
    class Meta:
        verbose_name=_(u'Compañía')
        verbose_name_plural = _(u'Compañías') 
        ordering=['nombre']
        
    def __unicode__(self):
        return self.nombre


        
class Obra(models.Model):

    nombre = models.CharField(_(u'Nombre'), max_length=250)
    duracion = models.PositiveSmallIntegerField(_(u'Duración (mins)'), validators = [MinValueValidator(0),], null=True, blank=True)
    idiomas = models.ManyToManyField(Idioma, verbose_name=_(u'Idiomas'), blank=True)
    no_idioma = models.BooleanField(_(u'Idioma no relevante'), help_text=_(u'Marcar si se trata de un espectáculo de mímica, un musical sin letra, una instalación, etc'), default=False)
    companyia = models.ForeignKey(Companyia, verbose_name=_(u'Compañía'))
    edad_min = models.PositiveSmallIntegerField(_(u'Edad mínima'), help_text=_(u'Usar en rangos: 5 si la obra es para público entre 5 y 10 años. Usar si una obra es a partir de una edad mínima: 12 si la obra es a partir de 12 años.'), validators = [MinValueValidator(0),], null=True, blank=True)
    edad_max = models.PositiveSmallIntegerField(_(u'Edad máxima'), help_text=_(u'Usar en rangos: 10 si la obra es para público entre 5 y 10 años. Usar si una obra es para menores de una edad: 6 si es para menores de 6 años.'), validators = [MinValueValidator(0),], null=True, blank=True)
    aforo_max = models.PositiveSmallIntegerField(_(u'Aforo máximo'), validators = [MinValueValidator(0),], default=AFORO_TEATRE)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    class Meta:
        verbose_name=_(u'Obra')
        verbose_name_plural = _(u'Obras') 
        ordering=['nombre']
     
    def campanyas(self):
        return ";\n".join([oc.campanya.nombre for oc in ObraEnCampanya.objects.filter(obra=self.id)])
    campanyas.short_description= _(u'En campaña')
    
    def nombre_idiomas(self):
        if self.no_idioma:
            return unicode(_(u'Idioma no relevante'))
        else:
            if self.idiomas.all().count()>0:
                return ";\n".join([i.nombre for i in self.idiomas.all()])
            else:
                return unicode(_(u'Idioma no definido'))
    nombre_idiomas.short_description= _(u'Idiomas de las funciones')
    
    
    def edades(self):
        result = u''
        if self.edad_min and self.edad_max:
            return  str(self.edad_min) + u' - ' + unicode(str(self.edad_max)) + u' ' + unicode(_(u'años'))
        elif self.edad_min:
            return  unicode(_(u'a partir de')) + u' ' + unicode(str(self.edad_min)) + u' ' + unicode(_(u'años'))
        elif self.edad_max:
            return  unicode(_(u'hasta los')) + u' ' + unicode(str(self.edad_max)) + u' ' + unicode(_(u'años'))
        else:
            return u''
    edades.short_description= _(u'Edades')
    
    #def ficha(self):
    #    return 'obra/obra' + str(self.id) + '.pdf'
    
    def __unicode__(self):
        return unicode(self.nombre + ': ' + self.edades())
        
    
class Campanya(models.Model):

    nombre = models.CharField(_(u'Nombre'), max_length=250)
    precio_obra = models.FloatField(_(u'Precio obra/espectador €'), validators = [MinValueValidator(0.0),], default=0)
    precio_visita_guiada = models.FloatField(_(u'Precio de la Actividades complementarias €'), validators = [MinValueValidator(0.0),], default=0)
    los_maestros_no_pagan = models.BooleanField(_(u'Los maestros no pagan'), help_text=_(u'Por defecto, los docentes y acompañantes no pagan las entradas en los grupos escolares. Desmarcar si en los grupos escolares los docentes SÍ que pagan entrada.'), default=True)
    obras =  models.ManyToManyField('Obra', through='ObraEnCampanya')
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    espectadores_previstos = models.PositiveSmallIntegerField(_(u'Espectadores (previstos)'), validators = [MinValueValidator(0),], default=0)
    espectadores_reales = models.PositiveSmallIntegerField(_(u'Espectadores (reales)'), validators = [MinValueValidator(0),], default=0)
    alumnos_previsto = models.PositiveSmallIntegerField(_(u'Alumnos (previsto)'),  validators = [MinValueValidator(0),], default=0)
    maestros_previsto = models.PositiveSmallIntegerField(_(u'Maestros (previsto)'),  validators = [MinValueValidator(0),], default=0)
    alumnos_real = models.PositiveSmallIntegerField(_(u'Alumnos (real)'), validators = [MinValueValidator(0),], default=0)
    maestros_real = models.PositiveSmallIntegerField(_(u'Maestros (real)'), validators = [MinValueValidator(0),], default=0)
    recaudacion_funciones_prevista = models.FloatField(_(u'Recaudación funciones (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0)
    recaudacion_funciones_real = models.FloatField(_(u'Recaudación funciones (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_visitas_prevista = models.FloatField(_(u'Recaudación actividades complementarias (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_visitas_real = models.FloatField(_(u'Recaudación actividades complementarias (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_prevista = models.FloatField(_(u'Total recaudación (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_real = models.FloatField(_(u'Total recaudación (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
     
    class Meta:
        verbose_name=_(u'Campaña')
        verbose_name_plural = _(u'Campañas') 
        ordering=['nombre']  
    
    
    def nombre_obras(self):
        return "; \n".join([oc.obra.nombre for oc in ObraEnCampanya.objects.filter(campanya=self.id)])
    nombre_obras.short_description= _(u'Obras en campaña')
    
    def calcula_espectadores_previstos(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.espectadores_previstos
        return amount
    
    def calcula_espectadores_reales(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.espectadores_reales
        return amount
        
        
    def calcula_alumnos_previstos(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.alumnos_previsto
        return amount
    
    def calcula_alumnos_reales(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.alumnos_real
        return amount
    
    def calcula_maestros_previstos(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.maestros_previsto
        return amount
    
    def calcula_maestros_reales(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.maestros_real
        return amount
    
    def calcula_recaudacion_funciones_prevista(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.recaudacion_funciones_prevista
        return amount
    
    def calcula_recaudacion_funciones_real(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.recaudacion_funciones_real
        return amount
    
    def calcula_recaudacion_visitas_prevista(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.recaudacion_visitas_prevista
        return amount
    
    def calcula_recaudacion_visitas_real(self):
        amount = 0
        obcam = ObraEnCampanya.objects.filter(campanya=self.id)
        for oc in obcam:
            amount += oc.recaudacion_visitas_real
        return amount
    
    def calcula_recaudacion_prevista(self):
        return self.calcula_recaudacion_funciones_prevista() + self.calcula_recaudacion_visitas_prevista()
    
    def calcula_recaudacion_real(self):
        return self.calcula_recaudacion_funciones_real() + self.calcula_recaudacion_visitas_real()
    
    def __unicode__(self):
        return unicode(self.nombre)

    def save(self, *args, **kwargs):
        setattr(self, 'espectadores_previstos', self.calcula_espectadores_previstos())
        setattr(self, 'espectadores_reales', self.calcula_espectadores_reales())
        setattr(self, 'alumnos_previsto', self.calcula_alumnos_previstos())
        setattr(self, 'alumnos_real', self.calcula_alumnos_reales())
        setattr(self, 'maestros_previsto', self.calcula_maestros_previstos())
        setattr(self, 'maestros_real', self.calcula_maestros_reales())
        setattr(self, 'recaudacion_funciones_prevista', self.calcula_recaudacion_funciones_prevista())
        setattr(self, 'recaudacion_visitas_prevista', self.calcula_recaudacion_visitas_prevista())
        setattr(self, 'recaudacion_prevista', self.calcula_recaudacion_prevista())
        setattr(self, 'recaudacion_funciones_real', self.calcula_recaudacion_funciones_real())
        setattr(self, 'recaudacion_visitas_real', self.calcula_recaudacion_visitas_real())
        setattr(self, 'recaudacion_real', self.calcula_recaudacion_real())
        super(Campanya, self).save(*args, **kwargs) 

class Aforo_Campanya(Campanya):
    class Meta:
        proxy = True
        verbose_name=_(u'Espectadores por campaña')
        verbose_name_plural = _(u'Espectadores por campañas') 

class Recaudacion_Campanya(Campanya):
    class Meta:
        proxy = True
        verbose_name=_(u'Recaudación por campaña')
        verbose_name_plural = _(u'Recaudación por campañas') 
        
        
class ObraEnCampanya(models.Model):
    obra = models.ForeignKey(Obra, related_name='se_representa')
    campanya = models.ForeignKey(Campanya, related_name='se_representa')
    observaciones = models.CharField(_(u'Observaciones'), max_length=1000, help_text=_(u'Ejemplo: Disponible en castellano a partir de 100 espectadores.'), null=True, blank=True)
    espectadores_previstos = models.PositiveSmallIntegerField(_(u'Espectadores (previstos)'), validators = [MinValueValidator(0),], default=0)
    espectadores_reales = models.PositiveSmallIntegerField(_(u'Espectadores (reales)'), validators = [MinValueValidator(0),], default=0)
    alumnos_previsto = models.PositiveSmallIntegerField(_(u'Alumnos (previsto)'),  validators = [MinValueValidator(0),], default=0)
    maestros_previsto = models.PositiveSmallIntegerField(_(u'Maestros (previsto)'),  validators = [MinValueValidator(0),], default=0)
    alumnos_real = models.PositiveSmallIntegerField(_(u'Alumnos (real)'), validators = [MinValueValidator(0),], default=0)
    maestros_real = models.PositiveSmallIntegerField(_(u'Maestros (real)'), validators = [MinValueValidator(0),], default=0)
    recaudacion_funciones_prevista = models.FloatField(_(u'Recaudación funciones (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_funciones_real = models.FloatField(_(u'Recaudación funciones (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_visitas_prevista = models.FloatField(_(u'Recaudación actividades complementarias (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_visitas_real = models.FloatField(_(u'Recaudación actividades complementarias (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_prevista = models.FloatField(_(u'Total recaudación (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_real = models.FloatField(_(u'Total recaudación (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    
    class Meta:
        verbose_name=_(u'Obra')
        verbose_name_plural = _(u'Obras') 
        ordering=['campanya','obra']
    
    def centros_que_reservan(self):
        return Centro.objects.filter(reserva__funcion__obra_campanya = self.id).exclude(reserva__estado='x')
    centros_que_reservan.short_description = _(u'Centros que reservan una obra en campaña')
    
    def nombres_centros_que_reservan(self):
        centros = self.centros_que_reservan()
        result =''
        for c in centros:
            result += c.nombre 
            if c.poblacion:
                result += '  (' + c.poblacion.nombre + ')'
            result +=';\n'
        return result
    nombres_centros_que_reservan.short_description = _(u'Centros')
        
    def num_centros_que_reservan(self):
        return len(self.centros_que_reservan())
    num_centros_que_reservan.short_description = _(u'Número de centros')
    
    def calcula_espectadores_previstos(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.aforo_ocupado_previsto()
        return amount
        
    
    def calcula_espectadores_reales(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.aforo_ocupado_real()
        return amount
    
    def calcula_maestros_previstos(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.aforo_maestros_previsto
        return amount
        
    
    def calcula_maestros_reales(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.aforo_maestros_real
        return amount
    
    def calcula_alumnos_previstos(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.aforo_alumnos_previsto
        return amount
        
    
    def calcula_alumnos_reales(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.aforo_alumnos_real
        return amount
    
    
    def calcula_recaudacion_funciones_prevista(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.recaudacion_funcion_prevista
        return amount
    
    def calcula_recaudacion_funciones_real(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            amount += f.recaudacion_funcion_real
        return amount
    
    def calcula_recaudacion_visitas_prevista(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            reservas = Reserva.objects.filter(funcion=f.id)
            for r in reservas:
                amount += r.precio_visitas_previsto
        return amount
    
    def calcula_recaudacion_visitas_real(self):
        amount = 0
        funciones = Funcion.objects.filter(obra_campanya=self.id)
        for f in funciones:
            reservas = Reserva.objects.filter(funcion=f.id)
            for r in reservas:
                amount += r.precio_visitas_real
        return amount
    
    def calcula_recaudacion_prevista(self):
        return self.calcula_recaudacion_funciones_prevista() + self.calcula_recaudacion_visitas_prevista()
    calcula_recaudacion_prevista.short_description = _(u'Total (previsto) €')
    
    def calcula_recaudacion_real(self):
        return self.calcula_recaudacion_funciones_real() + self.calcula_recaudacion_visitas_real()
    calcula_recaudacion_real.short_description = _(u'Total (real) €')
    
    def ficha(self):
        return 'obra/ObraCampanya' + str(self.id) + '.pdf'
    
    def nombre(self):
        if self.observaciones:
            return _(u"%(obra)s se representa en %(campanya)s (Nota: %(observacion)s)") % {'obra':self.obra, 'campanya':self.campanya, 'observacion':self.observaciones}
        else:
            return _(u"%(obra)s se representa en %(campanya)s") % {'obra':self.obra, 'campanya':self.campanya}

    def __unicode__(self):
        return unicode(self.nombre())
    
    def save(self, *args, **kwargs):
        setattr(self, 'espectadores_previstos', self.calcula_espectadores_previstos())
        setattr(self, 'espectadores_reales', self.calcula_espectadores_reales())
        setattr(self, 'alumnos_previsto', self.calcula_alumnos_previstos())
        setattr(self, 'alumnos_real', self.calcula_alumnos_reales())
        setattr(self, 'maestros_previsto', self.calcula_maestros_previstos())
        setattr(self, 'maestros_real', self.calcula_maestros_reales())
        setattr(self, 'recaudacion_funciones_prevista', self.calcula_recaudacion_funciones_prevista())
        setattr(self, 'recaudacion_visitas_prevista', self.calcula_recaudacion_visitas_prevista())
        setattr(self, 'recaudacion_prevista', self.calcula_recaudacion_prevista())
        setattr(self, 'recaudacion_funciones_real', self.calcula_recaudacion_funciones_real())
        setattr(self, 'recaudacion_visitas_real', self.calcula_recaudacion_visitas_real())
        setattr(self, 'recaudacion_real', self.calcula_recaudacion_real())
        super(ObraEnCampanya, self).save(*args, **kwargs) 
        
class Aforo_ObraEnCampanya(ObraEnCampanya):
    class Meta:
        proxy = True
        verbose_name=_(u'Espectadores por obra')
        verbose_name_plural = _(u'Espectadores por obras') 

class Recaudacion_ObraEnCampanya(ObraEnCampanya):
    class Meta:
        proxy = True
        verbose_name=_(u'Recaudación por obra')
        verbose_name_plural = _(u'Recaudación por obras') 

        
class Funcion(models.Model):
    obra_campanya = models.ForeignKey(ObraEnCampanya, verbose_name=_(u'Obra y campaña'), null=True, blank=True)
    nombre_obra = models.CharField(_(u'Nombre obra'), max_length=250, null=True, blank=True)
    precio = models.FloatField(_(u'Precio por espectador €'), help_text=_(u'Dejar en blanco si es el precio habitual de la campaña.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    idioma = models.ForeignKey(Idioma, verbose_name=_(u'Idioma de la función'), help_text=_(u'Dejar en blanco si la obra se representa en un único idioma o no tiene idioma relevante (es un musical, espectáculo de mímica, etc)'), null=True, blank=True) 
    fecha_hora = models.DateTimeField(_(u'Día y hora'))
    aforo_total = models.PositiveSmallIntegerField(_(u'Aforo'), help_text=_(u'Dejar en blanco si el aforo coincide con el que se ha puesto en la obra'), validators = [MinValueValidator(0),], null=True, blank=True)
    aforo_extra = models.PositiveSmallIntegerField(_(u'Aforo extra'), help_text=_(u'Son butacas extra para poder hacer "overbooking" de reservas. Pon 0 si no tienes butacas extra.'), validators = [MinValueValidator(0),], default=20)
    aforo_libre_previsto = models.SmallIntegerField(_(u'Aforo libre (previsto)'), null=True, blank=True)
    aforo_alumnos_previsto = models.PositiveSmallIntegerField(_(u'Asistencia alumnos (previsto)'),  validators = [MinValueValidator(0),], default=0)
    aforo_maestros_previsto = models.PositiveSmallIntegerField(_(u'Asistencia maestros (previsto)'),  validators = [MinValueValidator(0),], default=0)
    aforo_libre_real = models.SmallIntegerField(_(u'Aforo libre (real)'), null=True, blank=True)
    aforo_alumnos_real = models.PositiveSmallIntegerField(_(u'Asistencia alumnos (real)'), validators = [MinValueValidator(0),], default=0)
    aforo_maestros_real = models.PositiveSmallIntegerField(_(u'Asistencia maestros (real)'), validators = [MinValueValidator(0),], default=0)
    recaudacion_funcion_prevista = models.FloatField(_(u'Recaudación función (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_visitas_prevista = models.FloatField(_(u'Recaudación actividades complementarias (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_prevista = models.FloatField(_(u'Recaudación Total (prevista) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_funcion_real = models.FloatField(_(u'Recaudación función (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_visitas_real = models.FloatField(_(u'Recaudación actividades complementarias (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    recaudacion_real = models.FloatField(_(u'Recaudación real (real) €'), validators = [MinValueValidator(0.0),], default=0.0) 
    entradas_visitas_guiadas_previsto = models.PositiveSmallIntegerField(_(u'Entradas actividades complementarias (previsto)'), validators = [MinValueValidator(0),], default=0)
    entradas_visitas_guiadas_real = models.PositiveSmallIntegerField(_(u'Entradas actividades complementarias (real)'), validators = [MinValueValidator(0),], default=0)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)

    class Meta:
        verbose_name=_(u'Función')
        verbose_name_plural = _(u'Funciones') 
        ordering=['obra_campanya__obra__nombre', 'fecha_hora',]
    
    def obra(self):
        return self.obra_campanya.obra.nombre
    obra.short_description= _(u'Obra')

    def companyia(self):
        return self.obra_campanya.obra.companyia.nombre
    companyia.short_description= _(u'Compañía')
    
    def aforo_ocupado_previsto(self):
        return self.aforo_alumnos_previsto + self.aforo_maestros_previsto
    aforo_ocupado_previsto.short_description = _(u'Asistencia prevista')

    def aforo_ocupado_real(self):
        return self.aforo_alumnos_real + self.aforo_maestros_real
    aforo_ocupado_real.short_description = _(u'Asistencia real')
    
    def color_status(self):
        if self.aforo_ocupado_previsto()==0:
            return 'blue'
        elif self.aforo_ocupado_previsto()<= self.aforo_total/2:
            return 'green'
        elif self.aforo_ocupado_previsto() >= self.aforo_total - 10:
            return 'red'
        else:
            return 'yellow'
            
    '''def aforo_libre_previsto(self):
        return self.aforo_total - self.aforo_ocupado_previsto()
    aforo_libre_previsto.short_description = _(u'Aforo libre (previsto)')

    def aforo_libre_real(self):
        return self.aforo_total - self.aforo_ocupado_real()
    aforo_libre_real.short_description = _(u'Aforo libre (real)')
    '''
    
    def calcular_recaudacion_visitas_prevista(self):
        return self.entradas_visitas_guiadas_previsto * self.obra_campanya.campanya.precio_visita_guiada
    #calcular_recaudacion_visitas_prevista.short_description = _(u'Recaudación visitas (prevista) €')
    
    def calcular_recaudacion_visitas_real(self):
        return self.entradas_visitas_guiadas_real * self.obra_campanya.campanya.precio_visita_guiada
    #calcular_recaudacion_visitas_real.short_description = _(u'Recaudación visitas (real) €')
    
    def calcular_recaudacion_funcion_prevista(self):
        if self.obra_campanya.campanya.los_maestros_no_pagan:
            # solo pagan los alumnos:
            return self.aforo_alumnos_previsto * self.precio
        else:
            return self.aforo_ocupado_previsto() * self.precio
    #calcular_recaudacion_funcion_prevista.short_description = _(u'Recaudación función (prevista) €')
    
    def calcular_recaudacion_funcion_real(self):
        if self.obra_campanya.campanya.los_maestros_no_pagan:
            # solo pagan los alumnos:
            return self.aforo_alumnos_real * self.precio
        else:
            return self.aforo_ocupado_real() * self.precio
    #calcular_recaudacion_funcion_real.short_description = _(u'Recaudación función (real) €')
    
    def calcular_recaudacion_prevista(self):
        return self.calcular_recaudacion_visitas_prevista() + self.calcular_recaudacion_funcion_prevista()
    calcular_recaudacion_prevista.short_description = _(u'Recaudación total (prevista) €')
    
    def calcular_recaudacion_real(self):
        return self.calcular_recaudacion_visitas_real() + self.calcular_recaudacion_funcion_real()
    
    #def ficha(self):
    #    return 'obra/funcio' + str(self.id) + '.pdf'
    
    def print_obra_hora(self):
        return self.obra_campanya.obra.nombre + ' ' + timezone.localtime(self.fecha_hora).strftime("%d/%m/%Y %H:%M")

    def print_aforo(self):
        '''now = timezone.localtime(timezone.now())
        if timezone.localtime(self.fecha_hora) > now:
            return unicode(_(u'Aforo libre previsto:')) + ' ' + str(self.aforo_libre_previsto)
        else:
            return unicode(_(u'Aforo ocupado real:')) + ' ' + str(self.aforo_ocupado_real)
        '''
        return unicode(_(u'Aforo libre previsto:')) + ' ' + str(self.aforo_libre_previsto)
    
    def __unicode__(self):
        if self.obra_campanya.obra.idiomas.all().count() > 1 and self.idioma:
            return self.print_obra_hora() + ' (' + self.idioma.nombre  + ') ' + self.print_aforo()
        else:#self.fecha_hora.strftime('%a %b %d %X %z')
            return self.print_obra_hora() + ' ' + self.print_aforo()
        
    
    def save(self, *args, **kwargs):
        val = getattr(self, 'precio', False)
        if not val:
            setattr(self, 'precio', self.obra_campanya.campanya.precio_obra)
        val = getattr(self, 'aforo_total', False)
        if not val:
            setattr(self, 'aforo_total', self.obra_campanya.obra.aforo_max)
        val = getattr(self, 'aforo_libre_previsto', False)
        setattr(self, 'aforo_libre_previsto', self.aforo_total - self.aforo_ocupado_previsto())
        val = getattr(self, 'aforo_libre_real', False)
        
        setattr(self, 'aforo_libre_real', self.aforo_total - self.aforo_ocupado_real())
        val = getattr(self, 'idioma', False)
        if not val:
            if self.obra_campanya.obra.idiomas.all().count() > 0:
                setattr(self, 'idioma', self.obra_campanya.obra.idiomas.all()[0])
        setattr(self, 'recaudacion_funcion_prevista', self.calcular_recaudacion_funcion_prevista())
        setattr(self, 'recaudacion_visitas_prevista', self.calcular_recaudacion_visitas_prevista())
        setattr(self, 'recaudacion_prevista', self.calcular_recaudacion_prevista())
        setattr(self, 'recaudacion_funcion_real', self.calcular_recaudacion_funcion_real())
        setattr(self, 'recaudacion_visitas_real', self.calcular_recaudacion_visitas_real())
        setattr(self, 'recaudacion_real', self.calcular_recaudacion_real())
        setattr(self, 'nombre_obra', self.obra())
        super(Funcion, self).save(*args, **kwargs) 

class Aforo_Funcion(Funcion):
    class Meta:
        proxy = True
        verbose_name=_(u'Espectadores por función')
        verbose_name_plural = _(u'Espectadores por funciones') 

class Recaudacion_Funcion(Funcion):
    class Meta:
        proxy = True
        verbose_name=_(u'Recaudación por función')
        verbose_name_plural = _(u'Recaudación por funciones') 

class Curso(models.Model):
    nombre = models.CharField(_(u'Nombre'), max_length=250)
#    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))

    class Meta:
        verbose_name=_(u'Curso Escolar')
        verbose_name_plural = _(u'Cursos Escolares') 
        ordering=['nombre']

    def __unicode__(self):
        return self.nombre

class Reserva(models.Model):
    PENDIENTE = 'p'
    CONFIRMADA = 'c'
    CANCELADA = 'x'
    
    ESTADO_TIPO = (
        (PENDIENTE, _(u'PENDIENTE')),
        (CONFIRMADA, _(u'CONFIRMADA')),
        (CANCELADA, _(u'CANCELADA')),
    )

    EFECTIVO = 'p'
    TRANSFERENCIA = 'c'
    
    TIPO_PAGO = (
        (EFECTIVO, _(u'EFECTIVO')),
        (TRANSFERENCIA, _(u'TRANSFERENCIA')),
    )
    responsable = models.ForeignKey(Persona_contacto, verbose_name=_(u'Responsable de la reserva'), on_delete=models.SET_NULL, null=True, blank=True,)
    centro = models.ForeignKey(Centro, verbose_name=_(u'Centro'), null=True, blank=True, help_text=_(u'Rellenar únicamente si no hay una persona de contaco responsable asignada a la reserva.'))
    funcion = models.ForeignKey(Funcion, verbose_name=_(u'Función'))
    estado = models.CharField(_(u'Estado'), max_length=1, choices=ESTADO_TIPO, default=PENDIENTE)
    alumnos_previsto = models.PositiveSmallIntegerField(_(u'Nº de alumnos (previsto)'), validators = [MinValueValidator(0),], default=0)
    maestros_previsto = models.PositiveSmallIntegerField(_(u'Nº de maestros (previsto)'), validators = [MinValueValidator(0),], default=0)
    alumnos_real = models.PositiveSmallIntegerField(_(u'Nº de alumnos (real)'), validators = [MinValueValidator(0),], default=0)
    maestros_real = models.PositiveSmallIntegerField(_(u'Nº de maestros (real)'), validators = [MinValueValidator(0),], default=0)
    observaciones = models.CharField(_(u'Observaciones'), max_length=1000, help_text=_(u'Ejemplos: Ciclo de infantil en valenciano. Llamar el viernes para confirmar.'), null=True, blank=True)
    precio_previsto = models.FloatField(_(u'Total a pagar (previsto) €'), help_text=_(u'Calculado automáticamente.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    precio_real = models.FloatField(_(u'Total abonado (real) €'), help_text=_(u'Calculado automáticamente.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    precio_funcion_previsto = models.FloatField(_(u'A pagar por la función (previsto) €'), help_text=_(u'Calculado automáticamente.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    precio_funcion_real = models.FloatField(_(u'Pagado por la función (real) €'), help_text=_(u'Calculado automáticamente.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    precio_visitas_previsto = models.FloatField(_(u'A pagar por las actividades complementarias (previsto) €'), help_text=_(u'Calculado automáticamente.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    precio_visitas_real = models.FloatField(_(u'Pagado por las actividades complementarias (real) €'), help_text=_(u'Calculado automáticamente.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    reenviar_mail = models.BooleanField(_(u'Reenviar correo a centro'), help_text=_(u'Marca esta casilla si necesitas que el centro reciba un nuevo correo con la información de su reserva. Esto es recomendable si has hecho cambios después de que se mande el primer correo. Por ejemplo: si has modificado el número de asistentes, o la fecha y hora de la función.'), default=False)
    mail_pendiente_enviado = models.BooleanField(_(u'Correo de reserva pendiente enviado'), help_text=_(u'Indica si el sistema ha enviado un correo al centro sobre su reserva en estado PENDIENTE. Los correos se mandan cada mañana a las 8.30h.'), default=False)
    mail_confirmado_enviado = models.BooleanField(_(u'Correo de reserva confirmada enviado'), help_text=_(u'Indica si el sistema ha enviado un correo al centro sobre su reserva en estado CONFIRMADA. Los correos se mandan cada mañana a las 8.30h.'), default=False)
    mail_cancelado_enviado = models.BooleanField(_(u'Correo de reserva cancelada enviado'), help_text=_(u'Indica si el sistema ha enviado un correo al centro sobre su reserva en estado CANCELADA. Los correos se mandan cada mañana a las 8.30h.'), default=False)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)

    visita_guiada = models.BooleanField(_(u'¿Hay actividades complementarias?'), default=False)
    descripcion_visita_guiada = models.CharField(_(u'Descripción de las actividades complementarias'), max_length=1000, help_text=_(u'Ejemplos: Visita guiada por el teatro, etc.'), null=True, blank=True)

    tipo_pago = models.CharField(_(u'Tipo de pago'), max_length=1, choices=TIPO_PAGO, null=True, blank=True)
    pago_en = models.DateTimeField(_(u'Fecha pago'), help_text=_(u'Fecha cuando se efectuará/efectuó el pago.'), null=True, blank=True)
    datos_fiscales = models.CharField(_(u'Datos fiscales para la factura'), max_length=1000, null=True, blank=True)

    precio_funcion_personalizado = models.FloatField(_(u'Precio personalizado de la función €'), help_text=_(u'Dejar en blanco si es el precio habitual de la campaña o función.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    precio_visitas_personalizado = models.FloatField(_(u'Precio personalizado de las actividades complementarias €'), help_text=_(u'Dejar en blanco si es el precio habitual de la campaña o función.'),validators = [MinValueValidator(0.0),], null=True, blank=True)
    cursos = models.ManyToManyField(Curso, verbose_name=_(u'Cursos Escolares'), blank=True)

    def nombre_obra(self):
        return self.funcion.obra_campanya.obra.nombre
    
    def funcion_nombre(self):
        return self.funcion.__unicode__()
        
    def fecha_hora(self):
        return self.funcion.fecha_hora

        
    def total_previsto(self):
        return self.alumnos_previsto + self.maestros_previsto
    total_previsto.short_description = _(u'Total espectadores previstos')    

    def total_real(self):
        return self.alumnos_real + self.maestros_real
    total_real.short_description = _(u'Total espectadores reales')    

    def telefono(self):
        if self.responsable:
            return self.responsable.telefono
        else:
            return self.centro.telefono
    telefono.short_description = _(u'Teléfono')     
     
    def email(self):
        if self.responsable and self.responsable.email:
            return self.responsable.email
        else:
            return self.centro.email
    email.short_description = _(u'email')     
    
    def print_asistencia(self):
        return unicode(_(u'Asistencia prevista:')) + ' ' + str(self.total_previsto())
        '''
        now = timezone.localtime(timezone.now())
        if timezone.localtime(self.fecha_hora) > now:
            return unicode(_(u'Asistencia prevista:')) + ' ' + str(self.total_previsto())
        else:
            return unicode(_(u'Asistencia real:')) + ' ' + str(self.total_real())
        '''
    def print_obra_hora(self):
        return self.funcion.print_obra_hora()
    
    def nombre(self):
        return self.print_obra_hora() + ' - ' + self.centro.nombre + ': ' + self.print_asistencia() + ' (' + self.get_estado_display() + ')'
        
    
    class Meta:
        verbose_name=_(u'Reserva')
        verbose_name_plural = _(u'Reservas') 
        ordering=['centro__nombre','-funcion__fecha_hora',]
    
    
    def __unicode__(self):
        return self.nombre()
        
    def save(self, *args, **kwargs): 
        precio_funcion_previsto = 0
        precio_funcion_real = 0
        precio_visitas_previsto = 0
        precio_visitas_real = 0
        
        if self.funcion.obra_campanya.campanya.los_maestros_no_pagan:
            # Solo pagan los alumnos:
            precio_funcion_previsto = self.funcion.precio * self.alumnos_previsto
        else:
            precio_funcion_previsto = self.funcion.precio * self.total_previsto()
        if self.visita_guiada:
            if self.funcion.obra_campanya.campanya.los_maestros_no_pagan:
                # Solo pagan los alumnos:   
                precio_visitas_previsto = self.funcion.obra_campanya.campanya.precio_visita_guiada * self.alumnos_previsto
            else:
                precio_visitas_previsto = self.funcion.obra_campanya.campanya.precio_visita_guiada * self.total_previsto()
        setattr(self, 'precio_funcion_previsto', precio_funcion_previsto)
        setattr(self, 'precio_visitas_previsto', precio_visitas_previsto)
        setattr(self, 'precio_previsto', precio_funcion_previsto + precio_visitas_previsto)
        
        if self.funcion.obra_campanya.campanya.los_maestros_no_pagan:
            # Solo pagan los alumnos:
            precio_funcion_real = self.funcion.precio * self.alumnos_real
        else:
            precio_funcion_real = self.funcion.precio * self.total_real()
        
        if self.visita_guiada:
            if self.funcion.obra_campanya.campanya.los_maestros_no_pagan:
                # Solo pagan los alumnos:   
                precio_visitas_real = self.funcion.obra_campanya.campanya.precio_visita_guiada * self.alumnos_real
            else:
                precio_visitas_real = self.funcion.obra_campanya.campanya.precio_visita_guiada * self.total_real()
                
        setattr(self, 'precio_funcion_real', precio_funcion_real)
        setattr(self, 'precio_visitas_real', precio_visitas_real)
        setattr(self, 'precio_real', precio_funcion_real + precio_visitas_real)
        
        val = getattr(self, 'responsable', False)
        if val:
            setattr(self, 'centro', self.responsable.centro)
        super(Reserva, self).save(*args, **kwargs) 
    
        
class CorreoConfirmacionReserva(models.Model):
    ASUNTO_1 = '1'
    ASUNTO_2 = '2'
    ASUNTO_3 = '3'
    
    ASUNTOS = (
        (ASUNTO_1, _(u'Reserva para [nombre de la obra] creada ([ESTADO]) ')),
        (ASUNTO_2, _(u'Su reserva para la obra [nombre de la obra] [ESTADO]')),
        (ASUNTO_3, _(u'Reserva [ESTADO] para obra [nombre de la obra]')),
    )
    
    SALUDO_1 = '1'
    SALUDO_2 = '2'
    
    DESTINATARIO = (
        (SALUDO_1, _(u'[responsable de reserva] ([Nombre de centro] si no hay un responsable)')),
        (SALUDO_2, _(u'[Nombre de centro]')),
    )
    
    DETALLE_1 = '1'
    DETALLE_2 = '2'
    
    DETALLE_RESERVA = (
        (DETALLE_1, _(u'Textual')),
        (DETALLE_2, _(u'Tabular')),
    )
    
    PENDIENTE = 'p'
    CONFIRMADA = 'c'
    CANCELADA = 'x'
    
    ESTADO_TIPO = (
        (PENDIENTE, _(u'PENDIENTE')),
        (CONFIRMADA, _(u'CONFIRMADA')),
        (CANCELADA, _(u'CANCELADA')),
    )

    BORRADOR = 'b'
    LISTO = 'l'
    
    
    
    #obra_campanya = models.ForeignKey(ObraEnCampanya, verbose_name=_(u'Obra'), related_name='en_campanya')
    activo = models.BooleanField(_(u'Correo activo'), help_text=_(u'Indica si un correo está listo para mandar (activo) o se encuentra en estado borrador (desactivo)'), default=True)
    estado = models.CharField(_(u'Estado de la reserva'), max_length=1, help_text=_(u'Elige el estado de reserva para el que vas a redactar el correo.'), choices=ESTADO_TIPO, default=PENDIENTE)
    asunto = models.CharField(_(u'Asunto'), help_text=_(u'Elige la estructura del asunto que tendrá el correo. Las casillas entre [corchetes] se adaptarán a los datos de cada espectáculo.'), max_length=1, choices=ASUNTOS, default=ASUNTO_1)
    saludo = models.CharField(_(u'Saludo'), help_text=_(u'Fórmula de cortesía para dirigirse a la persona/centro que ha hecho la reserva: Estimado/a, Querido/a, etc'), max_length=100, null=True, blank=True)
    destinatario = models.CharField(_(u'Destinatario'), help_text=_(u'Elige el nombre a quien va dirigido el correo: Persona de contacto o Centro'),max_length=1, choices=DESTINATARIO, default=SALUDO_1)
    cuerpo_pre_detalle = models.CharField(_(u'Cuerpo: antes del detalle de la reserva'), help_text=_(u'Ejemplo: Su reserva se ha registrado correctamente. Abajo puede encontrar el detalle de su reserva.'), max_length=3000, null=True, blank=True) 
    detalle_reserva = models.CharField(_(u'Detalle de la reserva'), max_length=1, choices=DETALLE_RESERVA, default=DETALLE_1)
    cuerpo_post_detalle = models.CharField(_(u'Cuerpo: después del detalle de la reserva'),help_text=_(u'Ejemplo: Recuerde que debe asistir 30 minutos antes de la función. Si tiene cualquier duda, escríbanos a miteatro@teatro.com o llámenos al +0123456789.'), max_length=3000, null=True, blank=True) 
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    class Meta:
        verbose_name=_(u'Correo de estado de reserva')
        verbose_name_plural = _(u'Correos de estado de reservas')   

    def nombre(self):
        
        return unicode(_(u'Correo para reservas en estado:')) + ' ' + self.get_estado_display() 
        
    '''def hay_adjuntos(self): 
        return AdjuntoCorreoCampanya.objects.filter(correo=self.id).count() > 0
    hay_adjuntos.admin_order_field='campanya'
    hay_adjuntos.boolean=True
    hay_adjuntos.short_description=_(u'Hay adjuntos')
    '''
    
    def __unicode__(self):
        return self.get_estado_display() + ': ' + self.get_asunto_display()


class Adjunto(models.Model):
    nombre = models.CharField(_(u'Nombre'), max_length=600, help_text=_(u'Usa un nombre identificativo. Ejemplo: Dossier didáctico obra X. Cuestionario sobre obra X. Términos de contratación campanya 2015-20116, etc.')) 
    fichero = models.FileField(upload_to=u'adjuntos', verbose_name=_(u'Adjunto'))
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    def nombre_correos(self):
        return ";\n".join([ac.correo.nombre() for ac in AdjuntoCorreo.objects.filter(adjunto=self.id)])
    nombre_correos.short_description= _(u'Correos')
    
    def filename(self):
        return os.path.basename(self.fichero.name)
    
    class Meta:
        verbose_name=_(u'Adjunto')
        verbose_name_plural = _(u'Adjunto') 
        ordering=['nombre']  
    
    def __unicode__(self):
        return unicode(self.nombre)    
        
    
class CorreoReserva(models.Model):
    ASUNTO_1 = '1'
    ASUNTO_2 = '2'
    ASUNTO_3 = '3'
    
    ASUNTOS = (
        (ASUNTO_1, _(u'Reserva para [nombre de la obra] el [fecha hora]')),
        (ASUNTO_2, _(u'Su reserva para la obra [nombre de la obra] el [fecha hora]')),
        (ASUNTO_3, _(u'Reserva del [fecha hora] para obra [nombre de la obra]')),
    )
    
    SALUDO_1 = '1'
    SALUDO_2 = '2'
    
    DESTINATARIO = (
        (SALUDO_1, _(u'[responsable de reserva] ([Nombre de centro] si no hay un responsable)')),
        (SALUDO_2, _(u'[Nombre de centro]')),
    )
    
    DETALLE_1 = '1'
    DETALLE_2 = '2'
    
    DETALLE_RESERVA = (
        (DETALLE_1, _(u'Textual')),
        (DETALLE_2, _(u'Tabular')),
    )
    
    HORA_ENVIO = (
        ('0:0', '00:00h'),
        ('0:30', '00:30h'),
        ('1:0', '01:00h'),
        ('1:30', '01:30h'),
        ('2:0', '02:00h'),
        ('2:30', '02:30h'),
        ('3:0', '03:00h'),
        ('3:30', '03:30h'),
        ('4:0', '04:00h'),
        ('4:30', '04:30h'),
        ('5:0', '05:00h'),
        ('5:30', '05:30h'),
        ('6:0', '06:00h'),
        ('6:30', '06:30h'),
        ('7:0', '07:00h'),
        ('7:30', '07:30h'),
        ('8:0', '08:00h'),
        ('8:30', '08:30h'),
        ('9:0', '09:00h'),
        ('9:30', '09:30h'),
        ('10:0', '10:00h'),
        ('10:30', '10:30h'),
        ('11:0', '11:00h'),
        ('11:30', '11:30h'),
        ('12:0', '12:00h'),
        ('12:30', '12:30h'),
        ('13:0', '13:00h'),
        ('13:30', '13:30h'),
        ('14:0', '14:00h'),
        ('14:30', '14:30h'),
        ('15:0', '15:00h'),
        ('15:30', '15:30h'),
        ('16:0', '16:00h'),
        ('16:30', '16:30h'),
        ('17:0', '17:00h'),
        ('17:30', '17:30h'),
        ('18:0', '18:00h'),
        ('18:30', '18:30h'),
        ('19:0', '19:00h'),
        ('19:30', '19:30h'),
        ('20:0', '20:00h'),
        ('20:30', '20:30h'),
        ('21:0', '21:00h'),
        ('21:30', '21:30h'),
        ('22:0', '22:00h'),
        ('22:30', '22:30h'),
        ('23:0', '23:00h'),
        ('23:30', '23:30h'),
    )
    
    activo = models.BooleanField(_(u'Correo activo'), help_text=_(u'Indica si un correo está listo para mandar (activo) o se encuentra en estado borrador (desactivo)'), default=True)
    obra_campanya = models.ForeignKey(ObraEnCampanya, verbose_name=_(u'Obra'), help_text=_(u'Solo se muestran obras que tienen funciones'), related_name='en_campanya')
    dias = models.PositiveSmallIntegerField(_(u'Días'), help_text=_(u'Número de días antes o después de una función en que se mandará el correo. El correo se enviará a cada reserva de cada una de las funciones de la obra.'), validators = [MinValueValidator(0),], default=1,)
    hora = models.CharField(_(u'Hora de envío'), help_text=_(u'Elige la hora a la que se mandará el correo.'), max_length=5, choices=HORA_ENVIO, default='8:30')
    antes = models.BooleanField(_(u'Antes de la función'), help_text=_(u'Marcar si el correo se manda antes de la función. Desmarcar si se quiere mandar el correo tras la función (por ejemplo, un correo de agradecimiento o un correo pidiendo una valoración a los espectadores)'), default=True)
    asunto = models.CharField(_(u'Asunto'), help_text=_(u'Elige la estructura del asunto que tendrá el correo. Las casillas entre [corchetes] se adaptarán a los datos de cada espectáculo.'), max_length=1, choices=ASUNTOS, default=ASUNTO_1)
    saludo = models.CharField(_(u'Saludo'), help_text=_(u'Fórmula de cortesía para dirigirse a la persona/centro que ha hecho la reserva: Estimado/a, Querido/a, etc'), max_length=100, null=True, blank=True)
    destinatario = models.CharField(_(u'Destinatario'), help_text=_(u'Elige el nombre a quien va dirigido el correo: Persona de contacto o Centro'),max_length=1, choices=DESTINATARIO, default=SALUDO_1)
    cuerpo_pre_detalle = models.CharField(_(u'Cuerpo: antes del detalle de la reserva'), help_text=_(u'Ejemplo: Su reserva se ha registrado correctamente. Abajo puede encontrar el detalle de su reserva.'), max_length=3000, null=True, blank=True) 
    detalle_reserva = models.CharField(_(u'Detalle de la reserva'), max_length=1, choices=DETALLE_RESERVA, default=DETALLE_1)
    show_detalle = models.BooleanField(_(u'Mostrar detalle de reserva'), help_text=_(u'Marcar si se quiere mostrar el detalle de la reserva en el correo. Desmarcar para que no se incluya.'), default=True)
    cuerpo_post_detalle = models.CharField(_(u'Cuerpo: después del detalle de la reserva'),help_text=_(u'Ejemplo: Recuerde que debe asistir 30 minutos antes de la función. Si tiene cualquier duda, escríbanos a miteatro@teatro.com o llámenos al +0123456789.'), max_length=3000, null=True, blank=True) 
    adjuntos = models.ManyToManyField(Adjunto, verbose_name=_(u'Adjuntos'), through='AdjuntoCorreo')
    owner = models.ForeignKey('Teatro', verbose_name=_(u'Dueño'))
    
    class Meta:
        verbose_name=_(u'Correo recordatorio')
        verbose_name_plural = _(u'Correos recordatorios')  
        ordering=['obra_campanya__obra__nombre','antes','-dias',]

    
    def hay_adjuntos(self): 
        return self.adjuntos.all().count() > 0
    hay_adjuntos.admin_order_field='obra_campanya'
    hay_adjuntos.boolean=True 
    hay_adjuntos.short_description=_(u'Hay adjuntos')
    
    def nombre(self):
        if self.antes:
            return self.obra_campanya.nombre() + u': ' + str(self.dias) + ' ' + unicode(_(u'días antes'))
        else:
            return self.obra_campanya.nombre() + u': ' + str(self.dias) + ' ' + unicode(_(u'días después'))
            
    def __unicode__(self):
        return self.nombre()
        
    


class AdjuntoCorreo(models.Model):
    adjunto = models.ForeignKey(Adjunto, related_name='es_adjunto_de')
    correo = models.ForeignKey(CorreoReserva, related_name='tiene_adjunto')
     
    class Meta:
        verbose_name=_(u'Adjunto')
        verbose_name_plural = _(u'Adjuntos') 
        ordering=['adjunto__nombre','correo']

    def __unicode__(self):
        return unicode(self.adjunto.nombre) + ' ' + unicode(_('adjunto en')) + ' '  + self.correo.nombre()

        
class LogCorreosRecordatorios(models.Model):
    reserva = models.ForeignKey(Reserva, related_name='ha_recibido_correo_recordatorio')
    correo = models.ForeignKey(CorreoReserva, related_name='es_enviado_recordando')
    enviado_en = models.DateTimeField(_(u'Fecha envío'), auto_now_add=True)
    
    
    class Meta:
        verbose_name=_(u'Registro: Correo recodatorio enviado')
        verbose_name_plural = _(u'Registro: Correos recordatorio enviados') 
        ordering=['-enviado_en', 'reserva', 'correo',]
    
    def destinatario(self):
        return self.reserva.email()
    destinatario.short_description= _(u'Destinatario')
    
    def __unicode__(self):
        return self.correo.nombre() + ' ' + unicode(_('sobre:')) + ' ' + self.reserva.nombre() +  ' ' + unicode(_('para:')) + ' ' + self.destinatario()        
    

class LogCorreosEstadoReserva(models.Model):
    reserva = models.ForeignKey(Reserva, related_name='ha_recibido_correo_confirmacion')
    correo = models.ForeignKey(CorreoConfirmacionReserva, related_name='es_enviado_confirmando')
    enviado_en = models.DateTimeField(_(u'Fecha envío'), auto_now_add=True)
    
    
    class Meta:
        verbose_name=_(u'Registro: Correo de estado de reserva enviado')
        verbose_name_plural = _(u'Registro: Correos de estado de reserva enviados') 
        ordering=['-enviado_en', 'reserva', 'correo',]
    
    def destinatario(self):
        return self.reserva.email()
    destinatario.short_description= _(u'Destinatario')
    
    def __unicode__(self):
        return self.correo.nombre() + ' ' + unicode(_('sobre:')) + ' ' + self.reserva.nombre() +  ' ' + unicode(_('para:')) + ' ' + self.destinatario()        
        

class Teatro_contacto(models.Model):

    nombre = models.CharField(_(u'Nombre'), max_length=400)
    email = models.EmailField(_(u'Email'))
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(u"El número de teléfono tiene que tener el formato: '+1234567890' y una longitud máxima de 15 caracteres.")) 
    telefono = models.CharField(validators=[phone_regex], verbose_name=_(u'Teléfono de contacto'), max_length=16, blank=True, null=True)
    espacio_cultural = models.CharField(_(u'Nombre de tu teatro'), max_length=400)
    cargo = models.CharField(_(u'Cargo'), max_length=400)
    creado = models.DateTimeField(_(u'Fecha añadido'), auto_now_add= True)
    modificado = models.DateTimeField(_(u'Fecha modificado'), auto_now = True)
    
    class Meta:
        verbose_name=_(u'Contacto teatro')
        verbose_name_plural = _(u'Contactos de teatro') 
        ordering=['nombre', ]
    
    
    def __unicode__(self):
        return unicode(self.nombre) + ': ' + unicode(self.email)

        
####### THIS ALLOWS TO HAVE SEVERAL ADMINS IN DJANGO-SUIT
suit_multi_admin()

