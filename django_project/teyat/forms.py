# -*- coding: utf-8 -*-
from django import forms
from teyat.models import Teatro_contacto
from django.utils.translation import ugettext_lazy as _


class Teatro_contacto_Form(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(Teatro_contacto_Form, self).__init__(*args, **kwargs)
        self.fields['nombre'].required = True
        self.fields['email'].required = True
        self.fields['telefono'].required = False
        self.fields['cargo'].required = True
        self.fields['espacio_cultural'].required = True
    
    class Meta:
        model = Teatro_contacto

        
        exclude =('modificado','creado')

class RegistrationForm(Teatro_contacto_Form):
    error_css_class = 'error'
    required_css_class = 'required'

class RegistrationFormTermsOfService(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds a required checkbox
    for agreeing to a site's Terms of Service.

    """
    tos = forms.BooleanField(
        label= _(u'He leído y acepto  <a href="http://teyat.es/nota-legal#TOS" target="_blank">las condiciones de uso</a> de <span class="text-teyat">teyat</span> *'),
        widget=forms.CheckboxInput,
        error_messages={
            'required': _('Debes aceptar las condiciones de uso para poder registrarte')
        })