# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.forms import ModelForm, TextInput, Textarea, ValidationError, ModelChoiceField, ModelMultipleChoiceField
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms.models import BaseInlineFormSet
from modeltranslation.admin import TranslationAdmin
from teyat.models import Comarca, Centro, Persona_contacto, Poblacion, Companyia, Teatro_contacto
from teyat.models import Campanya, Aforo_Campanya, Recaudacion_Campanya
from teyat.models import ObraEnCampanya, Aforo_ObraEnCampanya, Recaudacion_ObraEnCampanya
from teyat.models import Obra, Idioma, Funcion, Aforo_Funcion, Recaudacion_Funcion, Reserva, Teatro, Curso
from teyat.models import AdjuntoCorreo, CorreoConfirmacionReserva, Adjunto , CorreoReserva, LogCorreosRecordatorios, LogCorreosEstadoReserva
from datetime import datetime, date, timedelta as td
from django.utils import timezone
from suit.admin import SortableTabularInline
from suit.admin import SortableModelAdmin
from suit.widgets import SuitDateWidget, SuitSplitDateTimeWidget
from django_project.settings import BASE_DIR
from django.core.mail import send_mail
from django.core.mail import send_mass_mail
from django.template.loader import render_to_string
import csv
from django.http import HttpResponse
from rules.contrib.admin import ObjectPermissionsModelAdmin, ObjectPermissionsStackedInline, ObjectPermissionsTabularInline
from django import forms
from django.db import models
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group
from django_markdown.widgets import MarkdownWidget
from django_select2.forms import ModelSelect2Widget
from import_export.admin import ExportMixin
from import_export import resources
from import_export import fields
from django.contrib.admin.actions import delete_selected
from django.contrib.admin.views.main import ChangeList
from admin_stats.admin import StatsAdmin, Avg, Sum
from django.contrib.admin.models import LogEntry

# ADMIN FOR TEATRE RAVAL
class RavalAdminSite(AdminSite):
    site_header = 'raval'
    
ravaladmin = RavalAdminSite(name='raval')
    
# ADMIN FOR TEYAT STAFF
class StaffAdminSite(AdminSite):
    site_header = 'staff'

    
staffadmin = StaffAdminSite(name='staff')
staffadmin.register(User, UserAdmin)
staffadmin.register(Group, GroupAdmin)

# ADMIN FOR USER ACTIONS:
class LogAdmin(admin.ModelAdmin):
    """Create an admin view of the history/log table"""
    list_display = ('action_time','user','content_type','change_message','is_addition','is_change','is_deletion')
    list_filter = ['action_time','user','content_type']
    ordering = ('-action_time',)
    readonly_fields = [ 'user','content_type','object_id','object_repr','action_flag','change_message']
    
    #We don't want people changing this historical record:
    def has_add_permission(self, request):
        return False
    def has_change_permission(self, request, obj=None):
        #returning false causes table to not show up in admin page :-(
        #I guess we have to allow changing for now
        return True
    def has_delete_permission(self, request, obj=None):
        return False

staffadmin.register(LogEntry, LogAdmin)

# ADMIN FOR SUSCRIPTORS STAFF
class TeyatAdminSite(AdminSite):
    site_header = 'admin'
    
teyatadmin = TeyatAdminSite(name='admin')
#teyatadmin.register(User, UserAdmin)
    
### SELECT2 WIDGETS ###

class FuncionWidget(ModelSelect2Widget):
    search_fields = ['nombre_obra__icontains', ]

class ObraCampanyaWidget(ModelSelect2Widget):
    search_fields = ['obra__nombre__icontains', 'campanya__nombre__icontains']
    
class ObraWidget(ModelSelect2Widget):
    search_fields = ['nombre__icontains', ]

class CampanyaWidget(ModelSelect2Widget):
    search_fields = ['nombre__icontains', ]
    
class CentroWidget(ModelSelect2Widget):
    search_fields = ['nombre__icontains',]
    
class PersonaContactoWidget(ModelSelect2Widget):
    search_fields = ['nombre__icontains', 'centro__nombre__icontains',]
    
class CompanyiaWidget(ModelSelect2Widget):
    search_fields = ['nombre__icontains',]

class PoblacionWidget(ModelSelect2Widget):
    search_fields = ['nombre__icontains',]

class ComarcaWidget(ModelSelect2Widget):
    search_fields = ['nombre__icontains',]
 

if 'prod' in BASE_DIR:
    base_url = 'http://teyat.es/'
else:
    base_url = 'http://teyat.es:9999/'


####################
##  TEATRO        ##
####################
class TeatroResource(resources.ModelResource):

    class Meta:
        model = Teatro
        fields = ( 'nombre', 'direccion', 'ciudad__nombre','email','website','telefono','aforo', 'emailcampanya','idioma_mails','users')
        export_order = ( 'nombre', 'direccion', 'ciudad__nombre','email','website','telefono','aforo', 'emailcampanya','idioma_mails','users')
    
    def dehydrate_users(self, teatro):
        return unicode(";\n".join([u.username for u in teatro.users.all()]))

#MyModelForm = select2_modelform(Teatro, form_class=forms.ModelForm)
        
class Teatro_Form(ModelForm):
    
    class Meta:
        model = Teatro 
        widgets = {
                'ciudad': PoblacionWidget(attrs={'class': 'span4'}),
                }
        exclude = ['contacto',]


class Teatro_Admin(admin.ModelAdmin):
    #resource_class = TeatroResource
    
    model = Teatro
    form = Teatro_Form
    list_display=('nombre', 'email','telefono','usuarios',)
    filter_horizontal= ('users',)
    ordering=('nombre',)
    #search_fields=('nombre','email', 'telefono',)
    readonly_fields=('users',)
    exclude=('contacto',)
    
    fieldsets = [
                (_(u'Nombre y dirección'), {
                        'fields': ['nombre','direccion', 'ciudad',] 
                }),
                (_(u'Datos de contacto'), {
                        'fields': ['email', 'website', 'telefono']
                }),
                (_(u'Aforo'), {
                        'fields': ['aforo',]
                }),
                (_(u'Logo'), {
                        'fields': ['logo_bn','logo_nb']
                }),
                (_(u'Mailing con clientes'), {
                        'fields': ['emailcampanya','idioma_mails']
                }),
                (_(u'Usuarios'), {
                        'fields': ['users']
                }),
        ]
    
    def get_queryset(self, request):
        qs = super(Teatro_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(nombre=request.user.teatro_set.first().nombre)
        
    def get_form(self, request, obj=None, **kwargs):
        form = super(Teatro_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        if not request.user.is_superuser:
            form.base_fields['ciudad'].queryset = form.base_fields['ciudad'].queryset.filter(owner__in=request.user.teatro_set.all())
        
        return form
 
class Teatro_Admin_staff(ExportMixin, admin.ModelAdmin):

    resource_class = TeatroResource
    model = Teatro
    list_display=('id','nombre', 'email','telefono','usuarios','creado')
    filter_horizontal= ('users',)
    ordering=('nombre',)
    #search_fields=('nombre','email', 'telefono',)
    
    def get_queryset(self, request):
        qs = super(Teatro_Admin_staff, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(nombre=request.user.teatro_set.first().nombre)

        
#admin.site.register(Teatro, Teatro_Admin)
staffadmin.register(Teatro, Teatro_Admin_staff)
teyatadmin.register(Teatro, Teatro_Admin)

class Teatro_contacto_Resource(resources.ModelResource):

    class Meta:
        model = Teatro_contacto
        fields = ( 'nombre', 'email', 'telefono','espacio_cultural','cargo','creado',)
        export_order = ( 'nombre', 'email', 'telefono','espacio_cultural','cargo','creado',)
   
class Teatro_contacto_Admin(ExportMixin, admin.ModelAdmin):
    resource_class = Teatro_contacto_Resource
    
    model = Teatro_contacto
    list_display=('nombre', 'email', 'telefono', 'espacio_cultural', 'cargo', 'creado')
    list_filter=('nombre','email', 'creado',)
    search_fields=('nombre', 'email','telefono')
    

#admin.site.register(Teatro_contacto, Teatro_contacto_Admin)
staffadmin.register(Teatro_contacto, Teatro_contacto_Admin)
#teyatadmin.register(Teatro_contacto, Teatro_contacto_Admin)

    
    
#############################
##   RESERVAS Y FUNCIONES  ##
#############################
class ReservaResource(resources.ModelResource):
    campanya = fields.Field()
    obra = fields.Field()
    actividades_complementarias = fields.Field()
    cursos = fields.Field()

    class Meta:
        model = Reserva
        fields = ( 'campanya', 'obra', 'funcion__fecha_hora', 'actividades_complementarias',
                    'responsable__nombre','responsable__email','responsable__telefono',
                    'centro__nombre','centro__email','centro__telefono',
                    'observaciones',
                    'alumnos_previsto','maestros_previsto',
                    'precio_visitas_previsto', 'precio_funcion_previsto', 'precio_previsto',
                    'alumnos_real','maestros_real',
                    'cursos'
                    'precio_visitas_real', 'precio_funcion_real', 'precio_real',
                    'estado', 'creado')
        export_order = ( 'campanya', 'obra', 'funcion__fecha_hora', 'actividades_complementarias',
                    'responsable__nombre','responsable__email','responsable__telefono',
                    'centro__nombre','centro__email','centro__telefono',
                    'observaciones',
                    'alumnos_previsto','maestros_previsto',
                    'precio_visitas_previsto', 'precio_funcion_previsto', 'precio_previsto',
                    'alumnos_real','maestros_real',
                    'cursos',
                    'precio_visitas_real', 'precio_funcion_real', 'precio_real',
                    'estado', 'creado')
    
    # def dehydrate_cursos(self, obj):
    #     return unicode(obj.nombre_cursos())

    def dehydrate_campanya(self, reserva):
        return unicode(reserva.funcion.obra_campanya.campanya.nombre)
        
    def dehydrate_obra(self, reserva):
        return unicode(reserva.funcion.obra_campanya.obra.nombre)
        
    def dehydrate_actividades_complementarias(self, reserva):
        if reserva.visita_guiada:
            return unicode(_(u'Sí'))
        else:
            return unicode(_(u'No'))
                
    def dehydrate_estado(self, reserva):
        if reserva.estado=='p':
            return unicode(_(u'PENDIENTE'))
        elif reserva.estado=='c':
            return unicode(_(u'CONFIRMADA'))
        else:
            return unicode(_(u'CANCELADA'))
         
class Reserva_Form(ModelForm):
    
    class Meta:
        model = Reserva 
        widgets = {
                'observaciones': Textarea(attrs={'rows':5, 'class':'span12'}),
                'descripcion_visita_guiada': Textarea(attrs={'rows':2, 'class':'span12'}),
                'datos_fiscales': Textarea(attrs={'rows':3, 'class':'span12'}),
                'funcion': FuncionWidget(attrs={'class':'span8'}), 
                'centro': CentroWidget(attrs={'class':'span8'}), 
                'responsable': PersonaContactoWidget(attrs={'class':'span8'}), 
                }
        
        exclude = ['mail_pendiente_enviado', 'mail_confirmado_enviado', 'mail_cancelado_enviado', 'observaciones_es','observaciones_ca']
        
        
    def clean(self):
        responsable = self.cleaned_data.get('responsable')
        centro = self.cleaned_data.get('centro')
        if not responsable and not centro:
            raise ValidationError(_(u"Error. Debes asignar un responsable o un centro a la reserva."))
        funcion = self.cleaned_data.get('funcion') 
        if not funcion:
            raise ValidationError(_(u"Error. No has seleccionado una función para la reserva."))
        
        reservas = Reserva.objects.filter(funcion=funcion)
        ocupado_alumnos=0
        ocupado_maestros=0
        visita_guiada=0
        for r in reservas:
            if r.id != self.instance.id and r.estado!='x':
                ocupado_alumnos += r.alumnos_previsto
                ocupado_maestros += r.maestros_previsto
                if r.visita_guiada:
                    visita_guiada += r.alumnos_previsto
        
        alumnos_previsto = self.cleaned_data.get('alumnos_previsto')
        maestros_previsto = self.cleaned_data.get('maestros_previsto')
        hay_visita = self.cleaned_data.get('visita_guiada')
        # if the Reserva is in CANCELLED status, we dont count the alumnos and maestros:
        if not alumnos_previsto or self.cleaned_data.get('estado')=='x':
            alumnos_previsto=0
            #raise ValidationError(_(u"No has especificado un número de alumnos previstos para la reserva."))
        if not maestros_previsto or self.cleaned_data.get('estado')=='x':
            maestros_previsto=0
            #raise ValidationError(_(u"No has especificado un número de maestros previstos para la reserva."))
            
        libres = funcion.aforo_total + funcion.aforo_extra - ocupado_alumnos - ocupado_maestros
        if alumnos_previsto + maestros_previsto > libres:
            raise ValidationError(_(u"Solo quedan {0} butacas libres (incluyendo las {1} butacas extras). No hay suficiente aforo disponible para esta reserva.".format(str(libres), str(funcion.aforo_extra))))
        '''else:
            estado = self.cleaned_data.get('estado')
            print 'ANTES'
            print funcion.aforo_libre_previsto
            if estado !='x': # Si no es una funcion cancelada
                funcion.aforo_alumnos_previsto = ocupado_alumnos + alumnos_previsto
                funcion.aforo_maestros_previsto = ocupado_maestros + maestros_previsto
                funcion.aforo_libre_previsto = funcion.aforo_total - (ocupado_alumnos + alumnos_previsto + ocupado_maestros + maestros_previsto) 
                if hay_visita:
                    funcion.entradas_visitas_guiadas_previsto = visita_guiada + alumnos_previsto
                    
            else: #se calcela la reserva
                for r in reservas:
                    if r.id == self.instance.id: # liberamos las butacas de esta reserva si ya había sido guardada previamente.
                        funcion.aforo_alumnos_previsto -= r.alumnos_previsto
                        funcion.aforo_maestros_previsto -= r.maestros_previsto
                        funcion.aforo_libre_previsto += r.alumnos_previsto + r.maestros_previsto
                        if r.visita_guiada:
                            funcion.entradas_visitas_guiadas_previsto -= r.alumnos_previsto
            print 'DESPUES'
            print funcion.aforo_libre_previsto
            print 'salvamos la funcion'
            print '-------------------'
            funcion.save()
        '''
        return self.cleaned_data


        
class FuncionFilter_4_Reserva(admin.SimpleListFilter):
    title = _('Función')
    parameter_name = 'funcion'

    def lookups(self, request, model_admin):
        funciones = Funcion.objects.filter(obra_campanya__obra__owner__in=request.user.teatro_set.all())
        return [(f.id, f.__unicode__) for f in funciones]
        # You can also use hardcoded model name like "Funcion" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(funcion__id__exact=self.value())
        else:
            return queryset

class CentroFilter_4_Reserva(admin.SimpleListFilter):
    title = _('Centro')
    parameter_name = 'centro'

    def lookups(self, request, model_admin):
        centros = Centro.objects.filter(owner=request.user.teatro_set.first())
        return [(c.id, c.__unicode__) for c in centros]
        # You can also use hardcoded model name like "Centro" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(centro__id__exact=self.value())
        else:
            return queryset


class BulkDeleteReservaMixin(object):
    class SafeDeleteQuerysetWrapper(object):
        def __init__(self, wrapped_queryset):
            self.wrapped_queryset = wrapped_queryset

        def _safe_delete(self):
            #for obj in self.wrapped_queryset:
            #    obj.delete()
            for o in self.wrapped_queryset:
                current_reserva = Reserva.objects.get(id=o.id) 
                # obtenemos la funcion de la reserva original por si 
                # el usuario ha modificado la funcion en el formulario
                funcion = current_reserva.funcion
                #print 'RESERVA A ELIMINAR:'
                #print 'ANTES'
                #print funcion.aforo_libre_previsto
                if current_reserva.estado !='x': # liberamos las butacas de esta reserva si ya había sido guardada previamente.
                    funcion.aforo_alumnos_previsto -=  current_reserva.alumnos_previsto
                    funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                    funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                    funcion.aforo_alumnos_real -=  current_reserva.alumnos_real
                    funcion.aforo_maestros_real -= current_reserva.maestros_real
                    funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                    
                    if current_reserva.visita_guiada:
                        if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                            funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                            funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                        else:
                            funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                            funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real
                    funcion.save()
                    obraencampanya = funcion.obra_campanya
                    obraencampanya.save()
                    campanya = obraencampanya.campanya
                    campanya.save()
                #print 'DESPUES'
                #print funcion.aforo_libre_previsto
                #print 'salvamos la funcion'
                #print '-------------------'
                o.delete()
                
        def __getattr__(self, attr):
            if attr == 'delete':
                return self._safe_delete
            else:
                return getattr(self.wrapped_queryset, attr)

        def __iter__(self):
            for obj in self.wrapped_queryset:
                yield obj

        def __getitem__(self, index):
            return self.wrapped_queryset[index]

        def __len__(self):
            return len(self.wrapped_queryset)

    def get_actions(self, request):
        actions = super(BulkDeleteReservaMixin, self).get_actions(request)
        actions['delete_selected'] = (BulkDeleteReservaMixin.action_safe_bulk_delete, 'delete_selected', _("Delete selected %(verbose_name_plural)s"))
        return actions

    def action_safe_bulk_delete(self, request, queryset):
        wrapped_queryset = BulkDeleteReservaMixin.SafeDeleteQuerysetWrapper(queryset)
        return delete_selected(self, request, wrapped_queryset)

class Reserva_con_delete_guay(BulkDeleteReservaMixin, admin.ModelAdmin):
    pass
    
class Reserva_Admin(ExportMixin, Reserva_con_delete_guay):
    resource_class = ReservaResource
    form = Reserva_Form
    readonly_fields=['precio_previsto','precio_real']
    list_display=( 'nombre_obra', 'fecha_hora', 'estado', 'centro', 'visita_guiada', 'observaciones', 'total_previsto','total_real','telefono', 'email','creado',)
    search_fields=('funcion__obra_campanya__obra__nombre','centro__nombre',)
    list_filter=('estado', 'funcion__nombre_obra','centro__nombre',) #FuncionFilter_4_Reserva,)#CentroFilter_4_Reserva,)
    #actions = ['action_delete_model', ]
    filter_horizontal= ('cursos',)
    
    fieldsets = [
                (_(u'Función'), {
                        'fields': ['funcion',] 
                }),
                (_(u'Actividades Complementarias'), {
                        'fields': ['visita_guiada', 'descripcion_visita_guiada',] 
                }),
                (_(u'Quién reserva'), {
                        'fields': ['responsable', 'centro',]
                }),
                (_(u'Observaciones'), {
                        'fields': ['observaciones',]
                }),
                (_(u'Asistencia prevista'), {
                        'fields': ['alumnos_previsto','maestros_previsto','precio_previsto','cursos',]
                }),
                (_(u'Pago'), {
                        'fields': ['precio_funcion_personalizado','precio_visitas_personalizado','tipo_pago','pago_en','datos_fiscales',]
                }),
                
                (_(u'Asistencia real'), {
                        'fields': ['alumnos_real','maestros_real','precio_real',]
                }),
                (_(u'Estado de la reserva'), {
                        'description': _(u'Sugerencia: Usa PENDIENTE si el grupo paga el mismo día de la función. Usa CONFIRMADA si paga antes.'),
                        'fields': []
                }),
                (None, {
                        'fields': ['estado', 'reenviar_mail',]
                }),
                
        ]
    
    
    def get_queryset(self, request):
        qs = super(Reserva_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(centro__owner=request.user.teatro_set.first())
    
    def get_formset(self, request, obj=None, **kwargs):
        # Hack! Hook the teatro from the user request
        self.teatro = request.user.teatro_set.first()
        return super(Reserva_Admin, self).get_formset(
            request, obj, **kwargs)
    
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):

        if db_field.name == 'centro':
            kwargs['queryset'] = Centro.objects.filter(owner=request.user.teatro_set.first())
        if db_field.name == 'responsable':
            kwargs['queryset'] = Persona_contacto.objects.filter(owner=request.user.teatro_set.first())
        #if db_field.name == 'funcion':
            # no se debe filtrar ni por fecha de representacion (mostrando solo las funciones que vendran) ni por aforo disponible porque editar las
            # reservas que forman parte de las funciones filtradas no será posible.
            #now = timezone.localtime(timezone.now())
            #kwargs['queryset'] = Funcion.objects.filter(obra_campanya__obra__owner__in=request.user.teatro_set.all(), aforo_libre_previsto__gt=-F('aforo_extra'), fecha_hora__gte=now).order_by('fecha_hora', 'obra_campanya__obra__nombre')

        #    kwargs['queryset'] = Funcion.objects.filter(obra_campanya__obra__owner__in=request.user.teatro_set.all()).order_by('fecha_hora', 'obra_campanya__obra__nombre')
            
           
        return super(Reserva_Admin, self).formfield_for_foreignkey(db_field, request=None, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'cursos':
            kwargs['queryset'] = Curso.objects.all()
        
        return super(Reserva_Admin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def delete_model(self, request, obj):
        current_reserva = Reserva.objects.get(id=obj.id) 
        # obtenemos la funcion de la reserva original por si 
        # el usuario ha modificado la funcion en el formulario
        funcion = current_reserva.funcion 
        #print 'RESERVA A ELIMINAR:'
        #print 'ANTES'
        #print funcion.aforo_libre_previsto
        #print obj.estado
        if current_reserva.estado !='x': # liberamos las butacas de esta reserva si ya había sido guardada previamente.
            funcion.aforo_alumnos_previsto -=  current_reserva.alumnos_previsto
            funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
            funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
            funcion.aforo_alumnos_real -=  current_reserva.alumnos_real
            funcion.aforo_maestros_real -= current_reserva.maestros_real
            funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                 
            if current_reserva.visita_guiada:
                if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                    funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                    funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                else:
                    funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                    funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real
                    
            funcion.save()
            obraencampanya = funcion.obra_campanya
            obraencampanya.save()
            campanya = obraencampanya.campanya
            campanya.save()
        #print 'DESPUES'
        #print funcion.aforo_libre_previsto
        #print 'salvamos la funcion'
        #print '-------------------'
        obj.delete()
        

    
    
    def save_model(self, request, obj, form, change):
        if obj.id:
            #print 'el objeto tiene id'
            current_reserva = Reserva.objects.get(id=obj.id)
        else:
            #print 'el objeto NO tiene id'
            current_reserva = obj
            
        # Si se ha modificado la funcion, debemos actualizar el aforo de la funcion anterior
        if current_reserva.funcion != obj.funcion:
            
            # modificamos el aforo de la funcion anterior:
            old_funcion = current_reserva.funcion
            #print 'modificamos la funcion anterior:'
            #print old_funcion
            #print 'ANTES'
            #print old_funcion.aforo_libre_previsto
            
            if current_reserva.estado!='x':
                old_funcion.aforo_alumnos_previsto -= current_reserva.alumnos_previsto
                old_funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                old_funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                old_funcion.aforo_alumnos_real -= current_reserva.alumnos_real
                old_funcion.aforo_maestros_real -= current_reserva.maestros_real
                old_funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                if current_reserva.visita_guiada:
                    if obj.funcion.obra_campanya.campanya.los_maestros_no_pagan:
                        old_funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                        old_funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                    else:
                        old_funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                        old_funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real                    
                old_funcion.save()
            #print 'DESPUES'
            #print old_funcion.aforo_libre_previsto
            #print 'salvamos la funcion'
            #print '-------------------'
        
        # ahora actualizamos el aforo de la función actual.
        #print 'actualizamos la funcion actual:'
        #print obj.funcion
        funcion = obj.funcion
        reservas = Reserva.objects.filter(funcion=funcion)
        ocupado_alumnos_previsto=0
        ocupado_alumnos_real=0
        ocupado_maestros_previsto=0
        ocupado_maestros_real=0
        visita_guiada_previsto=0
        visita_guiada_real=0
        for r in reservas:
            if r.id != obj.id and r.estado !='x':
                ocupado_alumnos_previsto += r.alumnos_previsto
                ocupado_alumnos_real += r.alumnos_real
                ocupado_maestros_previsto += r.maestros_previsto
                ocupado_maestros_real += r.maestros_real
                if r.visita_guiada:
                    if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                        # Solo contamos a los alumnos:  
                        visita_guiada_previsto += r.alumnos_previsto
                        visita_guiada_real += r.alumnos_real
                    else:
                        visita_guiada_previsto += r.alumnos_previsto + r.maestros_previsto
                        visita_guiada_real += r.alumnos_real + r.maestros_real
                        
        funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto
        funcion.entradas_visitas_guiadas_real = visita_guiada_real
        #print 'RESERVA A SALVAR:'
        #print 'ANTES'
        #print funcion.aforo_libre_previsto
        if obj.estado !='x': # Si no es una funcion cancelada
            funcion.aforo_alumnos_previsto = ocupado_alumnos_previsto + obj.alumnos_previsto 
            funcion.aforo_maestros_previsto = ocupado_maestros_previsto + obj.maestros_previsto
            funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos_previsto - obj.alumnos_previsto - ocupado_maestros_previsto - obj.maestros_previsto 
            
            funcion.aforo_alumnos_real = ocupado_alumnos_real + obj.alumnos_real 
            funcion.aforo_maestros_real = ocupado_maestros_real + obj.maestros_real
            funcion.aforo_libre_real = funcion.aforo_total - ocupado_alumnos_real - obj.alumnos_real - ocupado_maestros_real - obj.maestros_real
            
            if obj.visita_guiada:
                if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                    # Solo contamos a los alumnos:   
                    funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto + obj.alumnos_previsto
                    funcion.entradas_visitas_guiadas_real = visita_guiada_real + obj.alumnos_real    
                else:
                    funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto + obj.alumnos_previsto + obj.maestros_previsto
                    funcion.entradas_visitas_guiadas_real = visita_guiada_real + obj.alumnos_real + obj.maestros_real
               
                        
        else: #se calcela la reserva
            funcion.aforo_alumnos_previsto = ocupado_alumnos_previsto
            funcion.aforo_maestros_previsto = ocupado_maestros_previsto
            funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos_previsto - ocupado_maestros_previsto 
            
            funcion.aforo_alumnos_real = ocupado_alumnos_real
            funcion.aforo_maestros_real = ocupado_maestros_real
            funcion.aforo_libre_real = funcion.aforo_total - ocupado_alumnos_real - ocupado_maestros_real
                
        
        #print 'DESPUES'
        #print funcion.aforo_libre_previsto
        #print 'salvamos la funcion'
        #print '-------------------'
        funcion.save()
        
        obraencampanya = funcion.obra_campanya
        obraencampanya.save()
        campanya = obraencampanya.campanya
        campanya.save()
        
        
        obj.save()
        
        
    
    def get_form(self, request, obj=None, **kwargs):
        form = super(Reserva_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the queryset
        form.base_fields['responsable'].queryset = form.base_fields['responsable'].queryset.filter(owner__in=request.user.teatro_set.all())
        form.base_fields['centro'].queryset = form.base_fields['centro'].queryset.filter(owner__in=request.user.teatro_set.all())
        form.base_fields['funcion'].queryset = form.base_fields['funcion'].queryset.filter(obra_campanya__campanya__owner=request.user.teatro_set.first())
        return form
    
    
#admin.site.register(Reserva, Reserva_Admin)
ravaladmin.register(Reserva, Reserva_Admin)
teyatadmin.register(Reserva, Reserva_Admin)
    
class Reserva_InLine(ObjectPermissionsStackedInline):
    model  = Reserva
    form = Reserva_Form
    readonly_fields =['precio_previsto','precio_real']
    filter_horizontal= ('cursos',)

    extra= 0
    suit_classes= 'suit-tab suit-tab-funcion-reservas'
    
    fieldsets = [
                (None, {
                        'fields': [] 
                }),
                (None, {
                        'fields': ['visita_guiada', 'descripcion_visita_guiada',] 
                }),
                (None, {
                        'fields': ['responsable', 'centro',]
                }),
                (None, {
                        'fields': ['observaciones',]
                }),
                (None, {
                        'fields': ['alumnos_previsto','maestros_previsto','precio_previsto', 'cursos']
                }),
                (None, {
                        'fields': ['alumnos_real','maestros_real','precio_real',]
                }),
                (_(u'Pago'), {
                        'fields': ['precio_funcion_personalizado','precio_visitas_personalizado','tipo_pago','pago_en','datos_fiscales',]
                }),
                (None, {
                        'fields': ['estado', 'reenviar_mail',]
                }),
                
        ]
    
    
    def get_formset(self, request, obj=None, **kwargs):
        # Hack! Hook the teatro from the user request
        self.teatro = request.user.teatro_set.first()
        return super(Reserva_InLine, self).get_formset(
            request, obj, **kwargs)
    
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        
        if db_field.name == 'centro':
            kwargs['queryset'] = Centro.objects.filter(owner=request.user.teatro_set.first())
        if db_field.name == 'responsable':
            kwargs['queryset'] = Persona_contacto.objects.filter(owner=request.user.teatro_set.first())
        if db_field.name == 'funcion':
            # no se debe filtrar ni por fecha de representacion (mostrando solo las funciones que vendran) ni por aforo disponible porque editar las
            # reservas que forman parte de las funciones filtradas no será posible.
            #now = timezone.localtime(timezone.now())
            #kwargs['queryset'] = Funcion.objects.filter(obra_campanya__obra__owner__in=request.user.teatro_set.all(), aforo_libre_previsto__gt=-F('aforo_extra'), fecha_hora__gte=now).order_by('fecha_hora', 'obra_campanya__obra__nombre')

            kwargs['queryset'] = Funcion.objects.filter(obra_campanya__obra__owner__in=request.user.teatro_set.all()).order_by('fecha_hora', 'obra_campanya__obra__nombre')
            
        
        return super(Reserva_InLine, self).formfield_for_foreignkey(db_field, request=None, **kwargs)
    
    
    
class ObraFilter_4_Funcion(admin.SimpleListFilter):
    title = _('Obra')
    parameter_name = 'obra'

    def lookups(self, request, model_admin):
        obras = Obra.objects.filter(owner__in=request.user.teatro_set.all())
        return [(o.id, o.nombre) for o in obras]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(obra_campanya__obra__id__exact=self.value())
        else:
            return queryset

class IdiomaFilter_4_Funcion(admin.SimpleListFilter):
    title = _('Idioma')
    parameter_name = 'idioma'

    def lookups(self, request, model_admin):
        idiomas = Idioma.objects.filter(owner__in=request.user.teatro_set.all())
        return [(o.id, o.nombre) for o in idiomas]
        # You can also use hardcoded model name like "Idioma" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(idioma__id__exact=self.value())
        else:
            return queryset

           
class CampanyaFilter_4_Funcion(admin.SimpleListFilter):
    title = _('Campaña')
    parameter_name = 'campanya'

    def lookups(self, request, model_admin):
        campanyas = Campanya.objects.filter(owner__in=request.user.teatro_set.all())
        return [(o.id, o.nombre) for o in campanyas]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(obra_campanya__campanya__id__exact=self.value())
        else:
            return queryset
            
class Funcion_Form(ModelForm):
    
    class Meta:
        model = Funcion 
        widgets = {
                'obra_campanya': ObraCampanyaWidget(attrs={'class': 'span12'}),
                'fecha_hora': SuitSplitDateTimeWidget,
                }
        fields = '__all__'           

class FuncionResource(resources.ModelResource):
    campanya = fields.Field()
    obra = fields.Field()
    
    class Meta: 
        model = Funcion
        fields = ( 'campanya', 'obra', 'fecha_hora', 'idioma__nombre',
                    'precio','aforo_total','aforo_extra', 'creado')
        export_order = ( 'campanya', 'obra', 'fecha_hora', 'idioma__nombre',
                    'precio','aforo_total','aforo_extra', 'creado')
    
    def dehydrate_campanya(self, funcion):
        return unicode(funcion.obra_campanya.campanya.nombre)
        
    def dehydrate_obra(self, funcion):
        return unicode(funcion.obra_campanya.obra.nombre)
        
        
        
class Funcion_Admin_raval(ExportMixin, admin.ModelAdmin):
    save_as = True
    resource_class = FuncionResource
    model = Funcion
    form = Funcion_Form
    inlines = [Reserva_InLine,]
    list_display=('fecha_hora', 'idioma', 'obra', 'precio', 'aforo_total', 'aforo_libre_previsto', 'entradas_visitas_guiadas_previsto',
        'aforo_ocupado_previsto', 'recaudacion_visitas_prevista', 'recaudacion_funcion_prevista', 'recaudacion_prevista', 
       'aforo_ocupado_real', 'entradas_visitas_guiadas_real', 'recaudacion_visitas_real', 'recaudacion_funcion_real', 'recaudacion_real',)
    #ordering=('obra_campanya__obra__nombre',)   
    readonly_fields =['aforo_alumnos_previsto', 'aforo_maestros_previsto','aforo_alumnos_real', 'aforo_maestros_real','entradas_visitas_guiadas_previsto', 'entradas_visitas_guiadas_real','creado', 'modificado',]
    list_filter=(CampanyaFilter_4_Funcion, ObraFilter_4_Funcion,IdiomaFilter_4_Funcion,)
    search_fields=('obra_campanya__obra__nombre',)
    date_hierarchy = 'fecha_hora'
    list_select_related = True
    
    fieldsets = [
                (_(u'Consejo:'), {
                        'description': _(u'Si tienes que crear varias funciones para una misma obra, cambia la fecha y hora y presiona "Graba como nuevo".'),
                        'fields': [],
                }),
                (_(u'Obra'), {
                        'classes': ('suit-tab suit-tab-funcion',),
                        'fields': ['obra_campanya',],
                }),
                (_(u'Fecha y hora'), {
                        'classes': ('suit-tab suit-tab-funcion',),
                        'fields': ['fecha_hora', ],
                }),
                (_(u'Idioma de la representación'), {
                        'classes': ('suit-tab suit-tab-funcion',),
                        'fields': ['idioma', ],
                }),
                (_(u'Precio'), {
                        'classes': ('suit-tab suit-tab-funcion',),
                        'fields': ['precio', ],
                }),
                (_(u'Aforo'), {
                        'classes': ('suit-tab suit-tab-funcion',),
                        'fields': ['aforo_total', 'aforo_extra',],
                }),
                ((u'Asistencia'), {
                        'classes': ('suit-tab suit-tab-asistencia',),
                        'description': _(u'Se calcula automáticamente a partir de las reservas.'),
                        'fields': []
                }),
                (_(u'Prevista'), {
                        'classes': ('suit-tab suit-tab-asistencia',),
                        'fields': ['aforo_alumnos_previsto', 'aforo_maestros_previsto','entradas_visitas_guiadas_previsto',],
                }), 
                (_(u'Real'), {
                        'classes': ('suit-tab suit-tab-asistencia',),
                        'fields': ['aforo_alumnos_real', 'aforo_maestros_real','entradas_visitas_guiadas_real',],
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-funcion-reservas',),
                        'fields': [],
                }),
        ]

    suit_form_tabs = (('funcion', _(u'Función')),
                       ('asistencia', _(u'Asistencia')),
                          ('funcion-reservas', _(u'Reservas asociadas')),
                          )
                          
    
    def suit_row_attributes(self, obj, request):
        css_class = {
            'blue': 'info',
            'green': 'success',
            'yellow': 'warning',
            'red': 'error',
        }.get(obj.color_status())
        if css_class:
            return {'class': css_class, 'data': obj.fecha_hora}
    
    def get_queryset(self, request):
        qs = super(Funcion_Admin_raval, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(obra_campanya__campanya__owner=request.user.teatro_set.first())
    
    def get_form(self, request, obj=None, **kwargs):
        form = super(Funcion_Admin_raval,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        form.base_fields['obra_campanya'].queryset = form.base_fields['obra_campanya'].queryset.filter(campanya__owner__in=request.user.teatro_set.all())
        form.base_fields['idioma'].queryset = form.base_fields['idioma'].queryset.filter(owner__id__in=request.user.teatro_set.all())
        return form
    
    
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        #print 'Save_formset de Funcion'
        for obj in formset.deleted_objects:
            current_reserva = Reserva.objects.get(id=obj.id) 
            # obtenemos la funcion de la reserva original por si 
            # el usuario ha modificado la funcion en el formulario
            funcion = current_reserva.funcion 
            #print 'RESERVA A ELIMINAR:'
            #print 'ANTES'
            #print funcion.aforo_libre_previsto
            #print obj.estado
            if current_reserva.estado !='x': # liberamos las butacas de esta reserva si ya había sido guardada previamente.
                funcion.aforo_alumnos_previsto -=  current_reserva.alumnos_previsto
                funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                
                funcion.aforo_alumnos_real -=  current_reserva.alumnos_real
                funcion.aforo_maestros_real -= current_reserva.maestros_real
                funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                if current_reserva.visita_guiada:
                    if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                        # Descontar solo a los alumnos
                        funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                        funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                    else:
                        funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                        funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real                   
                funcion.save()
                obra_campanya = funcion.obra_campanya
                obra_campanya.save()
                campanya = obra_campanya.campanya
                campanya.save()
                
            #print 'DESPUES'
            #print funcion.aforo_libre_previsto
            #print 'salvamos la funcion'
            #print '-------------------'
                    
            obj.delete()
            
            
        #print "Instancias a modificar: %d" % len(instances)
        
        for instance in instances:
            #print instance
            if instance.id:
                #print 'la instancia tiene id'
                current_reserva = Reserva.objects.get(id=instance.id)
            else:
                #print 'la instancia NO tiene id'
                current_reserva = instance
            # Si se ha modificado la funcion, debemos actualizar el aforo de la funcion anterior
            if current_reserva.funcion != instance.funcion:
                # modificamos el aforo de la funcion anterior:
                old_funcion = current_reserva.funcion
                #print 'modificamos la funcion anterior:'
                #print old_funcion
                #print 'ANTES'
                #print old_funcion.aforo_libre_previsto
            
                if current_reserva.estado!='x':
                    old_funcion.aforo_alumnos_previsto -= current_reserva.alumnos_previsto
                    old_funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                    old_funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                    
                    old_funcion.aforo_alumnos_real -= current_reserva.alumnos_real
                    old_funcion.aforo_maestros_real -= current_reserva.maestros_real
                    old_funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                    
                    
                    if current_reserva.visita_guiada:
                        if old_funcion.obra_campanya.campanya.los_maestros_no_pagan:
                            old_funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                            old_funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                        else:
                            old_funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                            old_funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real
                    old_funcion.save()
                #print 'DESPUES'
                #print old_funcion.aforo_libre_previsto
                #print 'salvamos la funcion'
                #print '-------------------'
            
            # ahora actualizamos el aforo de la función actual.
            #print 'actualizamos la funcion actual:'
            #print instance.funcion
            funcion = instance.funcion
            reservas = Reserva.objects.filter(funcion=funcion)
            ocupado_alumnos_previsto=0
            ocupado_alumnos_real=0
            ocupado_maestros_previsto=0
            ocupado_maestros_real=0
            visita_guiada_previsto=0
            visita_guiada_real=0
            for r in reservas:
                if r.id != instance.id and r.estado !='x':
                    ocupado_alumnos_previsto += r.alumnos_previsto
                    ocupado_maestros_previsto += r.maestros_previsto
                    ocupado_alumnos_real += r.alumnos_real
                    ocupado_maestros_real += r.maestros_real
                    
                    if r.visita_guiada:
                        if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                            # solo sumamos a los alumnos
                            visita_guiada_previsto += r.alumnos_previsto
                            visita_guiada_real += r.alumnos_real
                        else:
                            visita_guiada_previsto += r.alumnos_previsto + r.maestros_previsto
                            visita_guiada_real += r.alumnos_real + r.maestros_real
            #print 'Sin contar la actual instancia: ocupado maestros real: %d' % ocupado_maestros_real
            #print 'Sin contar la actual instancia: ocupado alumnos real: %d' % ocupado_alumnos_real
            funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto
            funcion.entradas_visitas_guiadas_real = visita_guiada_real
            
            #print 'RESERVA A SALVAR:'
            #print 'ANTES: %d' % funcion.aforo_maestros_real
            if instance.estado !='x': # Si no es una funcion cancelada
                funcion.aforo_alumnos_previsto = ocupado_alumnos_previsto + instance.alumnos_previsto 
                funcion.aforo_maestros_previsto = ocupado_maestros_previsto + instance.maestros_previsto
                funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos_previsto - instance.alumnos_previsto - ocupado_maestros_previsto - instance.maestros_previsto 
                
                funcion.aforo_alumnos_real = ocupado_alumnos_real + instance.alumnos_real 
                funcion.aforo_maestros_real = ocupado_maestros_real + instance.maestros_real
                funcion.aforo_libre_real = funcion.aforo_total - ocupado_alumnos_real - instance.alumnos_real - ocupado_maestros_real - instance.maestros_real
                #print '(ocupado_maestros_real + instance.maestros_real):  %d + %d' % (ocupado_maestros_real, instance.maestros_real)

                if instance.visita_guiada:
                    if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                        # solo sumamos a los alumnos
                        funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto + instance.alumnos_previsto
                        funcion.entradas_visitas_guiadas_real = visita_guiada_real + instance.alumnos_real
                    else:
                        funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto + instance.alumnos_previsto + instance.maestros_previsto
                        funcion.entradas_visitas_guiadas_real = visita_guiada_real + instance.alumnos_real + instance.maestros_real
                        
            else: #se cancela la reserva
                funcion.aforo_alumnos_previsto = ocupado_alumnos_previsto
                funcion.aforo_maestros_previsto = ocupado_maestros_previsto
                funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos_previsto - ocupado_maestros_previsto
                
                funcion.aforo_alumnos_real = ocupado_alumnos_real
                funcion.aforo_maestros_real = ocupado_maestros_real
                funcion.aforo_libre_real = funcion.aforo_total - ocupado_alumnos_real - ocupado_maestros_real
                #print '(ocupado_maestros_real):  %d' % ocupado_maestros_real
                
                
                    
                
            #print 'DESPUES: %d' % funcion.aforo_maestros_real
            #print 'salvamos la funcion'
            #print '-------------------'
            funcion.save()
            obra_campanya = funcion.obra_campanya
            obra_campanya.save()
            campanya = obra_campanya.campanya
            campanya.save()
            instance.save()
        
        formset.save_m2m()

class funcion_ExportDisclaimer(ExportMixin, admin.ModelAdmin):
    change_list_template = 'admin/teyat/funcion/change_list.html'
    pass
    
class Funcion_Admin(funcion_ExportDisclaimer):
    save_as = True
    resource_class = FuncionResource    
    model = Funcion
    form = Funcion_Form
    list_display=('fecha_hora', 'obra', 'idioma', 'precio', 'aforo_total', )
    list_filter=(CampanyaFilter_4_Funcion, ObraFilter_4_Funcion,IdiomaFilter_4_Funcion,)
    search_fields=('obra_campanya__obra__nombre',)
    date_hierarchy = 'fecha_hora'
    list_select_related = True
    
    fieldsets = [
                #(_(u'Consejo:'), {
                #        'description': _(u'Si tienes que crear varias funciones para una misma obra, cambia la fecha y hora y presiona "Graba como nuevo".'),
                #        'fields': [],
                #}),
                (_(u'Obra'), {
                        'fields': ['obra_campanya',],
                }),
                (_(u'Fecha y hora'), {
                        'fields': ['fecha_hora', ],
                }),
                (_(u'Idioma de la representación'), {
                        'fields': ['idioma', ],
                }),
                (_(u'Precio'), {
                        'fields': ['precio', ],
                }),
                (_(u'Aforo'), {
                        'fields': ['aforo_total', 'aforo_extra',],
                }),
        ]

                          
    
    '''def suit_row_attributes(self, obj, request):
        css_class = {
            'blue': 'info',
            'green': 'success',
            'yellow': 'warning',
            'red': 'error',
        }.get(obj.color_status())
        if css_class:
            return {'class': css_class, 'data': obj.fecha_hora}
    '''
    def get_queryset(self, request):
        qs = super(Funcion_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(obra_campanya__campanya__owner=request.user.teatro_set.first())
    
    def get_form(self, request, obj=None, **kwargs):
        form = super(Funcion_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        form.base_fields['obra_campanya'].queryset = form.base_fields['obra_campanya'].queryset.filter(campanya__owner__in=request.user.teatro_set.all())
        form.base_fields['idioma'].queryset = form.base_fields['idioma'].queryset.filter(owner__id__in=request.user.teatro_set.all())
        return form
    
    
#admin.site.register(Funcion, Funcion_Admin)
teyatadmin.register(Funcion, Funcion_Admin)
ravaladmin.register(Funcion, Funcion_Admin_raval)



class Aforo_FuncionResource(resources.ModelResource):
    campanya = fields.Field()
    obra = fields.Field()
    
    class Meta: 
        model = Aforo_Funcion
        fields = ( 'campanya', 'obra', 'fecha_hora', 'idioma__nombre',
                    'aforo_total','aforo_extra', 
                    'aforo_libre_previsto','aforo_alumnos_previsto','aforo_maestros_previsto',
                    'entradas_visitas_guiadas_previsto',
                    'aforo_libre_real','aforo_alumnos_real','aforo_maestros_real',
                    'entradas_visitas_guiadas_real',
                    )
        export_order = ( 'campanya', 'obra', 'fecha_hora', 'idioma__nombre',
                    'aforo_total','aforo_extra', 
                    'aforo_libre_previsto','aforo_alumnos_previsto','aforo_maestros_previsto',
                    'entradas_visitas_guiadas_previsto',
                    'aforo_libre_real','aforo_alumnos_real','aforo_maestros_real',
                    'entradas_visitas_guiadas_real',
                    )
    
    def dehydrate_campanya(self, funcion):
        return unicode(funcion.obra_campanya.campanya.nombre)
        
    def dehydrate_obra(self, funcion):
        return unicode(funcion.obra_campanya.obra.nombre)

class Aforo_funcion_ExportDisclaimer(ExportMixin, StatsAdmin):
    change_list_template = 'admin/teyat/aforo_funcion/change_list.html'
    pass
    
class Aforo_Funcion_Admin(Aforo_funcion_ExportDisclaimer, admin.ModelAdmin):
    model = Aforo_Funcion
    resource_class = Aforo_FuncionResource 
    inlines = [Reserva_InLine,]
    
    list_display=('fecha_hora', 'obra', 'aforo_total', 'aforo_libre_previsto', 
        'aforo_ocupado_previsto', 'entradas_visitas_guiadas_previsto', 'aforo_ocupado_real', 'entradas_visitas_guiadas_real',)
    stats = (Sum('aforo_total'),Sum('aforo_libre_previsto'),Sum('aforo_alumnos_previsto'), Sum('aforo_maestros_previsto'),Sum('entradas_visitas_guiadas_previsto'),Sum('aforo_libre_real'),Sum('aforo_alumnos_real'), Sum('aforo_maestros_real'),Sum('entradas_visitas_guiadas_real'),)
    #ordering=('obra_campanya__obra__nombre',)   
    readonly_fields =['obra_campanya', 'fecha_hora', 'idioma','aforo_alumnos_previsto', 'aforo_maestros_previsto','aforo_alumnos_real', 'aforo_maestros_real','entradas_visitas_guiadas_previsto', 'entradas_visitas_guiadas_real','creado', 'modificado',]
    list_filter=(CampanyaFilter_4_Funcion, ObraFilter_4_Funcion,IdiomaFilter_4_Funcion,)
    search_fields=('obra_campanya__obra__nombre',)
    date_hierarchy = 'fecha_hora'
    list_select_related = True
    #class Media:
    #    js = ("js/autorefresh.js",)
        
    fieldsets = [
                ((u'Asistencia'), {
                        'classes': ('suit-tab suit-tab-asistencia',),
                        'description': _(u'Se calcula automáticamente a partir de las reservas.'),
                        'fields': []
                }),
                (_(u'Prevista'), {
                        'classes': ('suit-tab suit-tab-asistencia',),
                        'fields': ['aforo_alumnos_previsto', 'aforo_maestros_previsto','entradas_visitas_guiadas_previsto',],
                }), 
                (_(u'Real'), {
                        'classes': ('suit-tab suit-tab-asistencia',),
                        'fields': ['aforo_alumnos_real', 'aforo_maestros_real','entradas_visitas_guiadas_real',],
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-funcion-reservas',),
                        'fields': [],
                }),
        ]

    suit_form_tabs = (
                       ('asistencia', _(u'Asistencia')),
                          ('funcion-reservas', _(u'Reservas asociadas')),
                          )
                          
    
    def suit_row_attributes(self, obj, request):
        css_class = {
            'blue': 'info',
            'green': 'success',
            'yellow': 'warning',
            'red': 'error',
        }.get(obj.color_status())
        if css_class:
            return {'class': css_class, 'data': obj.fecha_hora}
    
    def get_actions(self, request):
        actions = super(Aforo_Funcion_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions
    
    
    def get_queryset(self, request):
        qs = super(Aforo_Funcion_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(obra_campanya__campanya__owner=request.user.teatro_set.first())
    
    
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        #print 'Save_formset de Funcion'
        for obj in formset.deleted_objects:
            current_reserva = Reserva.objects.get(id=obj.id) 
            # obtenemos la funcion de la reserva original por si 
            # el usuario ha modificado la funcion en el formulario
            funcion = current_reserva.funcion 
            #print 'RESERVA A ELIMINAR:'
            #print 'ANTES'
            #print funcion.aforo_libre_previsto
            #print obj.estado
            if current_reserva.estado !='x': # liberamos las butacas de esta reserva si ya había sido guardada previamente.
                funcion.aforo_alumnos_previsto -=  current_reserva.alumnos_previsto
                funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                
                funcion.aforo_alumnos_real -=  current_reserva.alumnos_real
                funcion.aforo_maestros_real -= current_reserva.maestros_real
                funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                if current_reserva.visita_guiada:
                    if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                        # Descontar solo a los alumnos
                        funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                        funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                    else:
                        funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                        funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real                   
                funcion.save()
                obra_campanya = funcion.obra_campanya
                obra_campanya.save()
                campanya = obra_campanya.campanya
                campanya.save()
                
            #print 'DESPUES'
            #print funcion.aforo_libre_previsto
            #print 'salvamos la funcion'
            #print '-------------------'
                    
            obj.delete()
            
            
        #print "Instancias a modificar: %d" % len(instances)
        
        for instance in instances:
            #print instance
            if instance.id:
                #print 'la instancia tiene id'
                current_reserva = Reserva.objects.get(id=instance.id)
            else:
                #print 'la instancia NO tiene id'
                current_reserva = instance
            # Si se ha modificado la funcion, debemos actualizar el aforo de la funcion anterior
            if current_reserva.funcion != instance.funcion:
                # modificamos el aforo de la funcion anterior:
                old_funcion = current_reserva.funcion
                #print 'modificamos la funcion anterior:'
                #print old_funcion
                #print 'ANTES'
                #print old_funcion.aforo_libre_previsto
            
                if current_reserva.estado!='x':
                    old_funcion.aforo_alumnos_previsto -= current_reserva.alumnos_previsto
                    old_funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                    old_funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                    
                    old_funcion.aforo_alumnos_real -= current_reserva.alumnos_real
                    old_funcion.aforo_maestros_real -= current_reserva.maestros_real
                    old_funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                    
                    
                    if current_reserva.visita_guiada:
                        if old_funcion.obra_campanya.campanya.los_maestros_no_pagan:
                            old_funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                            old_funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                        else:
                            old_funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                            old_funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real
                    old_funcion.save()
                #print 'DESPUES'
                #print old_funcion.aforo_libre_previsto
                #print 'salvamos la funcion'
                #print '-------------------'
            
            # ahora actualizamos el aforo de la función actual.
            #print 'actualizamos la funcion actual:'
            #print instance.funcion
            funcion = instance.funcion
            reservas = Reserva.objects.filter(funcion=funcion)
            ocupado_alumnos_previsto=0
            ocupado_alumnos_real=0
            ocupado_maestros_previsto=0
            ocupado_maestros_real=0
            visita_guiada_previsto=0
            visita_guiada_real=0
            for r in reservas:
                if r.id != instance.id and r.estado !='x':
                    ocupado_alumnos_previsto += r.alumnos_previsto
                    ocupado_maestros_previsto += r.maestros_previsto
                    ocupado_alumnos_real += r.alumnos_real
                    ocupado_maestros_real += r.maestros_real
                    
                    if r.visita_guiada:
                        if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                            # solo sumamos a los alumnos
                            visita_guiada_previsto += r.alumnos_previsto
                            visita_guiada_real += r.alumnos_real
                        else:
                            visita_guiada_previsto += r.alumnos_previsto + r.maestros_previsto
                            visita_guiada_real += r.alumnos_real + r.maestros_real
            #print 'Sin contar la actual instancia: ocupado maestros real: %d' % ocupado_maestros_real
            #print 'Sin contar la actual instancia: ocupado alumnos real: %d' % ocupado_alumnos_real
            funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto
            funcion.entradas_visitas_guiadas_real = visita_guiada_real
            
            #print 'RESERVA A SALVAR:'
            #print 'ANTES: %d' % funcion.aforo_maestros_real
            if instance.estado !='x': # Si no es una funcion cancelada
                funcion.aforo_alumnos_previsto = ocupado_alumnos_previsto + instance.alumnos_previsto 
                funcion.aforo_maestros_previsto = ocupado_maestros_previsto + instance.maestros_previsto
                funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos_previsto - instance.alumnos_previsto - ocupado_maestros_previsto - instance.maestros_previsto 
                
                funcion.aforo_alumnos_real = ocupado_alumnos_real + instance.alumnos_real 
                funcion.aforo_maestros_real = ocupado_maestros_real + instance.maestros_real
                funcion.aforo_libre_real = funcion.aforo_total - ocupado_alumnos_real - instance.alumnos_real - ocupado_maestros_real - instance.maestros_real
                #print '(ocupado_maestros_real + instance.maestros_real):  %d + %d' % (ocupado_maestros_real, instance.maestros_real)

                if instance.visita_guiada:
                    if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                        # solo sumamos a los alumnos
                        funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto + instance.alumnos_previsto
                        funcion.entradas_visitas_guiadas_real = visita_guiada_real + instance.alumnos_real
                    else:
                        funcion.entradas_visitas_guiadas_previsto = visita_guiada_previsto + instance.alumnos_previsto + instance.maestros_previsto
                        funcion.entradas_visitas_guiadas_real = visita_guiada_real + instance.alumnos_real + instance.maestros_real
                        
            else: #se cancela la reserva
                funcion.aforo_alumnos_previsto = ocupado_alumnos_previsto
                funcion.aforo_maestros_previsto = ocupado_maestros_previsto
                funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos_previsto - ocupado_maestros_previsto
                
                funcion.aforo_alumnos_real = ocupado_alumnos_real
                funcion.aforo_maestros_real = ocupado_maestros_real
                funcion.aforo_libre_real = funcion.aforo_total - ocupado_alumnos_real - ocupado_maestros_real
                #print '(ocupado_maestros_real):  %d' % ocupado_maestros_real
                
                
                    
                
            #print 'DESPUES: %d' % funcion.aforo_maestros_real
            #print 'salvamos la funcion'
            #print '-------------------'
            funcion.save()
            obra_campanya = funcion.obra_campanya
            obra_campanya.save()
            campanya = obra_campanya.campanya
            campanya.save()
            instance.save()
        
        formset.save_m2m()
    
teyatadmin.register(Aforo_Funcion, Aforo_Funcion_Admin)

class Recaudacion_FuncionResource(resources.ModelResource):
    campanya = fields.Field()
    obra = fields.Field()
    
    class Meta: 
        model = Recaudacion_Funcion
        fields = ( 'campanya', 'obra', 'fecha_hora', 'idioma__nombre',
                    'recaudacion_funcion_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista', 
                    'recaudacion_funcion_real', 'recaudacion_visitas_real',  'recaudacion_real',
                    )
        export_order = ( 'campanya', 'obra', 'fecha_hora', 'idioma__nombre',
                    'recaudacion_funcion_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista', 
                    'recaudacion_funcion_real', 'recaudacion_visitas_real',  'recaudacion_real',
                    )
    
    def dehydrate_campanya(self, funcion):
        return unicode(funcion.obra_campanya.campanya.nombre)
        
    def dehydrate_obra(self, funcion):
        return unicode(funcion.obra_campanya.obra.nombre)

class Recaudacion_Funcion_Stats(ExportMixin, StatsAdmin):
    change_list_template = 'admin/teyat/export_stats/change_list.html'
    change_form_template = 'admin/teyat/no_actions/change_form.html'
    pass
        
class Recaudacion_Funcion_Admin(Recaudacion_Funcion_Stats):
    model = Recaudacion_Funcion
    resource_class = Recaudacion_FuncionResource 
    list_display=('fecha_hora', 'obra', 'idioma', 'recaudacion_funcion_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista', 
       'recaudacion_funcion_real', 'recaudacion_visitas_real',  'recaudacion_real',)
    stats = (Sum('recaudacion_funcion_prevista'),Sum('recaudacion_visitas_prevista'),Sum('recaudacion_prevista'), Sum('recaudacion_funcion_real'),Sum('recaudacion_visitas_real'),Sum('recaudacion_real'),)    
    readonly_fields =['obra_campanya', 'fecha_hora', 'idioma','recaudacion_visitas_prevista', 'recaudacion_funcion_prevista', 'recaudacion_prevista', 
       'recaudacion_visitas_real', 'recaudacion_funcion_real', 'recaudacion_real',]
    list_filter=(CampanyaFilter_4_Funcion, ObraFilter_4_Funcion,IdiomaFilter_4_Funcion,)
    search_fields=('obra_campanya__obra__nombre',)
    date_hierarchy = 'fecha_hora'
    list_select_related = True
    
    fieldsets = [
                ((u'Recaudación'), {
                        #'classes': ('suit-tab suit-tab-recaudacion',),
                        'description': _(u'Se calcula automáticamente a partir de las reservas.'),
                        'fields': []
                }),
                (_(u'Prevista'), {
                        #'classes': ('suit-tab suit-tab-recaudacion',),
                        'fields': ['recaudacion_funcion_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista',],
                }), 
                (_(u'Real'), {
                        #'classes': ('suit-tab suit-tab-recaudacion',),
                        'fields': ['recaudacion_funcion_real', 'recaudacion_visitas_real', 'recaudacion_real',],
                }),
                #(None, {
                #        'classes': ('suit-tab suit-tab-funcion-reservas',),
                #        'fields': [],
                #}),
        ]

    def get_actions(self, request):
        actions = super(Recaudacion_Funcion_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions
        
    def get_queryset(self, request):
        qs = super(Recaudacion_Funcion_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(obra_campanya__campanya__owner=request.user.teatro_set.first())
    
    
    
teyatadmin.register(Recaudacion_Funcion, Recaudacion_Funcion_Admin)



##################################
##  CAMPAÑA Y OBRA EN CAMPAÑA   ##
##################################

            
class ObraEnCampanya_Form(ModelForm):
    
    class Meta:
        model = ObraEnCampanya 
        widgets = {
                'observaciones': Textarea(attrs={'rows':3, 'class':'span12'}),
                'obra': ObraWidget(attrs={'class': 'span12'}),
                'campanya': CampanyaWidget(attrs={'class': 'span6'}),
                }
        
        
        exclude = ('espectadores_previstos', 'espectadores_reales',
            'recaudacion_funciones_prevista', 'recaudacion_funciones_real',
            'recaudacion_visitas_prevista', 'recaudacion_visitas_real',
            'recaudacion_prevista', 'recaudacion_real',
            'maestros_previsto','maestros_real', 'alumnos_previsto','alumnos_real',
            'observaciones_es','observaciones_ca',)     

    
class ObraEnCampanya_InLine(ObjectPermissionsTabularInline):
    model = ObraEnCampanya
    form = ObraEnCampanya_Form
    extra= 0
    
    suit_classes= 'suit-tab suit-tab-obras-en-campanya'
    
    def get_formset(self, request, obj=None, **kwargs):
        # Hack! Hook the teatro from the user request
        self.teatro = request.user.teatro_set.first()
        return super(ObraEnCampanya_InLine, self).get_formset(
            request, obj, **kwargs)
    
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):

        if db_field.name == 'obra':
            kwargs['queryset'] = Obra.objects.filter(owner=self.teatro)

        return super(ObraEnCampanya_InLine, self).formfield_for_foreignkey(db_field, request=None, **kwargs)
    

class ObraFilter_4_ObraEnCampanya(admin.SimpleListFilter):
    title = _('Obra')
    parameter_name = 'obra'

    def lookups(self, request, model_admin):
        obras = Obra.objects.filter(owner__in=request.user.teatro_set.all())
        return [(o.id, o.nombre) for o in obras]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(obra__id__exact=self.value())
        else:
            return queryset
         
class CampanyaFilter_4_ObraEnCampanya(admin.SimpleListFilter):
    title = _('Campaña')
    parameter_name = 'campanya'

    def lookups(self, request, model_admin):
        campanyas = Campanya.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in campanyas]
        # You can also use hardcoded model name like "Campanya" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(campanya__id__exact=self.value())
        else:
            return queryset

class ObraEnCampanyaResource(resources.ModelResource):
    campanya = fields.Field()
    obra = fields.Field()
    num_centros_que_reservan = fields.Field()
    nombres_centros_que_reservan = fields.Field()
    
    class Meta: 
        model = ObraEnCampanya
        fields = ( 'obra','campanya', 
                   'num_centros_que_reservan', 
                   'nombres_centros_que_reservan', 
                   'espectadores_previstos', 
                   'recaudacion_funciones_prevista', 
                   'recaudacion_visitas_prevista',
                   'recaudacion_prevista',
                   'espectadores_reales', 
                   'recaudacion_funciones_real', 
                   'recaudacion_visitas_real',
                   'recaudacion_real',
                    )
        export_order = ( 'obra','campanya', 
                   'num_centros_que_reservan', 
                   'nombres_centros_que_reservan', 
                   'espectadores_previstos', 
                   'recaudacion_funciones_prevista', 
                   'recaudacion_visitas_prevista',
                   'recaudacion_prevista',
                   'espectadores_reales', 
                   'recaudacion_funciones_real', 
                   'recaudacion_visitas_real',
                   'recaudacion_real',
                    )
    
    def dehydrate_num_centros_que_reservan(self, obj):
        return unicode(obj.num_centros_que_reservan())
    
    def dehydrate_nombres_centros_que_reservan(self, obj):
        return unicode(obj.nombres_centros_que_reservan())
    
    def dehydrate_campanya(self, obj):
        return unicode(obj.campanya.nombre)
        
    def dehydrate_obra(self, obj):
        return unicode(obj.obra.nombre)


        
class ObraEnCampanya_Admin(ExportMixin, admin.ModelAdmin):
    resource_class = ObraEnCampanyaResource 
    form = ObraEnCampanya_Form
    list_display=('obra','campanya', 'num_centros_que_reservan', 'nombres_centros_que_reservan', 'espectadores_previstos', 'recaudacion_funciones_prevista', 'recaudacion_visitas_prevista','recaudacion_prevista','espectadores_reales', 'recaudacion_funciones_real', 'recaudacion_visitas_real','recaudacion_real','url_ficha',)
    search_fields=('obra__nombre','campanya__nombre',)
    list_filter=(ObraFilter_4_ObraEnCampanya, CampanyaFilter_4_ObraEnCampanya,)
    
    def url_ficha(self, obj):
        return '<a href="%s%s" target="_blank">Generar</a>' % (base_url, obj.ficha())
    url_ficha.allow_tags = True
    url_ficha.short_description = _(u'Ficha de taquilla')
    
    def get_queryset(self, request):
        qs = super(ObraEnCampanya_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(campanya__owner=request.user.teatro_set.first())
    
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'obra':
            kwargs['queryset'] = Obra.objects.filter(owner__in=request.user.teatro_set.all())
        if db_field.name == 'campanya':
            kwargs['queryset'] = Campanya.objects.filter(owner__in=request.user.teatro_set.all())
        
        return super(ObraEnCampanya_Admin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    
    
    
#admin.site.register(ObraEnCampanya, ObraEnCampanya_Admin)
#teyatadmin.register(ObraEnCampanya, ObraEnCampanya_Admin)
ravaladmin.register(ObraEnCampanya, ObraEnCampanya_Admin)
    
    
class Aforo_ObraEnCampanyaResource(resources.ModelResource):
    campanya = fields.Field()
    obra = fields.Field()
    num_centros_que_reservan = fields.Field()
    nombres_centros_que_reservan = fields.Field()
    
    class Meta: 
        model = Aforo_ObraEnCampanya
        fields = ( 'obra','campanya', 
                   'num_centros_que_reservan', 
                   'nombres_centros_que_reservan', 
                   'alumnos_previsto', 'maestros_previsto',
                   'espectadores_previstos', 
                   'alumnos_real', 'maestros_real',                   
                   'espectadores_reales', 
                    )
        export_order = ( 'obra','campanya', 
                   'num_centros_que_reservan', 
                   'nombres_centros_que_reservan', 
                   'alumnos_previsto', 'maestros_previsto',
                   'espectadores_previstos', 
                   'alumnos_real', 'maestros_real',                   
                   'espectadores_reales', 
                    )
    
    def dehydrate_num_centros_que_reservan(self, obj):
        return unicode(obj.num_centros_que_reservan())
    
    def dehydrate_nombres_centros_que_reservan(self, obj):
        return unicode(obj.nombres_centros_que_reservan())
    
    def dehydrate_campanya(self, obj):
        return unicode(obj.campanya.nombre)
        
    def dehydrate_obra(self, obj):
        return unicode(obj.obra.nombre)

class Aforo_ObraEnCampanya_Stats(ExportMixin, StatsAdmin):
    change_list_template = 'admin/teyat/export_stats/change_list.html'
    change_form_template = 'admin/teyat/no_actions/change_form.html'
    pass

        
class Aforo_ObraEnCampanya_Admin(Aforo_ObraEnCampanya_Stats):
    resource_class = Aforo_ObraEnCampanyaResource 
    model = Aforo_ObraEnCampanya
    list_display=('obra','campanya', 'num_centros_que_reservan', 'nombres_centros_que_reservan', 'espectadores_previstos', 'espectadores_reales', 'url_ficha',)
    stats = (Sum('alumnos_previsto'),Sum('maestros_previsto'),Sum('espectadores_previstos'), Sum('alumnos_real'),Sum('maestros_real'),Sum('espectadores_reales'),)
    search_fields=('obra__nombre','campanya__nombre',)
    list_filter=(ObraFilter_4_ObraEnCampanya, CampanyaFilter_4_ObraEnCampanya,)
    readonly_fields =['espectadores_previstos','espectadores_reales', 'maestros_previsto','maestros_real', 'alumnos_previsto','alumnos_real']
    
    fieldsets = [
                ((u'Asistencia'), {
                        'description': _(u'Se calcula automáticamente a partir de las funciones.'),
                        'fields': []
                }),
                (_(u'Prevista'), {
                        'fields': ['alumnos_previsto','maestros_previsto','espectadores_previstos',],
                }), 
                (_(u'Real'), {
                        'fields': ['alumnos_real','maestros_real','espectadores_reales',],
                }),
        ]
    
    def get_actions(self, request):
        actions = super(Aforo_ObraEnCampanya_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions
    
    def url_ficha(self, obj):
        return '<a href="%s%s" target="_blank">Generar</a>' % (base_url, obj.ficha())
    url_ficha.allow_tags = True
    url_ficha.short_description = _(u'Ficha de taquilla')
    
    
    def get_queryset(self, request):
        qs = super(Aforo_ObraEnCampanya_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(campanya__owner=request.user.teatro_set.first())
    

teyatadmin.register(Aforo_ObraEnCampanya, Aforo_ObraEnCampanya_Admin)

class Recaudacion_ObraEnCampanyaResource(resources.ModelResource):
    campanya = fields.Field()
    obra = fields.Field()
    
    class Meta: 
        model = Aforo_ObraEnCampanya
        fields = ( 'obra','campanya', 
                   'recaudacion_funciones_prevista', 
                   'recaudacion_visitas_prevista', 
                   'recaudacion_prevista',
                   'recaudacion_funciones_real', 
                   'recaudacion_visitas_real', 
                   'recaudacion_real', 
                    )
        export_order = ( 'obra','campanya', 
                   'recaudacion_funciones_prevista', 
                   'recaudacion_visitas_prevista', 
                   'recaudacion_prevista',
                   'recaudacion_funciones_real', 
                   'recaudacion_visitas_real', 
                   'recaudacion_real', 
                    )
    
    
    
    def dehydrate_campanya(self, obj):
        return unicode(obj.campanya.nombre)
        
    def dehydrate_obra(self, obj):
        return unicode(obj.obra.nombre)

class Recaudacion_ObraEnCampanya_Stats(ExportMixin, StatsAdmin):
    change_list_template = 'admin/teyat/export_stats/change_list.html'
    change_form_template = 'admin/teyat/no_actions/change_form.html'
    pass


class Recaudacion_ObraEnCampanya_Admin(Recaudacion_ObraEnCampanya_Stats):
    resource_class = Recaudacion_ObraEnCampanyaResource 
    model = Recaudacion_ObraEnCampanya
    list_display=('obra','campanya', 'recaudacion_funciones_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista','recaudacion_funciones_real', 'recaudacion_visitas_real', 'recaudacion_real', 'url_ficha',)
    stats = (Sum('recaudacion_funciones_prevista'),Sum('recaudacion_visitas_prevista'),Sum('recaudacion_prevista'), Sum('recaudacion_funciones_real'),Sum('recaudacion_visitas_real'),Sum('recaudacion_real'),)
    
    search_fields=('obra__nombre','campanya__nombre',)
    list_filter=(ObraFilter_4_ObraEnCampanya, CampanyaFilter_4_ObraEnCampanya,)
    readonly_fields =['recaudacion_funciones_prevista','recaudacion_funciones_real', 'recaudacion_visitas_prevista', 'recaudacion_visitas_real', 'recaudacion_prevista', 'recaudacion_real']
    
    fieldsets = [
                ((u'Recaudación'), {
                        'description': _(u'Se calcula automáticamente a partir de las funciones.'),
                        'fields': []
                }),
                (_(u'Prevista'), {
                        'fields': ['recaudacion_funciones_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista'],
                }), 
                (_(u'Real'), {
                        'fields': ['recaudacion_funciones_real', 'recaudacion_visitas_real','recaudacion_real'],
                }),
        ]
    
    def get_actions(self, request):
        actions = super(Recaudacion_ObraEnCampanya_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions
    
    def url_ficha(self, obj):
        return '<a href="%s%s" target="_blank">Generar</a>' % (base_url, obj.ficha())
    url_ficha.allow_tags = True
    url_ficha.short_description = _(u'Ficha de taquilla')
    
    def get_queryset(self, request):
        qs = super(Recaudacion_ObraEnCampanya_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(campanya__owner=request.user.teatro_set.first())

teyatadmin.register(Recaudacion_ObraEnCampanya, Recaudacion_ObraEnCampanya_Admin)

class Campanya_Form(ModelForm):
    class Meta:
        model = Campanya
        widgets = {
                'observaciones': Textarea(attrs={'rows':3, 'class':'span12'}),
                }
        
        exclude = ('espectadores_previstos', 'espectadores_reales',
            'recaudacion_funciones_prevista', 'recaudacion_funciones_real',
            'recaudacion_visitas_prevista', 'recaudacion_visitas_real',
            'recaudacion_prevista', 'recaudacion_real',
            'maestros_previsto','maestros_real', 'alumnos_previsto','alumnos_real',
            'nombre','owner',)     
    '''obras = forms.ModelMultipleChoiceField(queryset=Obra.objects.all(),required=False,widget=FilteredSelectMultiple(verbose_name=_(u'Obras'), is_stacked=True))
    # Overriding __init__ here allows us to provide initial
    # data for 'obras' field
    def __init__(self, *args, **kwargs):
        super(Campanya_Form, self).__init__(*args, **kwargs)
        # Only in case we build the form from an instance
        # (otherwise, 'biblio' list should be empty)
        if self.instance and self.instance.pk:
            #if 'instance' in kwargs:
            # We get the 'initial' keyword argument or initialize it
            # as a dict if it didn't exist.                
            initial = kwargs.setdefault('initial', {})
            # The widget for a ModelMultipleChoiceField expects
            # a list of primary key for the selected data.
            initial['productos'] = [t.pk for t in kwargs['instance'].obras.all()]

                forms.ModelForm.__init__(self, *args, **kwargs)
    '''  

class CampanyaResource(resources.ModelResource):
    nombre_obras = fields.Field()
    
    class Meta:
        model = Campanya
        fields = ( 'nombre', 'precio_obra', 'precio_visita_guiada', 'los_maestros_no_pagan', 'nombre_obras',
                   'creado')
        export_order = ( 'nombre', 'precio_obra', 'precio_visita_guiada', 'los_maestros_no_pagan', 'nombre_obras',
                   'creado')
    
    def dehydrate_nombre_obras(self, obj):
        return unidoce(obj.nombre_obras())
        
    def dehydrate_los_maestros_no_pagan(self, obj):
        if obj.los_maestros_no_pagan:
            return unicode(_(u'Sí'))
        else:
            return unicode(_(u'No'))
                
     
class Campanya_Admin_Raval(ExportMixin, admin.ModelAdmin):
    resource_class = CampanyaResource 
    form = Campanya_Form
    list_display=('nombre', 'nombre_obras', 'recaudacion_funciones_prevista','recaudacion_visitas_prevista','recaudacion_prevista', 'recaudacion_funciones_real','recaudacion_visitas_real','recaudacion_real')
    search_fields=('nombre_es','nombre_ca')
    inlines = [ObraEnCampanya_InLine,] 
    filter_horizontal= ('obras',)
    
    
    def get_fieldsets(self, request, obj=None):
        lang=request.user.teatro_set.first().idioma_mails
        
        if lang=='es':
            fieldsets = [
                (_(u'Nombre'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['nombre_es',]
                }),
                (_(u'Precios'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['precio_obra','precio_visita_guiada','los_maestros_no_pagan',]
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-obras-en-campanya',),
                        'fields': []
                }),
            ]
        elif lang == 'ca':
            fieldsets = [
                (_(u'Nombre'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['nombre_ca',]
                }),
                (_(u'Precios'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['precio_obra','precio_visita_guiada','los_maestros_no_pagan',]
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-obras-en-campanya',),
                        'fields': []
                }),
            ]
        else:
            fieldsets = [
                (_(u'Nombre'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['nombre_ca','nombre_es',]
                }),
                (_(u'Precios'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['precio_obra','precio_visita_guiada','los_maestros_no_pagan',]
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-obras-en-campanya',),
                        'fields': []
                }),
        ]

        return fieldsets
    
    
    suit_form_tabs = (('campanya', _(u'Campaña')),
                          ('obras-en-campanya', _(u'Obras')),
                          #('correo-campanya', _(u'Correos')),
                          )

    def get_queryset(self, request):
        qs = super(Campanya_Admin_Raval, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
        
    '''def get_form(self, request, obj=None, **kwargs):
        form = super(Campanya_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        form.base_fields['obras'].queryset = form.base_fields['obras'].queryset.filter(owner__in=request.user.teatro_set.all())
        return form
    '''
    
    
#admin.site.register(Campanya, Campanya_Admin)
#teyatadmin.register(Campanya, Campanya_Admin)
ravaladmin.register(Campanya, Campanya_Admin_Raval)

    
class Campanya_Admin(ExportMixin, StatsAdmin):
    resource_class = CampanyaResource 
    form = Campanya_Form
    list_display=('nombre', 'precio_obra', 'precio_visita_guiada', 'los_maestros_no_pagan', 'nombre_obras',)
    search_fields=('nombre_es','nombre_ca')
    inlines = [ObraEnCampanya_InLine,] 
    filter_horizontal= ('obras',)
    
    
    def get_fieldsets(self, request, obj=None):
        lang=request.user.teatro_set.first().idioma_mails
        
        if lang=='es':
            fieldsets = [
                (_(u'Nombre'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['nombre_es',]
                }),
                (_(u'Precios'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['precio_obra','precio_visita_guiada','los_maestros_no_pagan',]
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-obras-en-campanya',),
                        'fields': []
                }),
            ]
        elif lang == 'ca':
            fieldsets = [
                (_(u'Nombre'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['nombre_ca',]
                }),
                (_(u'Precios'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['precio_obra','precio_visita_guiada','los_maestros_no_pagan',]
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-obras-en-campanya',),
                        'fields': []
                }),
            ]
        else:
            fieldsets = [
                (_(u'Nombre'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['nombre_ca','nombre_es',]
                }),
                (_(u'Precios'), {
                        'classes': ('suit-tab suit-tab-campanya',),
                        'fields': ['precio_obra','precio_visita_guiada','los_maestros_no_pagan',]
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-obras-en-campanya',),
                        'fields': []
                }),
        ]

        return fieldsets
    
    
    suit_form_tabs = (('campanya', _(u'Campaña')),
                          ('obras-en-campanya', _(u'Obras')),
                     )

    def get_queryset(self, request):
        qs = super(Campanya_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
        
    
teyatadmin.register(Campanya, Campanya_Admin)
#ravaladmin.register(Campanya, Campanya_Admin_Raval)

class Aforo_CampanyaResource(resources.ModelResource):
    nombre_obras = fields.Field()
    
    class Meta:
        model = Aforo_Campanya
        fields = ( 'nombre', 'los_maestros_no_pagan', 'nombre_obras',
                   'alumnos_previsto', 'maestros_previsto', 'espectadores_previstos', 'maestros_real', 'alumnos_real','espectadores_reales',
                   )
        export_order = ( 'nombre', 'los_maestros_no_pagan', 'nombre_obras',
                   'alumnos_previsto', 'maestros_previsto', 'espectadores_previstos', 'maestros_real', 'alumnos_real','espectadores_reales',
                   )
    
    def dehydrate_nombre_obras(self, obj):
        return unicode(obj.nombre_obras())
        
    def dehydrate_los_maestros_no_pagan(self, obj):
        if obj.los_maestros_no_pagan:
            return unicode(_(u'Sí'))
        else:
            return unicode(_(u'No'))

class Aforo_Campanya_Stats(ExportMixin, StatsAdmin):
    change_list_template = 'admin/teyat/export_stats/change_list.html'
    change_form_template = 'admin/teyat/no_actions/change_form.html'

    pass
    
class Aforo_Campanya_Admin(Aforo_Campanya_Stats):
    resource_class = Aforo_CampanyaResource 
    model = Aforo_Campanya
    list_display=('nombre', 'espectadores_previstos', 'espectadores_reales')
    stats = (Sum('alumnos_previsto'),Sum('maestros_previsto'),Sum('espectadores_previstos'), Sum('alumnos_real'),Sum('maestros_real'),Sum('espectadores_reales'),)
    #search_fields=('nombre_es','nombre_ca')
    readonly_fields =['espectadores_previstos','espectadores_reales', 'maestros_previsto','maestros_real', 'alumnos_previsto','alumnos_real']
    
    fieldsets = [
                ((u'Asistencia'), {
                        'description': _(u'Se calcula automáticamente a partir de las obras de la campaña.'),
                        'fields': []
                }),
                (_(u'Prevista'), {
                        'fields': ['alumnos_previsto','maestros_previsto','espectadores_previstos',],
                }), 
                (_(u'Real'), {
                        'fields': ['alumnos_real','maestros_real','espectadores_reales',],
                }),
        ]
    
    def get_actions(self, request):
        actions = super(Aforo_Campanya_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions
    
    def get_queryset(self, request):
        qs = super(Aforo_Campanya_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
        
    
    
#admin.site.register(Campanya, Campanya_Admin)
teyatadmin.register(Aforo_Campanya, Aforo_Campanya_Admin)
#ravaladmin.register(Campanya, Campanya_Admin_Raval)


class Recaudacion_CampanyaResource(resources.ModelResource):
    nombre_obras = fields.Field()
    
    class Meta:
        model = Recaudacion_Campanya
        fields = ( 'nombre', 'los_maestros_no_pagan', 'nombre_obras',
                   'recaudacion_funciones_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista',
                   'recaudacion_funciones_real', 'recaudacion_visitas_real','recaudacion_real',
                   )
        export_order = ( 'nombre', 'los_maestros_no_pagan', 'nombre_obras',
                   'recaudacion_funciones_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista',
                   'recaudacion_funciones_real', 'recaudacion_visitas_real','recaudacion_real',
                   )
    
    def dehydrate_nombre_obras(self, obj):
        return unicode(obj.nombre_obras())
        
    def dehydrate_los_maestros_no_pagan(self, obj):
        if obj.los_maestros_no_pagan:
            return unicode(_(u'Sí'))
        else:
            return unicode(_(u'No'))

class Recaudacion_Campanya_Stats(ExportMixin, StatsAdmin):
    change_list_template = 'admin/teyat/export_stats/change_list.html'
    change_form_template = 'admin/teyat/no_actions/change_form.html'
    pass
   

class Recaudacion_Campanya_Admin(Recaudacion_Campanya_Stats):
    resource_class = Recaudacion_CampanyaResource 
    model = Recaudacion_Campanya
    list_display=('nombre', 'recaudacion_funciones_prevista', 'recaudacion_visitas_prevista','recaudacion_prevista', 'recaudacion_funciones_real','recaudacion_visitas_real','recaudacion_real')
    stats = (Sum('recaudacion_funciones_prevista'),Sum('recaudacion_visitas_prevista'),Sum('recaudacion_prevista'), Sum('recaudacion_funciones_real'),Sum('recaudacion_visitas_real'),Sum('recaudacion_real'),)
    search_fields=('nombre_es','nombre_ca')
    readonly_fields =['recaudacion_funciones_prevista','recaudacion_funciones_real', 'recaudacion_visitas_prevista', 'recaudacion_visitas_real', 'recaudacion_prevista', 'recaudacion_real']
    
    fieldsets = [
                ((u'Recaudación'), {
                        'description': _(u'Se calcula automáticamente a partir de las obras de la campaña.'),
                        'fields': []
                }),
                (_(u'Prevista'), {
                        'fields': ['recaudacion_funciones_prevista', 'recaudacion_visitas_prevista', 'recaudacion_prevista'],
                }), 
                (_(u'Real'), {
                        'fields': ['recaudacion_funciones_real', 'recaudacion_visitas_real','recaudacion_real'],
                }),
        ]
    
    def get_actions(self, request):
        actions = super(Recaudacion_Campanya_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions

    def get_queryset(self, request):
        qs = super(Recaudacion_Campanya_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
        
    
    
    
#admin.site.register(Campanya, Campanya_Admin)
teyatadmin.register(Recaudacion_Campanya, Recaudacion_Campanya_Admin)

 
###########################
##  COMPAÑIAS Y OBRAS    ##
########################### 
class Companyia_Form(ModelForm):

    class Meta:
        model = Companyia
        widgets = {
                'poblacion': PoblacionWidget(attrs={'class':'span4'}), 
                }       
        exclude = ('owner',)   

class CompanyiaResource(resources.ModelResource):
    obras = fields.Field()
    
    class Meta:
        model = Companyia
        fields = ( 'nombre', 'obras','direccion', 'poblacion__nombre', 'email', 'telefono',)
        export_order = ( 'nombre', 'obras','direccion', 'poblacion__nombre', 'email', 'telefono',)
    
    def dehydrate_obras(self, obj):
        return unicode(obj.obras())
            
        
class Companyia_Admin(ExportMixin, admin.ModelAdmin):
    resource_class = CompanyiaResource 
    form = Companyia_Form
    list_display=('nombre', 'email', 'telefono','obras')
    search_fields=('nombre','email','telefono',)
    
    def get_queryset(self, request):
        qs = super(Companyia_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def get_form(self, request, obj=None, **kwargs):
        form = super(Companyia_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        form.base_fields['poblacion'].queryset = form.base_fields['poblacion'].queryset.filter(owner__in=request.user.teatro_set.all())
        return form

    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
            
#admin.site.register(Companyia, Companyia_Admin)
teyatadmin.register(Companyia, Companyia_Admin)
ravaladmin.register(Companyia, Companyia_Admin)

class Obra_Form(ModelForm):

    class Meta:
        model = Obra
        widgets = {
                'companyia': CompanyiaWidget(attrs={'class':'span6'}), 
                }
        exclude = ('owner',) 
    
    '''
    TO-DO: Por ahora no controlamos si el aforo de una obra es diferente al del teatro.
    def clean(self):
        #run the standard clean method first
        cleaned_data=super(Obra_Form, self).clean()
        teatro_aforo = self.instance.owner.aforo
        obra_aforo =  cleaned_data.get('aforo_max')
        
        if obra_aforo > teatro_aforo:
            raise ValidationError(unicode(_(u'Error: El aforo para la obra es mayor que el aforo del teatro (')) + str(teatro_aforo) + unicode('). Si quiere modificar el aforo de su teatro, póngase en contacto con el servicio técnico de Teyat.es'))
        else:
            return cleaned_data
    '''

class Idiomas_4_Obra(admin.SimpleListFilter):
    title = _(u'Idioma')
    parameter_name = 'idiomas'

    def lookups(self, request, model_admin):
        idiomas = Idioma.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in idiomas]
        # You can also use hardcoded model name like "Comarca" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(idiomas__id__exact=self.value())
        else:
            return queryset
           
class CompanyiaFilter(admin.SimpleListFilter):
    title = _('Compañía')
    parameter_name = 'companyia'

    widgets = {
                'companyia': CompanyiaWidget(attrs={'class':'span4'}), 
                } 
    
    def lookups(self, request, model_admin):
        companyias = Companyia.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in companyias]
        # You can also use hardcoded model name like "Campanya" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(companyia__id__exact=self.value())
        else:
            return queryset 
            
            
class ObraResource(resources.ModelResource):
    campanyas = fields.Field()
    idiomas = fields.Field()
    edades = fields.Field()
    
    class Meta:
        model = Obra
        fields = ( 'nombre', 'idiomas', 'duracion', 'edades','aforo_max','companyia__nombre','campanyas',)
        export_order = ( 'nombre', 'idiomas', 'duracion', 'edades','aforo_max','companyia__nombre','campanyas',)
    
    def dehydrate_idiomas(self, obj):
        return unicode(obj.nombre_idiomas())
    def dehydrate_campanyas(self, obj):
        return unicode(obj.campanyas())
    def dehydrate_edades(self, obj):
        return unicode(obj.edades())
         
        
class Obra_Admin(ExportMixin, admin.ModelAdmin):
    resource_class = ObraResource 
    form = Obra_Form
    list_display=('nombre','companyia', 'nombre_idiomas', 'duracion', 'edades','aforo_max',)
    search_fields=('nombre',)
    list_filter=(CompanyiaFilter,Idiomas_4_Obra,'edad_min','edad_max',)
    filter_horizontal= ('idiomas',)
    
    def get_queryset(self, request):
        qs = super(Obra_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
        
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'idiomas':
            kwargs['queryset'] = Idioma.objects.filter(owner__in=request.user.teatro_set.all())
        
        return super(Obra_Admin, self).formfield_for_manytomany(db_field, request, **kwargs)

    
    def get_form(self, request, obj=None, **kwargs):
        form = super(Obra_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        form.base_fields['companyia'].queryset = form.base_fields['companyia'].queryset.filter(owner__in=request.user.teatro_set.all())
        form.base_fields['aforo_max'].initial = request.user.teatro_set.first().aforo
        return form
    
    
#admin.site.register(Obra, Obra_Admin)
teyatadmin.register(Obra, Obra_Admin)
ravaladmin.register(Obra, Obra_Admin)
    


    

###########################
##  CENTROS Y CONTACTOS  ##
###########################    
class Persona_contacto_Form(ModelForm):

    class Meta:
        model = Persona_contacto 
        widgets = {
                'observaciones': Textarea(attrs={'rows':5, 'class':'span12'}),
                'centro': CentroWidget(attrs={'class':'span4'}),
                }
                
        exclude = ('cargo_ca','cargo_es','observaciones_es','observaciones_ca', 'owner')     

class Persona_contacto_InLine_Formset(BaseInlineFormSet):
    
                
    def save_new(self, form, commit=True):
        obj = super(Persona_contacto_InLine_Formset, self).save_new(form, commit=False)
        # here you can add anything you need from the request
        obj.owner = self.request.user.teatro_set.first()

        if commit:
            obj.save()

        return obj
        
        
class Persona_contacto_InLine(ObjectPermissionsStackedInline):
    model  = Persona_contacto
    formset = Persona_contacto_InLine_Formset
    extra= 0
    exclude = ('cargo_ca','cargo_es','observaciones_es','observaciones_ca', 'owner') 
    suit_classes= 'suit-tab suit-tab-contactos'
    
                
    def get_formset(self, request, obj=None, **kwargs):
        formset = super(Persona_contacto_InLine, self).get_formset(request, obj, **kwargs)
        formset.request = request
        return formset
    
    
    
class CentroFilter_4_Persona_contacto(admin.SimpleListFilter):
    title = _('Centro')
    parameter_name = 'centro'

    def lookups(self, request, model_admin):
        centros = Centro.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in centros]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(centro__id__exact=self.value())
        else:
            return queryset
            
            
class Persona_contactoResource(resources.ModelResource):
    
    poblacion = fields.Field()
    class Meta:
        model = Persona_contacto
        fields = ( 'nombre','email', 'telefono', 'cargo', 'centro__nombre', 'poblacion', 'observaciones'
                    )
        export_order = ( 'nombre','email', 'telefono', 'cargo', 'centro__nombre', 'poblacion', 'observaciones'
                    )
                     
    def dehydrate_poblacion(self, obj):
        return unicode(obj.centro.poblacion.nombre_bonito())
            
class Persona_contacto_Admin(ExportMixin, admin.ModelAdmin):
    resource_class = Persona_contactoResource 
    form = Persona_contacto_Form
    list_display=('nombre', 'email', 'telefono', 'centro', 'cargo', 'observaciones',)
    list_filter=(CentroFilter_4_Persona_contacto,'centro__poblacion','centro__poblacion__comarca', 'centro__grado','centro__titularidad',)
    search_fields=('centro__nombre', 'nombre', 'email','telefono')
    
    
    def get_queryset(self, request):
        qs = super(Persona_contacto_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
        
    def get_form(self, request, obj=None, **kwargs):
        form = super(Persona_contacto_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        form.base_fields['centro'].queryset = form.base_fields['centro'].queryset.filter(owner__in=request.user.teatro_set.all())
        
        return form
#admin.site.register(Persona_contacto, Persona_contacto_Admin)
teyatadmin.register(Persona_contacto, Persona_contacto_Admin)
ravaladmin.register(Persona_contacto, Persona_contacto_Admin)


class PoblacionFilter_4_Centro(admin.SimpleListFilter):
    title = _(u'Población')
    parameter_name = 'poblacion'

    def lookups(self, request, model_admin):
        poblaciones = Poblacion.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre + ' - ' + str(c.codigo_postal) ) for c in poblaciones]
        # You can also use hardcoded model name like "Poblacion" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(poblacion__id__exact=self.value())
        else:
            return queryset
            
class ComarcaFilter_4_Centro(admin.SimpleListFilter):
    title = _(u'Comarca')
    parameter_name = 'poblacion__comarca'

    def lookups(self, request, model_admin):
        comarcas = Comarca.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in comarcas]
        # You can also use hardcoded model name like "Comarca" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(poblacion__comarca__id__exact=self.value())
        else:
            return queryset
            
class Centro_Form(ModelForm):

    class Meta:
        model = Centro
        widgets = {
                'poblacion': PoblacionWidget(attrs={'class':'span4'}),
                }
        fields = '__all__'

class CentroResource(resources.ModelResource):
    
    personas_contacto = fields.Field()
    poblacion = fields.Field()
    comarca = fields.Field()
    titularidad = fields.Field()
    grado = fields.Field()
    num_reservas_por_campanya = fields.Field()
    nombre_reservas_por_campanya = fields.Field()
    direccion_postal= fields.Field()
    class Meta:
        model = Centro
        fields = ( 'nombre', 'titularidad', 'grado', 'direccion_postal', 'direccion', 'poblacion', 'comarca', 'telefono','email', 'personas_contacto', 'num_reservas_por_campanya','nombre_reservas_por_campanya'
                    )
        export_order = ( 'nombre', 'titularidad', 'grado', 'direccion', 'poblacion', 'comarca', 'direccion_postal', 'telefono','email', 'personas_contacto', 'num_reservas_por_campanya','nombre_reservas_por_campanya'
                     )
    
    def dehydrate_titularidad(self, obj):
        return unicode(obj.get_titularidad_display())
    
    def dehydrate_grado(self, obj):
        return unicode(obj.get_grado_display())
        
    def dehydrate_direccion_postal(self, obj):
        return unicode(obj.direccion_postal())
        
    def dehydrate_poblacion(self, obj):
        if obj.poblacion:
            return unicode(obj.poblacion.nombre_bonito())
        else:
            return ''
    def dehydrate_comarca(self, obj):
        return unicode(obj.comarca())
            
    def dehydrate_personas_contacto(self,obj):
        return unicode(obj.personas_contacto())
        
    def dehydrate_num_reservas_por_campanya(self, obj):
        return unicode(obj.num_reservas_por_campanya())
        
    def dehydrate_nombre_reservas_por_campanya(self, obj):
        return unicode(obj.nombre_reservas_por_campanya())
        
class BulkDeleteCentroMixin(object):
    class SafeDeleteQuerysetWrapper(object):
        def __init__(self, wrapped_queryset):
            self.wrapped_queryset = wrapped_queryset

        def _safe_delete(self):
            #for obj in self.wrapped_queryset:
            #    obj.delete()
            for o in self.wrapped_queryset:
                reservas = Reserva.objects.filter(centro__id=o.id)
                for current_reserva in reservas: 
                    # obtenemos la funcion de la reserva original por si 
                    # el usuario ha modificado la funcion en el formulario
                    funcion = current_reserva.funcion 
                    #print 'RESERVA A ELIMINAR:'
                    #print 'ANTES'
                    #print funcion.aforo_libre_previsto
                    if current_reserva.estado !='x': # liberamos las butacas de esta reserva si ya había sido guardada previamente.
                        funcion.aforo_alumnos_previsto -=  current_reserva.alumnos_previsto
                        funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                        funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                        funcion.aforo_alumnos_real -=  current_reserva.alumnos_real
                        funcion.aforo_maestros_real -= current_reserva.maestros_real
                        funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                        
                        if current_reserva.visita_guiada:
                            if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                                funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                                funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                            else:
                                funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                                funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real
                        funcion.save()
                        obraencampanya = funcion.obra_campanya
                        obraencampanya.save()
                        campanya = obraencampanya.campanya
                        campanya.save()
                    current_reserva.delete()
                    #print 'DESPUES'
                    #print funcion.aforo_libre_previsto
                    #print 'salvamos la funcion'
                    #print '-------------------'
                o.delete()
                
        def __getattr__(self, attr):
            if attr == 'delete':
                return self._safe_delete
            else:
                return getattr(self.wrapped_queryset, attr)

        def __iter__(self):
            for obj in self.wrapped_queryset:
                yield obj

        def __getitem__(self, index):
            return self.wrapped_queryset[index]

        def __len__(self):
            return len(self.wrapped_queryset)

    def get_actions(self, request):
        actions = super(BulkDeleteCentroMixin, self).get_actions(request)
        actions['delete_selected'] = (BulkDeleteCentroMixin.action_safe_bulk_delete, 'delete_selected', _("Delete selected %(verbose_name_plural)s"))
        return actions

    def action_safe_bulk_delete(self, request, queryset):
        wrapped_queryset = BulkDeleteCentroMixin.SafeDeleteQuerysetWrapper(queryset)
        return delete_selected(self, request, wrapped_queryset)

class Centro_con_delete_guay(BulkDeleteCentroMixin, admin.ModelAdmin):
    pass
        
class Centro_Admin(ExportMixin, Centro_con_delete_guay):
    resource_class = CentroResource
    model = Centro
    form = Centro_Form
    inlines = [Persona_contacto_InLine] #, Reserva_InLine]
    list_display=( 'nombre', 'poblacion','comarca','telefono','email', 'nombre_reservas_por_campanya_html')
    ordering=('nombre',)
    readonly_fields =['creado', 'modificado',]
    list_filter=(PoblacionFilter_4_Centro, ComarcaFilter_4_Centro, 'grado','titularidad',)
    search_fields=('nombre', 'poblacion__nombre', 'email','telefono',)
    fieldsets = [
                (None, {
                        'classes': ('suit-tab suit-tab-nombre',),
                        'fields': ['nombre','titularidad', 'grado', 'direccion', 'poblacion', 'email', 'telefono','extension'] #,'ref']
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-contactos',),
                        'fields': []
                }),
                #(None, {
                #        'classes': ('suit-tab suit-tab-funcion-reservas',),
                #        'fields': []
                #}),        
        ]

    suit_form_tabs = (('nombre', _(u'Centro')),
                          ('contactos', _(u'Personas de contacto')),
                          #('funcion-reservas', _(u'Reservas')),
                          )
    
    
    def get_queryset(self, request):
        qs = super(Centro_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
    
    def get_form(self, request, obj=None, **kwargs):
        form = super(Centro_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        form.base_fields['poblacion'].queryset = form.base_fields['poblacion'].queryset.filter(owner__in=request.user.teatro_set.all())
        return form 
    
    def delete_model(self, request, obj):
        reservas = Reserva.objects.filter(centro__id=obj.id) 
        for current_reserva in reservas:
            # obtenemos la funcion de la reserva original por si 
            # el usuario ha modificado la funcion en el formulario
            funcion = current_reserva.funcion 
            #print 'RESERVA A ELIMINAR:'
            #print current_reserva
            #print 'AFORO ANTES'
            #print funcion.aforo_libre_previsto        
                
            if current_reserva.estado !='x': # liberamos las butacas de esta reserva si ya había sido guardada previamente.
                funcion.aforo_alumnos_previsto -=  current_reserva.alumnos_previsto
                funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                funcion.aforo_alumnos_real -=  current_reserva.alumnos_real
                funcion.aforo_maestros_real -= current_reserva.maestros_real
                funcion.aforo_libre_real += current_reserva.alumnos_real + current_reserva.maestros_real
                     
                if current_reserva.visita_guiada:
                    if funcion.obra_campanya.campanya.los_maestros_no_pagan:
                        funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                        funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real
                    else:
                        funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto - current_reserva.maestros_previsto
                        funcion.entradas_visitas_guiadas_real -= current_reserva.alumnos_real - current_reserva.maestros_real
                funcion.save()
                #print 'AFORO DESPUES'
                #print funcion.aforo_libre_previsto  
                obraencampanya = funcion.obra_campanya
                obraencampanya.save()
                campanya = obraencampanya.campanya
                campanya.save()
            current_reserva.delete()
        obj.delete()
    
    '''
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        
        #print 'Save_formset de Centro'
        for obj in formset.deleted_objects:
            funcion = getattr(obj, 'funcion', False)
            if funcion:
                current_reserva = Reserva.objects.get(id=obj.id) 
                # obtenemos la funcion de la reserva original por si 
                # el usuario ha modificado la funcion en el formulario
                funcion = current_reserva.funcion 
                #print 'RESERVA A ELIMINAR:'
                #print 'ANTES'
                #print funcion.aforo_libre_previsto
                #print obj.estado
                if current_reserva.estado !='x': # liberamos las butacas de esta reserva si ya había sido guardada previamente.
                    funcion.aforo_alumnos_previsto -=  current_reserva.alumnos_previsto
                    funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                    funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                    if current_reserva.visita_guiada:
                        funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                    funcion.save()
                #print 'DESPUES'
                #print funcion.aforo_libre_previsto
                #print 'salvamos la funcion'
                #print '-------------------'
                    
            obj.delete()
                
            
        for instance in instances:
            funcion = getattr(instance, 'funcion', False)
            if funcion:
                #print instance
                if instance.id:
                    #print 'la instancia tiene id'
                    current_reserva = Reserva.objects.get(id=instance.id)
                else:
                    #print 'la instancia NO tiene id'
                    current_reserva = instance
                # Si se ha modificado la funcion, debemos actualizar el aforo de la funcion anterior
                if current_reserva.funcion != instance.funcion:
                    # modificamos el aforo de la funcion anterior:
                    old_funcion = current_reserva.funcion
                    #print 'modificamos la funcion anterior:'
                    #print old_funcion
                    #print 'ANTES'
                    #print old_funcion.aforo_libre_previsto
                
                    if current_reserva.estado!='x':
                        old_funcion.aforo_alumnos_previsto -= current_reserva.alumnos_previsto
                        old_funcion.aforo_maestros_previsto -= current_reserva.maestros_previsto
                        old_funcion.aforo_libre_previsto += current_reserva.alumnos_previsto + current_reserva.maestros_previsto
                        if current_reserva.visita_guiada:
                            old_funcion.entradas_visitas_guiadas_previsto -= current_reserva.alumnos_previsto
                        old_funcion.save()
                    #print 'DESPUES'
                    #print old_funcion.aforo_libre_previsto
                    #print 'salvamos la funcion'
                    #print '-------------------'
                
                # ahora actualizamos el aforo de la función actual.
                #print 'actualizamos la funcion actual:'
                #print instance.funcion
                funcion = instance.funcion
                reservas = Reserva.objects.filter(funcion=funcion)
                ocupado_alumnos=0
                ocupado_maestros=0
                visita_guiada=0
                for r in reservas:
                    if r.id != instance.id and r.estado !='x':
                        ocupado_alumnos += r.alumnos_previsto
                        ocupado_maestros += r.maestros_previsto
                        if r.visita_guiada:
                            visita_guiada += r.alumnos_previsto
                #print 'RESERVA A SALVAR:'
                #print 'ANTES'
                #print funcion.aforo_libre_previsto
                if instance.estado !='x': # Si no es una funcion cancelada
                    funcion.aforo_alumnos_previsto = ocupado_alumnos + instance.alumnos_previsto 
                    funcion.aforo_maestros_previsto = ocupado_maestros + instance.maestros_previsto
                    funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos - instance.alumnos_previsto - ocupado_maestros - instance.maestros_previsto 
                    if instance.visita_guiada:
                        funcion.entradas_visitas_guiadas_previsto = visita_guiada + instance.alumnos_previsto
                            
                else: #se calcela la reserva
                    funcion.aforo_alumnos_previsto = ocupado_alumnos
                    funcion.aforo_maestros_previsto = ocupado_maestros
                    funcion.aforo_libre_previsto = funcion.aforo_total - ocupado_alumnos - ocupado_maestros 
                    if instance.visita_guiada:
                        funcion.entradas_visitas_guiadas_previsto = visita_guiada
                    
                #print 'DESPUES'
                #print funcion.aforo_libre_previsto
                #print 'salvamos la funcion'
                #print '-------------------'
                funcion.save()
                
            instance.save()
        formset.save_m2m()
        '''
            
    
    
    
    
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
    
#admin.site.register(Centro, Centro_Admin)
teyatadmin.register(Centro, Centro_Admin)
ravaladmin.register(Centro, Centro_Admin)

    
###########################
##         CURSO ESCOLAR ##
###########################
class Curso_Admin(ObjectPermissionsModelAdmin):
    model = Curso
    search_fields=('nombre',)
    #exclude=('nombre','owner',) 
    
    def get_queryset(self, request):
        qs = super(Curso_Admin, self).get_queryset(request)
        #if request.user.is_superuser:
        return qs
        #return qs.filter(owner=request.user.teatro_set.first())

    def save_model(self, request, obj, form, change):
        #obj.owner = request.user.teatro_set.first()
        obj.save()
    
teyatadmin.register(Curso, Curso_Admin)
ravaladmin.register(Curso, Curso_Admin)


###########################
##         IDIOMA        ##
###########################
class Idioma_Admin(ObjectPermissionsModelAdmin):
    model = Idioma
    search_fields=('nombre_es','nombre_ca')
    exclude=('nombre','owner',) 
    
    def get_queryset(self, request):
        qs = super(Idioma_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
    
#admin.site.register(Idioma, Idioma_Admin)
teyatadmin.register(Idioma, Idioma_Admin)
ravaladmin.register(Idioma, Idioma_Admin)

###########################
## COMARCA Y POBLACION   ##
###########################
class Comarca_Admin(ObjectPermissionsModelAdmin):
    model = Comarca
    search_fields=('nombre',)
    exclude=('owner',)
    
    def get_queryset(self, request):
        qs = super(Comarca_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
        
#admin.site.register(Comarca, Comarca_Admin)
teyatadmin.register(Comarca, Comarca_Admin)
ravaladmin.register(Comarca, Comarca_Admin)


class Poblacion_Form(ModelForm):

    class Meta:
        model = Poblacion
        widgets = {
                'comarca': ComarcaWidget(attrs={'class':'span4'}),
                }
        fields = '__all__'

class ComarcaFilter_4_Poblacion(admin.SimpleListFilter):
    title = _(u'Comarca')
    parameter_name = 'comarca'

    def lookups(self, request, model_admin):
        comarcas = Comarca.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in comarcas]
        # You can also use hardcoded model name like "Comarca" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(comarca__id__exact=self.value())
        else:
            return queryset
        
class Poblacion_Admin(ObjectPermissionsModelAdmin):
    model = Poblacion
    form = Poblacion_Form
    list_display=('nombre', 'codigo_postal', 'comarca',)
    search_fields=('nombre', 'codigo_postal','comarca__nombre',)
    list_filter=(ComarcaFilter_4_Poblacion,'codigo_postal', )
    exclude=('owner',)
    
    def get_queryset(self, request):
        qs = super(Poblacion_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())

    def get_form(self, request, obj=None, **kwargs):
        form = super(Poblacion_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        if not request.user.is_superuser:
            form.base_fields['comarca'].queryset = form.base_fields['comarca'].queryset.filter(owner__in=request.user.teatro_set.all())
        
        return form
    
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
    
#admin.site.register(Poblacion, Poblacion_Admin)
teyatadmin.register(Poblacion, Poblacion_Admin)
ravaladmin.register(Poblacion, Poblacion_Admin)
   

###########################
## CORREOS DE RESERVA    ##
###########################

class CorreoConfirmacionReserva_Form(ModelForm):
    
    class Meta:
        model = CorreoConfirmacionReserva
        
        widgets = {
                'cuerpo_pre_detalle_es': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
                'cuerpo_pre_detalle_ca': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
                'cuerpo_post_detalle_es': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
                'cuerpo_post_detalle_ca': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
            }
                
        exclude = ('saludo','cuerpo_pre_detalle','cuerpo_post_detalle','owner',)            
        


class CorreoConfirmacionReserva_Admin(admin.ModelAdmin):
    change_list_template = 'admin/teyat/correos/change_list.html'
    save_as = True
    form = CorreoConfirmacionReserva_Form
    list_display=('estado', 'activo', 'asunto', 'test_mail')
    
    def get_fieldsets(self, request, obj=None):
        lang=request.user.teatro_set.first().idioma_mails
        
        if lang=='es':
            fieldsets = [
                #(_(u'Consejo:'), {
                #        'description': _(u'Si editas este correo y presionas "Grabar" se guardarán los cambios. Si presionas "Grabar como nuevo" se creará una copia nueva.<br/>Una forma perfecta para obtener fácilmente tus correo para cada estado de reserva.'),
                #        'fields': [],
                #}),
                (_(u'Estado'), {
                        'fields': ['estado','activo',]
                }),
                (_(u'Destinatarios'), {
                        'fields': ['destinatario']
                }),
                (_(u'Asunto'), {
                        'fields': ['asunto']
                }),
                (_(u'Cuerpo'), {
                        'fields': ['saludo_es','cuerpo_pre_detalle_es','detalle_reserva','cuerpo_post_detalle_es']
                }),
            ]
        elif lang == 'ca':
            fieldsets = [
                #(_(u'Consejo:'), {
                #        'description': _(u'Si editas este correo y presionas "Grabar" se guardarán los cambios. Si presionas "Grabar como nuevo" se creará una copia nueva.<br/>Una forma perfecta para obtener fácilmente tus correo para cada estado de reserva.'),
                #        'fields': [],
                #}),

                (_(u'Estado'), {
                        'fields': ['estado','activo',]
                }),
                (_(u'Destinatarios'), {
                        'fields': ['destinatario']
                }),
                (_(u'Asunto'), {
                        'fields': ['asunto']
                }),
                (_(u'Cuerpo'), {
                        'fields': ['saludo_ca','cuerpo_pre_detalle_ca','detalle_reserva','cuerpo_post_detalle_ca']
                }),
            ]
        else:
            fieldsets = [
                #(_(u'Consejo:'), {
                #        'description': _(u'Si editas este correo y presionas "Grabar" se guardarán los cambios. Si presionas "Grabar como nuevo" se creará una copia nueva.<br/>Una forma perfecta para obtener fácilmente tus correo para cada estado de reserva.'),
                #        'fields': [],
                #}),

                (_(u'Estado'), {
                        'fields': ['estado','activo',]
                }),
                (_(u'Destinatarios'), {
                        'fields': ['destinatario']
                }),
                (_(u'Asunto'), {
                        'fields': ['asunto']
                }),
                (_(u'Cuerpo'), {
                        'fields': ['saludo_ca','saludo_es','cuerpo_pre_detalle_ca','cuerpo_pre_detalle_es','detalle_reserva','cuerpo_post_detalle_ca','cuerpo_post_detalle_es']
                }),
            ]

        return fieldsets
    
    def get_queryset(self, request):
        qs = super(CorreoConfirmacionReserva_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())


    def test_mail(self, obj):
        return '<a href="%smail/test-alta-reserva-%d" target="_blank">' % (base_url, obj.id) + unicode(_(u'Enviar a tu correo')) + '</a>'        
        #return '<a href="%smail/test-alta-reserva-%d" target="_blank">Enviar a %s</a>' % (base_url, obj.id, obj.owner.emailcampanya)
    test_mail.allow_tags = True
    test_mail.short_description = _(u'Test correo')
    
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
    
#admin.site.register(CorreoConfirmacionReserva, CorreoConfirmacionReserva_Admin)
teyatadmin.register(CorreoConfirmacionReserva, CorreoConfirmacionReserva_Admin)
ravaladmin.register(CorreoConfirmacionReserva, CorreoConfirmacionReserva_Admin)

'''
class ObraFilter_4_Adjunto(admin.SimpleListFilter):
    title = _(u'Obra')
    parameter_name = 'correos'

    def lookups(self, request, model_admin):
        obras = Obra.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in obras]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(correos__obra_campanya__obra__id__exact=self.value())
        else:
            return queryset

class CampanyaFilter_4_Adjunto(admin.SimpleListFilter):
    title = _(u'Campaña')
    parameter_name = 'correo'

    def lookups(self, request, model_admin):
        campanyas = Campanya.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in campanyas]
        # You can also use hardcoded model name like "Campanya" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(correo__obra_campanya__campanya__id__exact=self.value())
        else:
            return queryset
'''

class Adjunto_Form(ModelForm):
    
    class Meta:
        model = Adjunto 
                
        exclude = ('owner',)


class AdjuntoCorreo_InLine(ObjectPermissionsTabularInline):
    model = AdjuntoCorreo
    extra= 0
    
    suit_classes= 'suit-tab suit-tab-adjunto-en-correo'
    
    def get_formset(self, request, obj=None, **kwargs):
        # Hack! Hook the teatro from the user request
        self.teatro = request.user.teatro_set.first()
        return super(AdjuntoCorreo_InLine, self).get_formset(
            request, obj, **kwargs)
    
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):

        if db_field.name == 'correo':
            kwargs['queryset'] = CorreoReserva.objects.filter(owner=self.teatro)
        if db_field.name == 'adjunto':
            kwargs['queryset'] = Adjunto.objects.filter(owner=self.teatro)
        return super(AdjuntoCorreo_InLine, self).formfield_for_foreignkey(db_field, request=None, **kwargs)
    
'''
class AdjuntoCorreo_Admin(ObjectPermissionsModelAdmin):
    model = AdjuntoCorreo
admin.site.register(AdjuntoCorreo, AdjuntoCorreo_Admin)
'''

class Adjunto_Admin_raval(ObjectPermissionsModelAdmin):
    model = Adjunto
    list_display=('nombre','nombre_correos')
    search_fields=('nombre',)
    
    inlines = [AdjuntoCorreo_InLine,] 
    fieldsets = [
                (None, {
                        'classes': ('suit-tab suit-tab-adjunto',),
                        'fields': ['nombre','fichero']
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-adjunto-en-correo',),
                        'fields': []
                }),
        ]
        
    suit_form_tabs = (('adjunto', _(u'Fichero adjunto')),
                          ('adjunto-en-correo', _(u'Correos')),
                          )
    
    
    def get_queryset(self, request):
        qs = super(Adjunto_Admin_raval, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
    
class Adjunto_Admin(ObjectPermissionsModelAdmin):
    model = Adjunto
    list_display=('nombre','nombre_correos')
    search_fields=('nombre',)
    
    inlines = [AdjuntoCorreo_InLine,] 
    fieldsets = [
                (None, {
                        'fields': ['nombre','fichero']
                }),
        ]
        
    
    def get_queryset(self, request):
        qs = super(Adjunto_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())
        
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
    
#admin.site.register(Adjunto, Adjunto_Admin)
teyatadmin.register(Adjunto, Adjunto_Admin)
ravaladmin.register(Adjunto, Adjunto_Admin_raval)




class ObraFilter_4_Correo(admin.SimpleListFilter):
    title = _(u'Obra')
    parameter_name = 'obra_campanya'

    def lookups(self, request, model_admin):
        obras = Obra.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in obras]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(obra_campanya__obra__id=self.value())
        else:
            return queryset

class CampanyaFilter_4_Correo(admin.SimpleListFilter):
    title = _(u'Campaña')
    parameter_name = 'obra_campanya'

    def lookups(self, request, model_admin):
        campanyas = Campanya.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre) for c in campanyas]
        # You can also use hardcoded model name like "Campanya" instead of 
        # "model_admin.model" if this is not direct foreign key filter
 
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(obra_campanya__campanya__id__exact=self.value())
        else:
            return queryset

            
class CorreoReserva_Form(ModelForm):
    
    class Meta:
        model = CorreoReserva 
        widgets = {
                'obra_campanya': ObraCampanyaWidget(attrs={'class':'span12'}),
                'cuerpo_pre_detalle_es': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
                'cuerpo_pre_detalle_ca': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
                'cuerpo_post_detalle_es': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
                'cuerpo_post_detalle_ca': MarkdownWidget(attrs={'rows':5, 'class':'span12'}),
            }

        exclude = ('saludo','cuerpo_pre_detalle','cuerpo_post_detalle','owner',)

    

class CorreoReserva_Admin(admin.ModelAdmin):
    change_list_template = 'admin/teyat/correos/change_list.html'
    save_as = True
    form = CorreoReserva_Form
    list_display=('obra_campanya', 'antes', 'dias', 'hora', 'hay_adjuntos','activo', 'test_mail', )
    search_fields=('obra_campanya__obra__nombre',)
    #list_filter=(CampanyaFilter_4_Correo, ObraFilter_4_Correo) 
    # ambos filtros se aplican sobre obra_campanya. Solo podemos usar uno de ellos. 
    list_filter=(ObraFilter_4_Correo, 'activo',)
    inlines = [AdjuntoCorreo_InLine,] 
        
    suit_form_tabs = (('correo', _(u'Correo')),
                          ('adjunto-en-correo', _(u'Adjuntos')),
                          )
    
    def get_fieldsets(self, request, obj=None):
        lang=request.user.teatro_set.first().idioma_mails
        
        if lang=='es':
            fieldsets = [
                #('<i class="icon-info-sign"></i>', {
                #        'description': _(u'Si editas este correo y presionas "Grabar" se guardarán los cambios. Si presionas "Grabar como nuevo" se creará una copia nueva.<br/>Una forma perfecta para reutilizar el correo recordatorio de una obra sin tener que empezar de cero.'),
                #        'fields': [],
                #}),
                (_(u'Estado'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'description': _(u'Solo se mandan correos Activos. El sistema lo desactivará automáticamente cuando se hayan cumplido todas las reglas de envío que definas.'),
                        'fields': ['activo',]
                }),
                (_(u'Obra y Campaña'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['obra_campanya',]
                }),
                (_(u'Reglas de envío'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['antes','dias','hora',]
                }),                
                (_(u'Destinatarios'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['destinatario']
                }),
                (_(u'Asunto'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['asunto']
                }),
                (_(u'Cuerpo'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['saludo_es','cuerpo_pre_detalle_es','show_detalle','cuerpo_post_detalle_es']
                }),
                ('<i class="icon-question-sign"></i>', {
                        'description': _(u'Presiona "Agregar Adjunto Adicional" y elige un adjunto del desplegable. Crea un nuevo adjunto presionando <i class="icon-plus-sign"></i>.<br/>Edita un adjunto existente seleccionándolo en el desplegable y presionando <i class="icon-pencil"></i>. Añade más adjuntos presionando sobre "Agregar Adjunto Adicional"'),
                        'classes': ('suit-tab suit-tab-adjunto-en-correo',),
                        'fields': [],
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-adjunto-en-correo',),
                        
                        'fields': []
                }),
            ]
        elif lang == 'ca':
            fieldsets = [
                #('<i class="icon-question-sign"></i>', {
                #        'description': _(u'Si editas este correo y presionas "Grabar" se guardarán los cambios. Si presionas "Grabar como nuevo" se creará una copia nueva.<br/>Una forma perfecta para reutilizar el correo recordatorio de una obra sin tener que empezar de cero.'),
                #        'fields': [],
                #}),
                (_(u'Estado'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'description': _(u'Solo se mandan correos Activos. El sistema lo desactivará automáticamente cuando se hayan cumplido todas las reglas de envío que definas.'),
                        'fields': ['activo',]
                }),
                (_(u'Obra y Campaña'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['obra_campanya',]
                }),
                (_(u'Reglas de envío'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['antes','dias','hora',]
                }),                
                (_(u'Destinatarios'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['destinatario']
                }),
                (_(u'Asunto'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['asunto']
                }),
                (_(u'Cuerpo'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['saludo_ca','cuerpo_pre_detalle_ca','show_detalle','cuerpo_post_detalle_ca']
                }),
                ('<i class="icon-question-sign"></i>', {
                        'description': _(u'Presiona "Agregar Adjunto Adicional" y elige un adjunto del desplegable. Crea un nuevo adjunto presionando <i class="icon-plus-sign"></i>.<br/>Edita un adjunto existente seleccionándolo en el desplegable y presionando <i class="icon-pencil"></i>. Añade más adjuntos presionando sobre "Agregar Adjunto Adicional"'),
                        'classes': ('suit-tab suit-tab-adjunto-en-correo',),
                        'fields': [],
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-adjunto-en-correo',),
                        'fields': []
                }),
            ]
        else:
            fieldsets = [
                #('<i class="icon-question-sign"></i>', {
                #        'description': _(u'Si editas este correo y presionas "Grabar" se guardarán los cambios. Si presionas "Grabar como nuevo" se creará una copia nueva.<br/>Una forma perfecta para reutilizar el correo recordatorio de una obra sin tener que empezar de cero.'),
                #        'fields': [],
                #}),
                (_(u'Estado'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'description': _(u'Solo se mandan correos Activos. El sistema lo desactivará automáticamente cuando se hayan cumplido todas las reglas de envío que definas.'),
                        'fields': ['activo',]
                }),
                (_(u'Obra y Campaña'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['obra_campanya',]
                }),
                (_(u'Reglas de envío'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['antes','dias','hora',]
                }),                
                (_(u'Destinatarios'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['destinatario']
                }),
                (_(u'Asunto'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['asunto']
                }),
                (_(u'Cuerpo'), {
                        'classes': ('suit-tab suit-tab-correo',),
                        'fields': ['saludo_ca','saludo_es','cuerpo_pre_detalle_ca','cuerpo_pre_detalle_es','show_detalle','cuerpo_post_detalle_ca','cuerpo_post_detalle_es']
                }),
                ('<i class="icon-question-sign"></i>', {
                        'description': _(u'Presiona "Agregar Adjunto Adicional" y elige un adjunto del desplegable. Crea un nuevo adjunto presionando <i class="icon-plus-sign"></i>.<br/>Edita un adjunto existente seleccionándolo en el desplegable y presionando <i class="icon-pencil"></i>. Añade más adjuntos presionando sobre "Agregar Adjunto Adicional"'),
                        'classes': ('suit-tab suit-tab-adjunto-en-correo',),
                        'fields': [],
                }),
                (None, {
                        'classes': ('suit-tab suit-tab-adjunto-en-correo',),
                        'fields': []
                }),
            ]

        return fieldsets
    
    
    def get_queryset(self, request):
        qs = super(CorreoReserva_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user.teatro_set.first())

    def test_mail(self, obj):
        return '<a href="%smail/test-recordatorio-reserva-%d" target="_blank">' % (base_url, obj.id) + unicode(_(u'Enviar a tu correo')) + '</a>' 
        #return '<a href="%smail/test-recordatorio-reserva-%d" target="_blank">Enviar a %s</a>' % (base_url, obj.id, obj.owner.emailcampanya)
    test_mail.allow_tags = True
    test_mail.short_description = _(u'Test correo')
    
    
    
    
    def get_form(self, request, obj=None, **kwargs):
        form = super(CorreoReserva_Admin,self).get_form(request, obj,**kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        #we modify the the queryset
        if not request.user.is_superuser:
            form.base_fields['obra_campanya'].queryset = form.base_fields['obra_campanya'].queryset.filter(campanya__owner=request.user.teatro_set.first(), funcion__isnull=False).distinct()
        
        return form
    
    def save_model(self, request, obj, form, change):
        obj.owner = request.user.teatro_set.first()
        obj.save()
    
#admin.site.register(CorreoReserva, CorreoReserva_Admin)
teyatadmin.register(CorreoReserva, CorreoReserva_Admin)
ravaladmin.register(CorreoReserva, CorreoReserva_Admin)

class CorreoConfirmacionReservaFilter(admin.SimpleListFilter):
    title = _('Correo')
    parameter_name = 'correo'

    def lookups(self, request, model_admin):
        correos = CorreoConfirmacionReserva.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre()) for c in correos]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(correo__id__exact=self.value())
        else:
            return queryset

class CorreoReservaFilter(admin.SimpleListFilter):
    title = _('Correo')
    parameter_name = 'correo'

    def lookups(self, request, model_admin):
        correos = CorreoReserva.objects.filter(owner__in=request.user.teatro_set.all())
        return [(c.id, c.nombre()) for c in correos]
        # You can also use hardcoded model name like "Obra" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(correo__id__exact=self.value())
        else:
            return queryset

            
class CentroFilter_4_LogCorreos(admin.SimpleListFilter):
    title = _('Centro')
    parameter_name = 'reserva__centro'

    def lookups(self, request, model_admin):
        centros = Centro.objects.filter(owner=request.user.teatro_set.first())
        return [(c.id, c.__unicode__) for c in centros]
        # You can also use hardcoded model name like "Centro" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(reserva__centro__id__exact=self.value())
        else:
            return queryset
            
class FuncionFilter_4_LogCorreos(admin.SimpleListFilter):
    title = _('Función')
    parameter_name = 'reserva__funcion'

    def lookups(self, request, model_admin):
        funciones = Funcion.objects.filter(obra_campanya__obra__owner__in=request.user.teatro_set.all())
        return [(f.id, f.__unicode__) for f in funciones]
        # You can also use hardcoded model name like "Funcion" instead of 
        # "model_admin.model" if this is not direct foreign key filter

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(reserva__funcion__id__exact=self.value())
        else:
            return queryset
            
class LogCorreosRecordatorios_Admin(admin.ModelAdmin):

    model = LogCorreosRecordatorios
    list_display=('enviado_en', 'reserva', 'destinatario', 'correo', )
    list_filter=(CorreoReservaFilter, FuncionFilter_4_LogCorreos, CentroFilter_4_LogCorreos,)
    fields= ('reserva', 'correo', 'enviado_en',)
    readonly_fields =('reserva', 'correo', 'enviado_en',)
    date_hierarchy = 'enviado_en'
    list_select_related = True
    
    change_form_template = 'admin/teyat/no_actions/change_form.html'
    
    def get_actions(self, request):
        actions = super(LogCorreosRecordatorios_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions
    
    def get_queryset(self, request):
        qs = super(LogCorreosRecordatorios_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(correo__owner=request.user.teatro_set.first())
    
teyatadmin.register(LogCorreosRecordatorios, LogCorreosRecordatorios_Admin)
ravaladmin.register(LogCorreosRecordatorios, LogCorreosRecordatorios_Admin)


class LogCorreosEstadoReserva_Admin(admin.ModelAdmin):

    model = LogCorreosEstadoReserva
    list_display=('enviado_en', 'correo', 'reserva', 'destinatario', 'resend_mail')
    list_filter=(CorreoConfirmacionReservaFilter, FuncionFilter_4_LogCorreos, CentroFilter_4_LogCorreos,)
    fields= ('reserva', 'correo', 'enviado_en',)
    readonly_fields =('reserva', 'correo', 'enviado_en',)
    date_hierarchy = 'enviado_en'
    list_select_related = True
    
    change_form_template = 'admin/teyat/no_actions/change_form.html'
    
    def get_actions(self, request):
        actions = super(LogCorreosEstadoReserva_Admin, self).get_actions(request)
        del actions['delete_selected'] # we remove the default delete action
        return actions
    
    
    def get_queryset(self, request):
        qs = super(LogCorreosEstadoReserva_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(correo__owner=request.user.teatro_set.first())
    
    def resend_mail(self, obj):
        return '<a href="%smail/reenviar-correo-estado-%d" target="_blank">' % (base_url, obj.id) + unicode(_(u'Reenviar correo a destinatarios')) + '</a>' 
    resend_mail.allow_tags = True
    resend_mail.short_description = _(u'Reenviar correo')
    
    
teyatadmin.register(LogCorreosEstadoReserva, LogCorreosEstadoReserva_Admin)
ravaladmin.register(LogCorreosEstadoReserva, LogCorreosEstadoReserva_Admin)


'''def enviar_correo_centros(self, request, queryset):
        # the below can be modified according to your application.
        # queryset will hold the instances of your model
        centros = Centro.objects.all()
        total_sent = 0
        messages =()
        for q in queryset:
            for i, c in enumerate(centros):
                if i>10:
                    pass
                else:
                    context = {'centro': c, 'correo': q}
                    text_content = render_to_string('email/campanya_centros_plaintext.html', context)
                    #html_content = render_to_string('email/campanya_centros.html', context)
                    cur_message = (q.asunto, text_content, 'db.teatre.raval@gmail.com', ['javier.vicente.r@gmail.com'])
                    messages = (cur_message,)  + messages 
                sent = send_mass_mail(messages, fail_silently=False)
                #sent = send_mail(subject=q.asunto, message=text_content, from_email='db.teatre.raval@gmail.com', recipient_list=['javier.vicente.r@gmail.com'], fail_silently=False, html_message=html_content) # use your email function here
                total_sent+= sent
                
                    
        if total_sent == 1:
            message_bit = _(u'1 correo enviado')
        else:
            message_bit = _(u'%s correos enviados') % total_sent
        self.message_user(request, _(u'%s satisfactoriamente.') % message_bit)
    enviar_correo_centros.short_description = _(u"Enviar correo a todos los centros")
    '''
    
    
'''
class AdjuntoCorreoCampanya_InLine(admin.TabularInline):
    model  = AdjuntoCorreoCampanya
    extra= 0

class AdjuntoCorreoCampanya_Admin(admin.ModelAdmin):
    model = AdjuntoCorreoCampanya
    list_display=('nombre','correo')
    search_fields=('nombre','correo__asunto_es', 'correo__asunto_ca',)
    list_filter= ('correo__campanya',)
    
admin.site.register(AdjuntoCorreoCampanya, AdjuntoCorreoCampanya_Admin)

class CorreoCampanya_InLine(admin.TabularInline):
    model  = CorreoCampanya
    form = CorreoCampanya_Form
    extra= 0
    suit_classes= 'suit-tab suit-tab-correo-campanya'
'''
