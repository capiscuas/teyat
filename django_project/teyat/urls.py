# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url, include

from teyat import views
          
          
urlpatterns = patterns('',
    
    url(r"^obra/obra(?P<id>\d+).pdf$", views.gererar_plantilla_obra, name='gererar_plantilla_obra'),
    url(r"^obra/ObraCampanya(?P<id>\d+).pdf$", views.gererar_plantilla_obra_campanya, name='gererar_plantilla_obra_campanya'),
    url(r"^obra/funcio(?P<id>\d+).pdf$", views.gererar_plantilla_funcion, name='gererar_plantilla_funcion'),
    url(r"^mail/test-alta-reserva-(?P<id>\d+)$", views.testear_correo_alta_reserva, name='testear_correo_alta_reserva'),
    url(r"^mail/test-recordatorio-reserva-(?P<id>\d+)$", views.testear_correo_recordatorio_reserva, name='testear_correo_recordatorio_reserva'),
    url(r"^mail/reenviar-correo-estado-(?P<id>\d+)$", views.reenviar_correo_estado_reserva, name='reenviar_correo_estado_reserva'),
    #url(r"^sitemap.xml$", views.sitemap, name='sitemap'),
    #url(r"^mail/reenviar-correo-recordatorio-(?P<id>\d+)$", views.reenviar_correo_recordatorio_reserva, name='reenviar_correo_recordatorio_reserva'),
)
