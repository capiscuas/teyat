# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from teyat.views import lanzar_correos_recordatorio_reserva, lanzar_correos_confirmacion_reserva

class Command(BaseCommand):
    args = 'None'
    help = 'Performs rutinary control of mails to send to spectators and centres with regard to the status of their bookings.'

    
    
    def handle(self, *args, **options):
    
        lanzar_correos_recordatorio_reserva()
        lanzar_correos_confirmacion_reserva()
        