# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from teyat.models import Reserva, Funcion

class Command(BaseCommand):
    args = 'None'
    help = 'Imprime las funciones de la campaña 2016 que tienen una discrepancia entre el aforo ocupado y el sumatorio de asientos ocupados por la reserva.'

    
    
    def handle(self, *args, **options):
    
        funciones = Funcion.objects.filter(obra_campanya__campanya__nombre__icontains='2016')
        for f in funciones:
            reservas=Reserva.objects.filter(funcion=f)
            aforo=0
            for r in reservas:
                if r.estado!='x':
                    aforo+=r.total_previsto()
            if aforo!=f.aforo_ocupado_previsto():
                print f