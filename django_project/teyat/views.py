# -*- coding: utf-8 -*-
from django.shortcuts import render 
from easy_pdf.rendering import render_to_pdf_response
from teyat.models import Teatro, Teatro_contacto
from teyat.models import Campanya, Obra, Funcion
from teyat.models import Reserva, Persona_contacto, Centro
from teyat.models import ObraEnCampanya, CorreoConfirmacionReserva, CorreoReserva, LogCorreosEstadoReserva, LogCorreosRecordatorios
from teyat.forms import RegistrationFormTermsOfService, Teatro_contacto_Form
from itertools import izip
from django.template.loader import render_to_string
from django.http import Http404
from django.core.mail import send_mail, BadHeaderError
from django.core.mail.message import EmailMessage
from django.utils import translation
from django.utils import timezone
from django.utils.translation import ugettext as _
from meta.views import Meta
from django.templatetags.static import static
from django.utils.translation import get_language
import random
import string
from django.template import RequestContext
from django_project.settings import BASE_DIR
from django.shortcuts import render, render_to_response, redirect
from django.template.context_processors import csrf
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.views.decorators.csrf import csrf_exempt

if 'prod' in BASE_DIR:
    base_url = 'http://teyat.es/'
else:
    base_url = 'http://teyat.es:9999/'
    
IVA = 1.21
MONTHLY_PRICE_PER_YEAR_LAUNCH = 960
MONTHLY_PRICE_PER_YEAR_STANDARD = 1440
MONTHLY_PRICE_PER_MONTH_LAUNCH = 80
MONTHLY_PRICE_PER_MONTH_STANDARD = 120
MONTHLY_PRICE_PER_DAY_LAUNCH = 2.67
MONTHLY_PRICE_PER_DAY_STANDARD = 4

YEARLY_PRICE_PER_YEAR_LAUNCH = 800
YEARLY_PRICE_PER_YEAR_STANDARD = 1200
YEARLY_PRICE_PER_MONTH_LAUNCH = 66.67
YEARLY_PRICE_PER_MONTH_STANDARD = 100
YEARLY_PRICE_PER_DAY_LAUNCH = 2.19
YEARLY_PRICE_PER_DAY_STANDARD = 3.29

LAUNCH_TIME = timezone.localtime(timezone.now()).replace(year=2016, month=3, day=1,hour=9, minute=0, second=0, microsecond=0) 
BEFORE_LAUNCH=False
now = timezone.localtime(timezone.now())

if now < LAUNCH_TIME:
    BEFORE_LAUNCH = True

LANDING_PICS = ['images/actriz-escenario-teyat.jpg',
                #'images/luces-escenario-teyat.jpg', 
                'images/ballet-ninya-teyat.jpg', 
                #'images/marioneta-escenario-teyat.jpg',
                #'images/escenario-musica-teyat.jpg',
                'images/ballet-escenario-teyat.jpg',
                'images/teyat-platea.jpg' ]
                 
#'images/butacas-teyat-bn.jpg',
                
def index(request):  
    return render(request,'teyat/index.html', {})       
    

def landing(request):
    
    title =_(u'Software de gestión para teatros con campaña escolar | teyat')
    description = _(u'Programa de gestión de teatros para campaña escolar. Gestiona tus centros escolares, administra tus reservas y controla tus aforos.')
    landing_picture = LANDING_PICS[random.randint(0,len(LANDING_PICS)-1)]
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )
    
    
    context = {
        'IVA': IVA,
        'MONTHLY_PRICE_PER_MONTH': MONTHLY_PRICE_PER_MONTH_LAUNCH,
        'MONTHLY_PRICE_PER_YEAR': MONTHLY_PRICE_PER_YEAR_LAUNCH,
        'YEARLY_PRICE_PER_YEAR': YEARLY_PRICE_PER_YEAR_LAUNCH,
        'YEARLY_PRICE_PER_MONTH': YEARLY_PRICE_PER_MONTH_LAUNCH,
        'meta': meta,
        'title_page': title,
        'landing_picture': landing_picture,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    
    
    return render(request, 'teyat/landing.html', context)
   
def features(request):  
    
    title =_(u'Características de teyat')
    description = _(u'Gestiona tu teatro con campaña escolar fácilmente y desde un único sitio: clientes, campañas, reservas, recaudaciones, correos y más.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )

    context ={
        'meta':meta,
        'title_page': title,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    return render(request,'teyat/features.html', context)       
    
    
def prices(request):

    title =_(u'Precios | teyat')
    description = _(u'Mejora tu gestión con un software pensado para teatros con campaña escolar. Benefíciate de un mes gratis y nuestros precios especiales de lanzamiento.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )
    
    context = {
        'IVA': IVA,
        'MONTHLY_PRICE_PER_MONTH': MONTHLY_PRICE_PER_MONTH_LAUNCH,
        'MONTHLY_PRICE_PER_MONTH_BEFORE': MONTHLY_PRICE_PER_MONTH_STANDARD,
        'MONTHLY_PRICE_PER_YEAR': MONTHLY_PRICE_PER_YEAR_LAUNCH,
        'MONTHLY_PRICE_PER_YEAR_BEFORE': MONTHLY_PRICE_PER_YEAR_STANDARD,
        'YEARLY_PRICE_PER_YEAR': YEARLY_PRICE_PER_YEAR_LAUNCH,
        'YEARLY_PRICE_PER_YEAR_BEFORE': YEARLY_PRICE_PER_YEAR_STANDARD,
        'YEARLY_PRICE_PER_MONTH': YEARLY_PRICE_PER_MONTH_LAUNCH,
        'YEARLY_PRICE_PER_MONTH_BEFORE': YEARLY_PRICE_PER_MONTH_STANDARD,
        'meta': meta,
        'title_page': title,
        'base_url': base_url,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    return render(request,'teyat/prices.html', context)       
    

def registro(request, alta_form=None):
    
    if not alta_form:
        alta_form = RegistrationFormTermsOfService()
    
    title =_(u'Prueba teyat gratis')
    description = _(u'Descubre teyat y gestiona las campañas escolares de tu teatro online fácil y rápido: clientes, campañas, reservas, recaudaciones y más.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )
    context = {
        'alta_form': alta_form,
        'meta': meta,
        'title_page': title,
        'base_url': base_url,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
    }
    
    return render(request,'teyat/register.html', context)
    

def pwd_generator(size=8, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits ):
    return ''.join(random.choice(chars) for _ in range(size))

    
    
def alta_user_teatro(teatro_contacto):

    ### CREATE THE USER
    nombre_completo = teatro_contacto.nombre.strip().split(' ')
    last_name=''
    first_name=''
    if len(nombre_completo)>2:
        last_name = nombre_completo[-2] + ' ' + nombre_completo[-1]
        first_name = ' '.join([s for s in nombre_completo[:-2]])
    elif len(nombre_completo)==2:
        last_name = nombre_completo[1]
        first_name = nombre_completo[0]
    else:
        first_name = nombre_completo[0]
    
    username = teatro_contacto.email.split('@')[0].lower()
    how_many= User.objects.filter(username__startswith=username).count()
    if how_many>0:
        username = username+str(how_many)
    
    password = pwd_generator()
    user = User.objects.create_user(username=username, password=password, email=teatro_contacto.email, first_name=first_name, last_name=last_name)
    user.is_staff=True 
    user.save()
    
    
    #### CREATE THE TEATRO
    teatro = Teatro()
    teatro.nombre = teatro_contacto.espacio_cultural
    teatro.email = teatro_contacto.email
    teatro.emailcampanya = teatro_contacto.email
    teatro.save()
    teatro.contacto = teatro_contacto
    teatro.users.add(user)
    teatro.save()
    
    
    #### Notify user with instructions:
    from_email = 'teyat@teyat.es'
    to_email=[user.email]
    subject= _(u'Todo listo para que empieces a usar teyat')
    
    message = '<p>'+unicode(_(u'Hola')) + ' ' + user.first_name + ',</p>'
    message += '<br/>'
    message += '<p>' + unicode(_(u'Hemos creado para ti una cuenta de usuario asociada a tu dirección de correo.'))+'</p>'
    message += '<br/>'
    message += '<p>'+unicode(_(u'Para empezar a usar teyat <a href="http://teyat.es/admin/">inicia sesión</a> con la cuenta de usuario y contraseña que te indicamos a continuación:'))+'</p>'
    message += '<br/>'
    message += '<p>'+unicode(_(u'Nombre de usuario: <b>%(usuario)s</b>') % {'usuario':username})+'</p>'
    message += '<p>'+unicode(_(u'Contraseña: <b>%(pwd)s</b>') % {'pwd':password})+'</p>'
    message += '<br/>'
    message += '<p>'+unicode(_(u'Una vez dentro de la administración de teyat, <b>puedes cambiar fácilmente tu contraseña</b> por una que te resulte más fácil de recordar. Para ello, presiona el enlace de "Cambiar contraseña" situado en la parte superior derecha:'))+'</p>'
    message += '<br/>'
    message += '<img src="http://teyat.es/static/images/teyat-admin-cambiar-pwd-1.jpg">'
    message += '<br/>'
    message += '<br/>'
    message += '<font size="4">'+unicode(_(u'¿Cuáles son los siguientes pasos?'))+'</font>'
    message += '<br/>'
    message += '<div><ul>'
    message += '<li>'+unicode(_(u'<b>Familiarízate con teyat</b>: El interfaz de teyat está pensado para ayudarte. Las secciones de la izquierda están diseñadas para que vayas visitándolas y rellenándolas de arriba a abajo.'))+'</li>'
    message += '<br/>'
    message += '<li>'+unicode(_(u'<b>Configura los detalles de tu teatro</b>: Sube los logotipos de tu espacio cultural, define cual es tu dirección de contacto para las campañas de reservas o el aforo de tu sala.'))+'</li>'
    message += '<br/>'
    message += '<li>'+unicode(_(u'<b>Si necesitas asistencia online estamos aquí</b> para dártela. Escríbenos a <a href="mailto:soporte@teyat.es">soporte@teyat.es</a> y te atenderemos lo antes posible.'))+'</li>'
    #message += '<li>'+unicode(_(u'<b>Puedes solicitar el alta de otros 2 usuarios</b>. A partir de este momento puedes tener asociados a tu teatro hasta 3 usuarios para que te ayuden a gestionar tu espacio cultural. Para ello, escríbenos un mail indicándonos el nombre que quieres para cada cuenta de usuario y la dirección de correo electrónico que irá asociada a cada cuenta.'))+'</li>'
    message += '</ul></div>'
    message += '<br/>'
    message += '<p>'+unicode(_(u'Atentamente,'))+'</p>'
    message += '<br/>'
    message += '<p>'+unicode(_(u'Tatiana y Javi, creadores de teyat.'))+'</p>'
    message += '<br/>'
    message += '<br/>'
   
    
    html_content = render_to_string('email/staff_mail_base.html', {'message':message})
        
    send_mail(subject, message, from_email, to_email, fail_silently=False, html_message=html_content)
    
    
    #### Notify teyat staff by e-mail:
    from_email = 'teyat@teyat.es'
                    
    subject = '[teyat] Tienes un nuevo teatro!'
    text_content = u'Hola!\n\nAcaba de registrarse un usuario. Estos son los detalles:\n\n'
    text_content += u'Nombre: ' + teatro_contacto.nombre + '\n'
    text_content += u'Mail: ' + teatro_contacto.email + '\n'
    text_content += u'Telefono: ' + teatro_contacto.telefono + '\n'
    text_content += u'Espacio cultural: ' + teatro_contacto.espacio_cultural + '\n'
    text_content += u'Cargo: ' + teatro_contacto.cargo + '\n'
    text_content += u'\n\nHemos creado una cuenta de usuario para este contacto (' + user.username + ')'
    text_content += u' y se ha creado el espacio cultural correspondiente.'
    text_content += u'\n\n\nHemos mandado un correo a ' + teatro_contacto.nombre + u' con los detalles de cómo debe crear su password y llevar a cabo sus primeros pasos'
    text_content += u'\n\nEstate atento a su actividad y, si ves que no se aclara, llámalo por teléfono: ' + teatro_contacto.telefono
    text_content += u'\n\nUn saludo!'
                
    recipient = ['info@teyat.es']
    send_mail(subject, text_content, from_email, recipient, fail_silently=False)
    
    return 0

@csrf_exempt
def gracias_alta(request):
    if request.method == 'POST':
        
        if "submit_alta_form" in request.POST:
            # get the data from the form
            #alta_form = Teatro_contacto_Form(request.POST)
            alta_form = RegistrationFormTermsOfService(request.POST)
            if alta_form.is_valid(): # All validation rules pass
                print 'data valid'
                nombre = alta_form.cleaned_data['nombre']
                email = alta_form.cleaned_data['email']
                telefono = alta_form.cleaned_data['telefono']
                espacio_cultural = alta_form.cleaned_data['espacio_cultural']
                cargo = alta_form.cleaned_data['cargo']
                
                nuevo_usuario = Teatro_contacto(nombre=nombre, email=email, telefono=telefono,espacio_cultural=espacio_cultural, cargo=cargo)
                nuevo_usuario.save()
                
                # We create a User and a Teatro from the form data
                alta_user_teatro(nuevo_usuario)
                
                
                '''# Notify by e-mail:
                from_email = 'teyat@teyat.es'
                    
                subject = '[teyat] Nuevo registro'
                text_content = 'Hola!\n\nAcaba de registrarse un usuario. Estos son los detalles:\n\n'
                text_content += 'Nombre: ' + nombre + '\n'
                text_content += 'Mail: ' + email + '\n'
                text_content += 'Telefono: ' + telefono + '\n'
                text_content += 'Espacio cultural: ' + espacio_cultural + '\n'
                text_content += 'Cargo: ' + cargo + '\n'
                
                recipient = ['info@teyat.es']
                send_mail(subject, text_content, from_email, recipient, fail_silently=False)
                '''
            else:
                return render(request, "teyat/register.html", {'alta_form': alta_form,})
    else:
        nuevo_usuario=Teatro_contacto()
        nuevo_usuario.nombre="usuario"
        nuevo_usuario.email="usuario@usuario.es"
        
    title =_(u'Gracias por registrarse en teyat')
    description = _(u'Acabo de registrarme en teyat y estoy seguro de que a un teatro como el tuyo también puede interesarle.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = base_url,
        locale = get_language(),
    )
    
    context = {
        'nuevo_usuario': nuevo_usuario,
        'meta': meta,
        'base_url': base_url,
        'title_page': title,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    context.update(csrf(request))
    #print context
    
    return render_to_response('teyat/gracias_alta.html', context)

def contacto(request):

        
    title =_(u'Contacta con teyat')
    description = _(u'Gestiona tu teatro online fácil y rápido: clientes, campañas, reservas, recaudaciones. Teatro campaña escolar.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )
    
    context = {
        'meta': meta,
        'title_page': title,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    
    return render(request,'teyat/contacto.html', context)       

def about(request):  
    
    title =_(u'Sobre teyat y sus creadores')
    description = _(u'Gestiona tu teatro online fácil y rápido: clientes, campañas, reservas, recaudaciones. Teatro campaña escolar.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )

    context ={
        'meta':meta,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    return render(request,'teyat/about.html', context)      
    
    
def legal(request):  
    
    title =_(u'Nota legal y Privacidad de teyat')
    description = _(u'Gestiona tu teatro online fácil y rápido: clientes, campañas, reservas, recaudaciones. Teatro campaña escolar.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )

    context ={
        'meta':meta,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    return render(request,'teyat/legal.html', context)      
    
def cookies(request):  
    
    title =_(u'Política de Cookies de teyat')
    description = _(u'Gestiona tu teatro online fácil y rápido: clientes, campañas, reservas, recaudaciones. Teatro campaña escolar.')
    
    meta = Meta(
        title=title,
        description= description,
        image=static('images/teyat-software-de-gestion-cultural.jpg'),
        url = request.build_absolute_uri(),
        locale = get_language(),
    )

    context ={
        'meta':meta,
        'BEFORE_LAUNCH': BEFORE_LAUNCH,
        'LAUNCH_TIME': LAUNCH_TIME,
        'base_url': base_url,
    }
    return render(request,'teyat/cookies.html', context)      
    

def sitemap(request):

    ### Adding static pages
    static_urls = ['teyat_lang:landing','teyat_lang:prices', 'teyat_lang:features','teyat_lang:register', 'teyat_lang:about', 
        'teyat_lang:contacto', 'teyat_lang:legal', 'teyat_lang:cookies']
    urls=[]
    for u in static_urls:
        aux = reverse(u)
        if aux.startswith('/es/') or aux.startswith('/ca/'):
            urls.append(aux[3:])
        else:
            urls.append(aux)
            
    context = {'urls': urls}
    return render(request,'teyat/sitemap.html', context, content_type="application/xhtml+xml")

    
    
    
    
    
    
    
def gererar_plantilla_obra(request, id):  
    
    try:
        obra = Obra.objects.get(pk=id)
        
        func = Funcion.objects.filter(obra_campanya__obra=obra)
        
        funciones=[]
        reservas=[]
        for f in func:
            res= Reserva.objects.filter(funcion=f, estado='c')
            funciones.append(f)
            reservas.append(res)
        if funciones!=[]:
                funciones_y_reservas = izip(funciones, reservas)
        context = {'obra': obra, 'funciones_y_reservas': funciones_y_reservas, 'now': timezone.now()}
        return render_to_pdf_response(request,'teyat/ficha_obra.html', context)       
    except Obra.DoesNotExist:
        raise Http404
    raise Http404  

def gererar_plantilla_obra_campanya(request, id):  
    
    try:
        obra_campanya = ObraEnCampanya.objects.get(id=id)
        
        func = Funcion.objects.filter(obra_campanya=obra_campanya)
        
        funciones=[]
        reservas=[]
        for f in func:
            #res= Reserva.objects.filter(funcion=f, estado='c')
            res= Reserva.objects.filter(funcion=f).exclude(estado='x')
            funciones.append(f)
            reservas.append(res)
            
        funciones_y_reservas=[]
        if funciones!=[]:
                funciones_y_reservas = izip(funciones, reservas)
        context = {'obra_campanya': obra_campanya, 'funciones_y_reservas': funciones_y_reservas, 'now': timezone.now()}
        return render_to_pdf_response(request,'teyat/ficha_obra_campanya.html', context)       
    except ObraEnCampanya.DoesNotExist:
        raise Http404
    raise Http404      
    
    
def gererar_plantilla_funcion(request, id):  
    
    try:
        funcion = Funcion.objects.get(pk=id)
        reservas= Reserva.objects.filter(funcion=funcion, estado='c')
        context = {'funcion': funcion, 'reservas': reservas, 'now': timezone.now()}
        return render_to_pdf_response(request,'teyat/ficha_funcion.html', context)
        
    except Funcion.DoesNotExist:
        raise Http404
    raise Http404  

def elaborar_asunto_alta_reserva(correo, func):  
    subject = '[' + correo.owner.nombre + '] ' 
    if correo.asunto=='1': # Reserva para [nombre de la obra] creada ([ESTADO])
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'ca':
            translation.activate('ca')
            subject += u'Reserva per' + ' ' + func.obra_campanya.obra.nombre + ' ' + 'creada (' + correo.get_estado_display() + ')'
        if correo.owner.idioma_mails==u'bi':
            subject += ' / ' 
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'es':
            translation.activate('es')
            subject += u'Reserva para'  + ' ' + func.obra_campanya.obra.nombre + ' ' + 'creada (' + correo.get_estado_display() + ')'
    if correo.asunto=='2': # Su reserva para la obra [nombre de la obra] [ESTADO]
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'ca':
            translation.activate('ca')
            subject += u'La seua reserva per a l\'obra' + ' ' + func.obra_campanya.obra.nombre + '(' + correo.get_estado_display() + ')'
        if correo.owner.idioma_mails==u'bi':
            subject += ' / ' 
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'es':
            translation.activate('es')
            subject += u'Su reserva para la obra'  + ' ' + func.obra_campanya.obra.nombre + '(' + correo.get_estado_display() + ')'
    if correo.asunto=='3': # Reserva [ESTADO] para obra [nombre de la obra]
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'ca':
            translation.activate('ca')
            subject += u'Reserva' + ' ' + correo.get_estado_display() + ' ' + 'per a l\'obra' + ' ' + func.obra_campanya.obra.nombre
        if correo.owner.idioma_mails==u'bi':
            subject += ' / '
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'es':
            translation.activate('es')
            subject += u'Reserva' + ' ' + correo.get_estado_display() + ' ' + 'para la obra' + ' ' + func.obra_campanya.obra.nombre
    return subject

def elaborar_asunto_recordatorio_reserva(correo, func):  
    subject = '[' + correo.owner.nombre + '] ' 
    if correo.asunto=='1': # Reserva para [nombre de la obra] el [fecha_hora]
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'ca':
            subject += u'Rerserva per' + ' ' + func.obra_campanya.obra.nombre + ' el ' + timezone.localtime(func.fecha_hora).strftime("%d/%m/%Y %H:%M")
        if correo.owner.idioma_mails==u'bi':
            subject += ' / '
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'es':
            subject += u'Reserva para'  + ' ' + func.obra_campanya.obra.nombre + ' el ' + timezone.localtime(func.fecha_hora).strftime("%d/%m/%Y %H:%M")
    if correo.asunto=='2': # Su reserva para la obra [nombre de la obra] el [fecha_hora]'
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'ca':
            subject += u'La seua reserva per a l\'obra' + ' ' + func.obra_campanya.obra.nombre + ' el ' + timezone.localtime(func.fecha_hora).strftime("%d/%m/%Y %H:%M")
        if correo.owner.idioma_mails==u'bi':
            subject += ' / '
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'es':
            subject += u'Su reserva para la obra'  + ' ' + func.obra_campanya.obra.nombre + ' el ' + timezone.localtime(func.fecha_hora).strftime("%d/%m/%Y %H:%M") 
    if correo.asunto=='3': # Reserva del [fecha hora] para obra [nombre de la obra]
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'ca':
            subject += u'Reserva el' + ' ' + timezone.localtime(func.fecha_hora).strftime("%d/%m/%Y %H:%M") + ' ' + 'per a l\'obra' + ' ' + func.obra_campanya.obra.nombre
        if correo.owner.idioma_mails==u'bi':
            subject += ' / ' 
        if correo.owner.idioma_mails==u'bi' or correo.owner.idioma_mails==u'es':
            subject += u'Reserva el' + ' ' + timezone.localtime(func.fecha_hora).strftime("%d/%m/%Y %H:%M") + ' ' + 'para la obra' + ' ' + func.obra_campanya.obra.nombre
    return subject
    
def testear_correo_alta_reserva(request, id):  
    
    try:
        correo = CorreoConfirmacionReserva.objects.get(id=id)
        
        func = Funcion.objects.filter(obra_campanya__obra__owner=correo.owner).order_by('?').first()
        
        #destinatario = Teatro.objects.get(id=correo.owner.id)
        destinatario = request.user
        
        #reserva = None
        reserva = Reserva.objects.filter(funcion=func).order_by('?').first()
        
        subject = elaborar_asunto_alta_reserva(correo, func)
        
        from_email = correo.owner.emailcampanya
        
        to_email = destinatario.email
        
        context = {'correo': correo,
                   'from_email': from_email,
                   'destinatario': destinatario,
                   'func': func,
                   'reserva': reserva
                  }
    
        # Mail for the Guest
        text_content = render_to_string('email/alta_reserva_plaintext.html', context)
        html_content = render_to_string('email/alta_reserva.html', context)
        send_mail(subject, text_content, from_email, [to_email], fail_silently=False, html_message=html_content)

        return render(request, 'teyat/test_correo.html', {'mail': destinatario.email, 'BEFORE_LAUNCH': BEFORE_LAUNCH, 'LAUNCH_TIME': LAUNCH_TIME,})
    except CorreoConfirmacionReserva.DoesNotExist:
        raise Http404
    raise Http404     
  

    
def enviar_correo_confirmacion_reserva(reserva):
    
    current_lang = get_language()
    
    try:
        
        correo = CorreoConfirmacionReserva.objects.get(owner=reserva.centro.owner, estado=reserva.estado)
        
        if not correo.activo:
            return False # DRAFT status
        
        func = Funcion.objects.get(id=reserva.funcion.id)
        
        if reserva.responsable:
            destinatario = Persona_contacto.objects.get(id=reserva.responsable.id)
            to_email = [destinatario.email]
            to_email.append(destinatario.centro.email)
        else:
            destinatario = Centro.objects.get(id=reserva.centro.id)
            to_email = [destinatario.email]
            
        
        subject = elaborar_asunto_alta_reserva(correo, func)
        
        from_email = correo.owner.emailcampanya
        
        
        
        context = {'correo': correo,
                   'from_email': from_email,
                   'destinatario': destinatario,
                   'func': func,
                   'reserva': reserva
                  }
    
        # Mail
        text_content = render_to_string('email/alta_reserva_plaintext.html', context)
        html_content = render_to_string('email/alta_reserva.html', context)
        
        send_mail(subject, text_content, from_email, to_email, fail_silently=False, html_message=html_content)
        #print 'simulando envío:'
        #print correo
        #print reserva
        
        # Log the action
        logcorreo = LogCorreosEstadoReserva(reserva=reserva, correo=correo)
        logcorreo.save()
        
    except CorreoConfirmacionReserva.DoesNotExist:
        return False
    return True      

def reenviar_correo_estado_reserva(request, id):
    
    logcorreo = LogCorreosEstadoReserva.objects.get(id=id)
    enviar_correo_estado_reserva(logcorreo.reserva, logcorreo.correo)
    #print 'simulando reenvío'
    return render(request, 'teyat/reenviar_correo.html', {'reserva': logcorreo.reserva, 'correo': logcorreo.correo,'BEFORE_LAUNCH': BEFORE_LAUNCH, 'LAUNCH_TIME': LAUNCH_TIME,})
    
'''def reenviar_correo_recordatorio_reserva(request, id):
    
    logcorreo = LogCorreosRecordatorios(id=id)
     
    send_recordatorio_mail(logcorreo.correo, logcorreo.reserva.funcion)
    
    return render(request, 'teyat/reenviar_correo.html', {'reserva': logcorreo.reserva, 'correo': logcorreo.correo,'BEFORE_LAUNCH': BEFORE_LAUNCH, 'LAUNCH_TIME': LAUNCH_TIME,})
'''

def enviar_correo_estado_reserva(reserva,correo):
    
    current_lang = get_language()

    func = Funcion.objects.get(id=reserva.funcion.id)
        
    if reserva.responsable:
        destinatario = Persona_contacto.objects.get(id=reserva.responsable.id)
        to_email = [destinatario.email]
        to_email.append(destinatario.centro.email)
    else:
        destinatario = Centro.objects.get(id=reserva.centro.id)
        to_email = [destinatario.email]
            
        
    subject = elaborar_asunto_alta_reserva(correo, func)
        
    from_email = correo.owner.emailcampanya
        
    context = {'correo': correo,
               'from_email': from_email,
               'destinatario': destinatario,
               'func': func,
               'reserva': reserva
               }
    
    # Mail
    text_content = render_to_string('email/alta_reserva_plaintext.html', context)
    html_content = render_to_string('email/alta_reserva.html', context)
        
    send_mail(subject, text_content, from_email, to_email, fail_silently=False, html_message=html_content)
    #print 'simulando envío:'
    #print correo
    #print reserva 
    
    # Log the action
    logcorreo = LogCorreosEstadoReserva(reserva=reserva, correo=correo)
    logcorreo.save()
        
    return True      
    
    
    
    
def testear_correo_recordatorio_reserva(request, id):
    
    try:
        correo = CorreoReserva.objects.get(id=id)
        
        func = Funcion.objects.filter(obra_campanya__obra=correo.obra_campanya.obra).order_by('?').first()
        
        #destinatario = Teatro.objects.get(id=correo.owner.id)
        destinatario = request.user
        
        reserva = Reserva.objects.filter(funcion=func).order_by('?').first()
        
        subject = elaborar_asunto_recordatorio_reserva(correo, func)
        
        from_email = correo.owner.emailcampanya
        
        to_email = destinatario.email
        
        context = {'correo': correo,
                   'from_email': from_email,
                   'destinatario': destinatario,
                   'func': func,
                   'reserva': reserva
                  }
    
        
        # We send the mail 
        html_content = render_to_string('email/recordatorio_reserva.html', context)
        msg = EmailMessage(subject, html_content, from_email, [to_email])
        msg.content_subtype = "html"  # Main content is now text/html
        adjuntos = correo.adjuntos.all()
        for a in adjuntos:
            msg.attach_file(a.fichero.path)
        msg.send()
        
        return render(request, 'teyat/test_correo.html', {'mail': destinatario.email, 'BEFORE_LAUNCH': BEFORE_LAUNCH, 'LAUNCH_TIME': LAUNCH_TIME,})
    except CorreoConfirmacionReserva.DoesNotExist:
        raise Http404
    raise Http404     
    
    
def send_recordatorio_mail(correo, func):
    
    reservas = Reserva.objects.filter(funcion=func).exclude(estado='x')
    #print 'enviando correos para funcion:'
    #print func
    for reserva in reservas:
        if reserva.responsable:
            destinatario = Persona_contacto.objects.get(id=reserva.responsable.id)
            to_email = [destinatario.email]
            to_email.append(destinatario.centro.email)
        else:
            destinatario = Centro.objects.get(id=reserva.centro.id)
            to_email = [destinatario.email]
            
        
        subject = elaborar_asunto_recordatorio_reserva(correo, func)
        
        from_email = correo.owner.emailcampanya
        
        context = {'correo': correo,
                   'from_email': from_email,
                   'destinatario': destinatario,
                   'func': func,
                   'reserva': reserva
                  }
    
        html_content = render_to_string('email/recordatorio_reserva.html', context)
        msg = EmailMessage(subject, html_content, from_email, to_email)
        msg.content_subtype = "html"  # Main content is now text/html
        adjuntos = correo.adjuntos.all()
        for a in adjuntos:
            msg.attach_file(a.fichero.path)
        
        #print 'simulando envío:'
        #print correo
        #print reserva
        
        msg.send()
     
        # Log the action
        logcorreo = LogCorreosRecordatorios(reserva=reserva, correo=correo)
        logcorreo.save()
    
    return 0
    
    
def send_recordatorio_mail_to_teatro(correo, func):

    destinatario = Teatro.objects.get(id=correo.owner.id)
    #print 'enviando correo a:'
    #print destinatario
    to_email = [destinatario.email]
            
    subject = elaborar_asunto_recordatorio_reserva(correo, func)
        
    from_email = correo.owner.emailcampanya
        
    context = {'correo': correo,
           'from_email': from_email,
           'destinatario': destinatario,
           'func': func,
           'reserva': None
            }
    
    html_content = render_to_string('email/recordatorio_reserva.html', context)
    msg = EmailMessage(subject, html_content, from_email, to_email)
    msg.content_subtype = "html"  # Main content is now text/html
    adjuntos = correo.adjuntos.all()
    for a in adjuntos:
        msg.attach_file(a.fichero.path)
        
     
    msg.send()
    
    return 0
    
    
def lanzar_correos_recordatorio_reserva(test=False):  
    
    now = timezone.localtime(timezone.now())
    #now = timezone.localtime(timezone.now()).replace(year=2016, month=2, day=29,hour=21, minute=30, second=0, microsecond=0) 

    # Cogemos todos los correos recordatorios activos:
    correos = CorreoReserva.objects.filter(activo=True)
    
    # para cada correo:
    for correo in correos:
        candidate_to_deactivate=[]
        hora_envio = int(correo.hora.split(':')[0])
        minutos_envio = int(correo.hora.split(':')[1])
        '''
        print 'hora actual: '
        print  now.hour 
        print 'hora envio: ' 
        print  hora_envio
        print 'minuto actual: '
        print  now.minute 
        print 'minuto envio: ' 
        print minutos_envio
        '''
        
        if now.hour==hora_envio and minutos_envio-2 <= now.minute and now.minute <= minutos_envio+2:
            # cogemos las funciones relacionadas con el correo:
            funciones = Funcion.objects.filter(obra_campanya__obra=correo.obra_campanya.obra)
            
            for func in funciones:
                #print func
                #send_to_teatre=True
                
                # We replace the time of the function to obtain rounded deltas
                delta = timezone.localtime(func.fecha_hora).replace(hour=now.hour, minute=now.minute) - now
                #print delta
                #print 'días de diferencia'
                #print delta.days
                if delta.days + 1 > 0: # la obra todavía no se ha celebrado
                    #print 'La obra todavía no se ha representado'
                    if correo.antes:
                        #print ' correo antes!!'
                        candidate_to_deactivate.append(0)
                        #print 'dias: ' 
                        #print  correo.dias
                        if delta.days + 1 ==correo.dias:
                            #print 'enviamos!'
                            send_recordatorio_mail(correo, func)
                                #if send_to_teatre:
                                #    send_recordatorio_mail_to_teatro(correo,func)
                                #    send_to_teatre=False
                                #print 'Horay! mail sent:'
                                #print func
                        else: # correo se manda tras la obra
                            candidate_to_deactivate.append(0)
                else: # la obra ya se ha representado
                    #print 'La obra ya se ha representado!!'
                    if correo.antes:
                        #print ' correo antes!!'
                        candidate_to_deactivate.append(1)
                    else: # correo se manda tras la obra
                        #print 'correo después'
                        #print 'dias: ' 
                        #print  correo.dias
                        if delta.days + 1 ==-correo.dias:
                            #print 'enviamos!'
                            send_recordatorio_mail(correo, func)
                            #if send_to_teatre:
                            #    send_recordatorio_mail_to_teatro(correo,func)
                            #    send_to_teatre=False
                            #print 'Horay! mail sent:'
                            #print func
                        if delta.days + 1 <-correo.dias: # ya han pasado los días que había que esperar para mandar el correo tras la obra
                            candidate_to_deactivate.append(1)
                        else:
                            candidate_to_deactivate.append(0)

            #print 'Candidato para desactivar?'
            #print candidate_to_deactivate
            if sum(candidate_to_deactivate) == len(funciones):   
                correo.activo=False
                correo.save()
                #print 'correo desactivado'
    return 0    

def lanzar_correos_confirmacion_reserva():
    
    reservas = Reserva.objects.all()
    
    for reserva in reservas:
        
        try:
            correo = CorreoConfirmacionReserva.objects.get(owner=reserva.centro.owner, estado=reserva.estado,activo=True)

            # comprobamos las veces que se ha mandado un correo de estado de reserva para esta reserva:
            times_sent = LogCorreosEstadoReserva.objects.filter(reserva=reserva).count()
            # mandamos el correo si nunca se ha enviado correo de estado de reserva
            if times_sent == 0: 
                enviar_correo_estado_reserva(reserva, correo)
            # alternativamente, mandamos el correo si nos lo han pedido explicitamente
            elif reserva.reenviar_mail==True:
                if enviar_correo_estado_reserva(reserva, correo):
                    reserva.reenviar_mail=False
                    reserva.save()
                        
        except CorreoConfirmacionReserva.DoesNotExist:
            pass
    return 0     

# Funcion aunxiliar para guardar el registro de los corros que ya se han mandado en el TEATRE RAVAL
def registro_correos_estado():
    
    reservas = Reserva.objects.all()
    correo_confirmado = CorreoConfirmacionReserva.objects.get(estado='c', owner__nombre__icontains='raval')
    correo_cancelado = CorreoConfirmacionReserva.objects.get(estado='x', owner__nombre__icontains='raval')
    correo_pendiente = CorreoConfirmacionReserva.objects.get(estado='p', owner__nombre__icontains='raval')
    for r in reservas:
        if r.mail_pendiente_enviado:
            logcorreo, created = LogCorreosEstadoReserva.objects.get_or_create(reserva=r, correo=correo_pendiente)
            logcorreo.enviado_en = r.modificado
            logcorreo.save()
        if r.mail_confirmado_enviado:
            logcorreo, created = LogCorreosEstadoReserva.objects.get_or_create(reserva=r, correo=correo_confirmado)
            logcorreo.enviado_en = r.modificado
            logcorreo.save()
        if r.mail_cancelado_enviado:
            logcorreo, created = LogCorreosEstadoReserva.objects.get_or_create(reserva=r, correo=correo_cancelado)
            logcorreo.enviado_en = r.modificado
            logcorreo.save()
        
            
# Funcion aunxiliar para evitar que se manden correos a los centros del TEATRE RAVAL
# informando sobre las reservas de la campanya anterior
def preparar_estado_correos_raval():
    teatro= Teatro.objects.get(nombre__icontains='raval')
    campanya_pasada= Campanya.objects.get(nombre__icontains='2014',owner=teatro)
    
    reservas = Reserva.objects.filter(funcion__obra_campanya__campanya=campanya_pasada)
    for reserva in reservas:
        if reserva.estado=='c':
            reserva.mail_confirmado_enviado=True
            reserva.save()
        elif reserva.estado=='p':
            reserva.mail_pendiente_enviado=True
            reserva.save()
        else:
            reserva.mail_cancelado_enviado=True
            reserva.save()
            
# Funcion aunxiliar para guardar el registro de los corros que ya se han mandado en el TEATRE RAVAL
def missing_correos_estado_raval():
    
    reservas = Reserva.objects.filter(centro__owner__id=2)
    correo_confirmado = CorreoConfirmacionReserva.objects.get(estado='c', owner__nombre__icontains='raval')
    correo_cancelado = CorreoConfirmacionReserva.objects.get(estado='x', owner__nombre__icontains='raval')
    correo_pendiente = CorreoConfirmacionReserva.objects.get(estado='p', owner__nombre__icontains='raval')
    for r in reservas:
        if LogCorreosEstadoReserva.objects.filter(reserva=r).count()==0:
            print r
        